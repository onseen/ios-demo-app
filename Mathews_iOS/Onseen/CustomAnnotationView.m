//
//  CustomAnnotationView.m
//  Onseen
//
//  Created by CherryPie on 9/22/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "CustomAnnotationView.h"

@implementation CustomAnnotationView

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)setParameters{
    NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] init];
    NSDictionary * attrForTitle = @{NSForegroundColorAttributeName : [UIColor blackColor],
                                    NSFontAttributeName : [UIFont fontWithName:@"AvenirNext-Medium" size:15.f]
                                    };
    NSDictionary * attrForSubtitle = @{NSForegroundColorAttributeName : [UIColor blackColor],
                                       NSFontAttributeName : [UIFont fontWithName:@"AvenirNext-Regular" size:15.f]
                                       };
    NSAttributedString *tempAttrString = [[NSAttributedString alloc]
                                          initWithString:[NSString stringWithFormat:@"%@: ",self.titleString]
                                          attributes:attrForTitle];
    [attrTitle appendAttributedString:tempAttrString];
    tempAttrString = [[NSAttributedString alloc]
                      initWithString:self.subtitleString
                      attributes:attrForSubtitle];
    [attrTitle appendAttributedString:tempAttrString];
    CGSize maximumLabelSize = CGSizeMake(220, MAXFLOAT);
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    NSString *tempString = attrTitle.string;
    CGRect labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                                  options:options
                                               attributes:attrForSubtitle
                                                  context:nil];
    CGFloat height = ceilf(labelBounds.size.height);
    tempString = _textString;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                                  options:options
                                               attributes:attrForSubtitle
                                                  context:nil];
    height = height +  ceilf(labelBounds.size.height);
    tempString = _addressString;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                           options:options
                                        attributes:attrForSubtitle
                                           context:nil];
    height = height +  ([tempString length] ? ceilf(labelBounds.size.height) : 0);
    self.titleLabel.attributedText = attrTitle;
    self.subtitleLabel.text  = _textString;
    self.addressLabel.text = _addressString;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height + 20);
    [self.layer setCornerRadius:5];
    self.layer.masksToBounds = NO;
    self.layer.shadowColor =  [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:0.5].CGColor ;
    self.layer.shadowPath = [UIBezierPath
                             bezierPathWithRoundedRect:CGRectMake(-2, -2, self.bounds.size.width+4, self.bounds.size.height+4)
                             cornerRadius:1].CGPath;
    self.layer.cornerRadius = 1;
    self.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.layer.shadowRadius = 1;
    self.layer.shadowOpacity = 1;
    self.layer.opacity = 1.0;
    UIImageView *arrowImage= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    arrowImage.frame = CGRectMake(self.center.x-15, self.frame.size.height-2, 30, 30);
    arrowImage.layer.zPosition = -5;
    [self addSubview:arrowImage];
}

@end
