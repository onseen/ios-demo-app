

#import <Foundation/Foundation.h>
#import "Marker.h"
@import CoreLocation;
@import UIKit;

@interface Offer : NSObject

@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSMutableSet *iBeacons;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *like;
@property (strong, nonatomic) NSString *media;
@property (strong, nonatomic) NSString *address;
@property BOOL optedIn;
@property (strong, nonatomic) Marker *marker;
@property (strong, nonatomic) NSString *logoImage;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSMutableSet *macAddress;
@property (strong, nonatomic) NSString *expirationDate;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
