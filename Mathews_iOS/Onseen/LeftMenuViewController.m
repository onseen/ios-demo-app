//
//  LeftMenuViewController.m
//  Onseen
//

//

#import "LeftMenuViewController.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "WebViewController.h"

@interface LeftMenuViewController ()<UITableViewDelegate, UITableViewDataSource>


@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"Cell%ld",(long)indexPath.row] forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MainViewController *mainViewController = (MainViewController*)self.sideMenuController;
    WebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    
    switch (indexPath.row) {
        case 0:
            webViewController.nameString = @"View Inventory";
            webViewController.urlString = @"http://www.mathewsnewark.com/inventory.cfm?reset=true&viewnewused=new";
            break;
        case 1:
            webViewController.nameString = @"Service Scheduling";
            webViewController.urlString = @"http://mathewsfordnewark.com/Service-Appointment/";
            break;
        case 2:
            webViewController.nameString = @"Credit Application";
            webViewController.urlString = @"https://www.routeone.net/XRD/xrdStart.do?dealerId=DV7KM";
            break;
        case 3:
            webViewController.nameString = @"Finance Products";
            webViewController.urlString = @"http://www.mathewsnewark.com/content.cfm?contentId=1561";
            break;
        case 4:
            webViewController.nameString = @"Lease End Process";
            webViewController.urlString = @"https://www.ford.com/finance/lease-end";
            break;
        case 5:
            webViewController.nameString = @"Contact Info";
            webViewController.urlString = @"http://www.mathewsnewark.com/contact.cfm";
            break;
        case 6:
            webViewController.nameString = @"Staff Bios";
            webViewController.urlString = @"http://www.mathewsnewark.com/staff.cfm?dkid=706sales";
            break;
        default:
            break;
    }
    [mainViewController presentViewController:webViewController animated:YES completion:nil];
    [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
}
- (IBAction)hideMenu:(id)sender {
    MainViewController *mainViewController = (MainViewController*)self.sideMenuController;
    [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
}


@end
