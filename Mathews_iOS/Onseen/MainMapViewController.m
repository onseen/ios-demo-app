//
//  ViewController.m
//  Onseen
//
//  Created by admin on 21.09.16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "MainMapViewController.h"
#import <MapKit/MapKit.h>
#import "DataFetcher.h"
#import "OffersViewController.h"
#import "CustomAnnotationView.h"
#import "OfferDetailViewController.h"
#import "LocationManager.h"
#import "Campaign.h"
#import "ReachabilityManager.h"
#import "CampaignDataSource.h"
#import "SDImageCache.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "AccountViewController.h"

typedef enum {
    Campaigns = 0,
    Offers = 1
} MarkerType;

@interface MainMapViewController ()<MKMapViewDelegate, UIGestureRecognizerDelegate>
    
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomFilterView;
    @property (weak, nonatomic) IBOutlet UIImageView *campaignsCheckImage;
    @property (weak, nonatomic) IBOutlet UIImageView *offersCheckImage;
    @property (weak, nonatomic) IBOutlet MKMapView *mainMapView;
    @property (strong, nonatomic) MKAnnotationView *selectedAnnotationView;
    @property (nonatomic) BOOL isFilterViewOpen;
    @property (nonatomic) BOOL isCampaingsParameterCheck;
    @property (nonatomic) BOOL isOffersParameterCheck;
    @property (nonatomic) BOOL isStartInit;
    @property (strong, nonatomic) CustomAnnotationView *calloutView;
    
    @end

@implementation MainMapViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkForTapOnCalloutView)];
    tap.delegate = self;
    [_mainMapView addGestureRecognizer:tap];
    
    
    [[LocationManager shared] initCurrentLocation];
    _isFilterViewOpen = NO;
    _isOffersParameterCheck = YES;
    _isCampaingsParameterCheck = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCurrentLocation)
                                                 name:@"UpdateLocation"
                                               object:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"IS_FIRST_START"]){
        [defaults setObject:@"NO" forKey:@"IS_FIRST_START"];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Mathews"
                                                                                 message:@"Please take a minute to complete your Profile"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Complete"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             AccountViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountViewController"];
                                                             [self.navigationController pushViewController:vc animated:YES];
                                                         }];
        [alertController addAction:actionOk];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                             }];
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}
    
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _isStartInit = YES;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"DEVICE_ID"]){
        [DataFetcher registerDeviceOnSuccess:^(NSDictionary *phoneInfo) {
            [self getMarkers];
        } failure:^(NSError *error) {
        }];
    }
    else{
        [self getMarkers];
    }
    
//    OfferDetailViewController *sharedOfferDetailViewController = [OfferDetailViewController getSharedOfferDetailViewController];
//    if (sharedOfferDetailViewController){
//        [sharedOfferDetailViewController movePosition];
//    }
}
    
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}
    
- (void)updateCurrentLocation{
    _isStartInit = NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"DEVICE_ID"]){
        [DataFetcher registerDeviceOnSuccess:^(NSDictionary *phoneInfo) {
            [self getMarkers];
        } failure:^(NSError *error) {
        }];
    }
    else{
        [self getMarkers];
    }
}
    
- (void)getMarkers{
    [DataFetcher getNewCampaignAndOffersLat:[LocationManager shared].lastValidLocation.coordinate.latitude lon:[LocationManager shared].lastValidLocation.coordinate.longitude success:^(NSArray *compaigns) {
        [self.mainMapView removeAnnotations:[self.mainMapView annotations]];
        [[CampaignDataSource sharedDataSource] updateDataSource:compaigns];
        [[ReachabilityManager shared] initReachabilityStatus];
        if (_isCampaingsParameterCheck)
            [self addMarksOnMapWithType:Campaigns];
        if (_isOffersParameterCheck)
            [self addMarksOnMapWithType:Offers];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.description);
    }];
}
    
- (void)removeMarksFromMap{
    NSArray *annotationToRemove = [_mainMapView annotations];
    for (id<MKAnnotation> ann in annotationToRemove) {
        if ([ann.title integerValue] ){
            if (!_isOffersParameterCheck)
            [_mainMapView removeAnnotation:ann];
        }
        else{
            if (!_isCampaingsParameterCheck)
            [_mainMapView removeAnnotation:ann];
        }
    }
}
    
- (void)addMarksOnMapWithType:(MarkerType)type{
    for (Campaign *campaign in [CampaignDataSource sharedDataSource].campaigns) {
        // if someone likes a campaign, show the offers
        if ([campaign.like integerValue]) {
            // cycle through offers and add them to the mapview
            if (type == Offers){
                for (Offer *offer in campaign.offers) {
                    NSLog(@"CAMPAIGN offer.optedIn=%i", offer.optedIn);
                    if (offer.optedIn == false) {
                        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                        point.coordinate = CLLocationCoordinate2DMake(offer.marker.latitude, offer.marker.longitude);
                        point.title = [NSString stringWithFormat:@"%u",type];
                        point.subtitle = offer.uuid;
                        [self.mainMapView addAnnotation:point];
                    }
                }
            }
        } else {
            // Show campaign
            if (type == Campaigns){
                for (Marker *marker in campaign.markers) {
                    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                    point.coordinate = CLLocationCoordinate2DMake(marker.latitude, marker.longitude);
                    point.title = [NSString stringWithFormat:@"%u",type];
                    point.subtitle = campaign.uuid;
                    [self.mainMapView addAnnotation:point];
                }
            }
        }
    }
}

#pragma mark - filter methods
    
- (IBAction)openFilterAction:(id)sender {
    _isFilterViewOpen = !_isFilterViewOpen;
    _constraintBottomFilterView.constant = _isFilterViewOpen ? 0 : -101;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}
    
- (IBAction)chooseCampaignsAction:(id)sender {
    _isStartInit = YES;
    _isCampaingsParameterCheck = !_isCampaingsParameterCheck;
    _campaignsCheckImage.hidden = !_isCampaingsParameterCheck;
    if (_isCampaingsParameterCheck)
    [self addMarksOnMapWithType:Campaigns];
    else
    [self removeMarksFromMap];
}
    
- (IBAction)chooseOffersAction:(id)sender {
    _isStartInit = YES;
    _isOffersParameterCheck = !_isOffersParameterCheck;
    _offersCheckImage.hidden = !_isOffersParameterCheck;
    if (_isOffersParameterCheck)
    [self addMarksOnMapWithType:Offers];
    else
    [self removeMarksFromMap];
}
    
#pragma mark - MKMapViewDelegate
    
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *annotationIdentifier = @"annotationIdentifier";
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                    reuseIdentifier:annotationIdentifier];
    UIImage *flagImage = [UIImage imageNamed:[[annotation title] integerValue] ? @"BluePin" : @"YellowPin"];
    CGSize size = CGSizeMake(17, 50);
    UIGraphicsBeginImageContext(size);
    [flagImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    annotationView.image = resizedImage;
    annotationView.layer.anchorPoint = CGPointMake(0.5f, 1.0f);
    return annotationView;
}
    
- (void) calloutTapped{
    OfferDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
    if ([[_selectedAnnotationView.annotation title] integerValue]){
        vc.popUpType = OffersMap;
        vc.datas = [@[[self findItemById:[_selectedAnnotationView.annotation subtitle] withType:Offers]] mutableCopy];
    }
    else{
        vc.popUpType = CampaignsMap;
        Campaign *camp = [self findItemById:[_selectedAnnotationView.annotation subtitle] withType:Campaigns];
        vc.datas = [@[camp] mutableCopy];
        vc.likeCampaign = ^{
            OfferDetailViewController *vcBlock = [self.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
            vcBlock.popUpType = OffersMap;
            vcBlock.datas = [camp.offers mutableCopy];
            vcBlock.providesPresentationContextTransitionStyle = YES;
            vcBlock.definesPresentationContext = YES;
            [vcBlock setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self presentViewController:vcBlock animated:YES completion:nil];
        };
    }
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:vc animated:YES completion:nil];
}
    
- (id)findItemById:(NSString*)itemId withType:(MarkerType)type{
    if (type == Campaigns){
        for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
        if ([camp.uuid isEqual:itemId])
        return camp;
    }
    else{
        for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
        for (Offer *offer in camp.offers)
        if ([offer.uuid isEqual:itemId])
        return offer;
    }
    return nil;
}
    
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    if (_isStartInit){
        MKAnnotationView *aV;
        for (aV in views) {
            CGRect endFrame = aV.frame;
            aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - 230.0, aV.frame.size.width, aV.frame.size.height);
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [aV setFrame:endFrame];
            [UIView commitAnimations];
        }
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if (_calloutView){
        CGPoint point = [touch locationInView:_calloutView];
        if (point.x > 0 && point.x < _calloutView.frame.size.width && point.y > 0 && point.y < _calloutView.frame.size.height)
        return YES;
    }
    return NO;
}
    
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    _selectedAnnotationView = view;
    _calloutView = [[[NSBundle mainBundle] loadNibNamed:@"CustomAnnotationView" owner:self options:nil] firstObject];
    id obj = [self findItemById:[view.annotation subtitle] withType:[[view.annotation title] integerValue] ? Offers : Campaigns];
    _calloutView.titleString = [[view.annotation title] integerValue] ? ((Offer*)obj).name : ((Campaign*)obj).account.name;
    _calloutView.subtitleString = [[view.annotation title] integerValue] ? ((Offer*)obj).text : @"";
    _calloutView.textString = [[view.annotation title] integerValue] ? ((Offer*)obj).title : ((Campaign*)obj).name;
    _calloutView.addressString = [[view.annotation title] integerValue] ? ((Offer*)obj).address : @"";
    [_calloutView setParameters];
    _calloutView.frame = CGRectMake(-_calloutView.frame.size.width/2 + 10, -30 - _calloutView.frame.size.height,_calloutView.frame.size.width, _calloutView.frame.size.height);
    [view addSubview:_calloutView];
}
    
- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    [_calloutView removeFromSuperview];
    _calloutView = nil;
}
    
- (void)checkForTapOnCalloutView{
    [self calloutTapped];
}
- (IBAction)showMenu:(id)sender {
    MainViewController *mainViewController = (MainViewController*)self.sideMenuController;
    [mainViewController showLeftViewAnimated:YES completionHandler:nil];
}
    
@end
