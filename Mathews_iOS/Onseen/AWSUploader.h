//
//  AWSUploader.h
//  Mercedes
//
//  Created by Mike Timashov on 2/8/17.
//  Copyright © 2017 CherryPie Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AWSUploader : NSObject

typedef void (^AWSUploaderCompletionHandler)(NSString *imageUrl);

- (instancetype)initWithBucket:(NSString*)bucket;
- (void)uploadImage:(UIImage *)image completionHandler:(AWSUploaderCompletionHandler)completionHandler;

@end
