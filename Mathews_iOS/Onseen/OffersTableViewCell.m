//
//  OffersTableViewCell.m
//  Onseen
//
//  Created by CherryPie on 9/21/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "OffersTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation OffersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)prepareForReuse{
    _logoImage.image = nil;
    [_logoImage sd_cancelCurrentImageLoad];
}

@end
