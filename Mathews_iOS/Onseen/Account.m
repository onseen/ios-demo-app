

#import "Account.h"

@implementation Account

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.uuid = dictionary[@"id"];
        self.media = dictionary[@"media"] ? dictionary[@"media"] : @"";
        self.name = dictionary[@"name"] ? dictionary[@"name"] : @"";
    }
    return self;
}

@end
