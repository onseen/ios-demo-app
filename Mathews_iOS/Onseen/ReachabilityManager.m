//
//  ReachabilityManager.m
//  Onseen
//
//  Created by CherryPie on 9/29/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "ReachabilityManager.h"
#import "Reachability.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "CampaignDataSource.h"
#import "Campaign.h"
#import "Offer.h"
#import "DataFetcher.h"
#import "OfferDetailViewController.h"
#import "AppDelegate.h"

@interface ReachabilityManager()

@property (nonatomic, strong) Reachability *internetReachability;
@property (nonatomic, strong) Offer *currentOffer;

@end

@implementation ReachabilityManager

+ (ReachabilityManager*)shared
{
    static ReachabilityManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ReachabilityManager alloc] init];

    });
    return _sharedInstance;
}

- (void)initReachabilityStatus{
    [self findMacAddress];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    _internetReachability = [Reachability reachabilityForInternetConnection];
    [_internetReachability startNotifier];
}

-(void) checkNetworkStatus:(NSNotification *)notice {
    NetworkStatus internetStatus = [_internetReachability currentReachabilityStatus];
    switch (internetStatus) {
        case NotReachable: {
            break;
        }
        case ReachableViaWiFi: {
            if (_currentOffer)
                [self postOutFromBoundsRequest];
            NSLog(@"The internet is working via WIFI.");
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
            [self findMacAddress];
            break;
        }
        case ReachableViaWWAN: {
            if (_currentOffer)
                [self postOutFromBoundsRequest];
            NSLog(@"The internet is working via WWAN.");
            break;
        }
    }
}

- (void)findMacAddress{
    NSString *wifiMacAddress = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"BSSID"]) {
            wifiMacAddress = [self getValidMacAddress:info[@"BSSID"]] ;
        }
    }
    NSMutableArray *offersArray = [NSMutableArray array];
    if ([wifiMacAddress length]){
    for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
        for (Offer *offer in camp.offers){
            if ([offer.macAddress containsObject:wifiMacAddress])
                [offersArray addObject:offer];
        }
    }
    if ([offersArray count]){
        _currentOffer = [offersArray firstObject];
        [self postInBoundsRequest];
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
            [[CampaignDataSource sharedDataSource].nearOffers addObject:[offersArray mutableCopy]];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            OfferDetailViewController *vc = [delegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
            vc.providesPresentationContextTransitionStyle = YES;
            vc.definesPresentationContext = YES;
            vc.datas = [[CampaignDataSource sharedDataSource].nearOffers lastObject];
            vc.popUpType = OffersMap;
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [((UINavigationController*)delegate.window.rootViewController).topViewController presentViewController:vc animated:YES completion:^{
                [[CampaignDataSource sharedDataSource].nearOffers removeLastObject];
            }];
        }
    }
}

- (NSString*)getValidMacAddress:(NSString*)macAddress{
    NSMutableString *validMacAddress = [NSMutableString string];
    NSArray *tempArray = [macAddress componentsSeparatedByString:@":"];
    for (NSString *str in tempArray){
        NSString *tempString = str.length > 1 ? [str uppercaseString] : [[NSString stringWithFormat:@"0%@",str] uppercaseString];
        [validMacAddress appendString:[NSString stringWithFormat:@":%@",tempString]];
    }
    return [validMacAddress substringFromIndex:1];
}

- (void)postInBoundsRequest{
    [DataFetcher inBoundsWithOffer:_currentOffer.uuid success:^{
    } failure:^(NSError *error) {
    }];
}

- (void)postOutFromBoundsRequest{
    [DataFetcher outFromBoundsWithOffer:_currentOffer.uuid success:^{
        _currentOffer = nil;
    } failure:^(NSError *error) {
    }];
}

@end
