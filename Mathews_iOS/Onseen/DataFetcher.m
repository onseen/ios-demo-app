//
//  DataFetcher.m
//  Onseen
//
//  Created by CherryPie on 9/27/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "DataFetcher.h"
#import "AFNetworking.h"
#import "NSDictionary+JSONRepresentation.h"
#import <sys/utsname.h>





@implementation DataFetcher

+ (void)getNewCampaignAndOffersLat:(CGFloat)lat
                               lon:(CGFloat)lon
                           success:(void (^)(NSArray *compaigns))success
                           failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId;
    if ([defaults objectForKey:@"DEVICE_ID"]){
        deviceId  = [defaults objectForKey:@"DEVICE_ID"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:[NSString stringWithFormat:@"%@offers/v2/%@/%@/%.6f/%.6f/1000",basicURL,applicationId,deviceId,lat,lon] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject[@"data"] && ![responseObject[@"data"] isEqual:[NSNull null]])
                success(responseObject[@"data"]);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
    }
    else{
        [DataFetcher registerDeviceOnSuccess:^(NSMutableDictionary *phoneInfo) {
            NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:[NSString stringWithFormat:@"%@offers/v2/%@/%@/%.6f/%.6f/1000",basicURL,applicationId,deviceId,lat,lon] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (responseObject[@"data"] && ![responseObject[@"data"] isEqual:[NSNull null]])
                    success(responseObject[@"data"]);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
        } failure:^(NSError *error) {
        }];
    }
}

+ (void)getMyOffersListOnSuccess:(void (^)(NSArray *compaigns))success
                         failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId;
    if ([defaults objectForKey:@"DEVICE_ID"]){
        deviceId  = [defaults objectForKey:@"DEVICE_ID"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:[NSString stringWithFormat:@"%@offers/v2/%@/%@/",basicURL,applicationId,deviceId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject[@"data"] && ![responseObject[@"data"] isEqual:[NSNull null]])
                success(responseObject[@"data"]);
            else
                failure([NSError errorWithDomain:@"com.company.myapp.ErrorDomain" code:0 userInfo:nil]);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
    }
    else{
        [DataFetcher registerDeviceOnSuccess:^(NSMutableDictionary *phoneInfo) {
            NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:[NSString stringWithFormat:@"%@offers/v2/%@/%@/",basicURL,applicationId,deviceId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (responseObject[@"data"])
                    success(responseObject[@"data"]);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failure(error);
            }];
        } failure:^(NSError *error) {
            failure(error);
        }];
    }
}

+ (void)likeOffer:(NSString*)offerId
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@offers/v2/%@/%@/like/%@",basicURL,applicationId,deviceId,offerId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)dislikeOffer:(NSString*)offerId
             success:(void (^)())success
             failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@offers/v2/%@/%@/dislike/%@",basicURL,applicationId,deviceId,offerId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)likeCampaigns:(NSString*)campaignId
              success:(void (^)())success
              failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@campaigns/v2/%@/%@/like/%@",basicURL,applicationId,deviceId,campaignId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)dislikeCampaigns:(NSString*)campaignId
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@campaigns/v2/%@/%@/dislike/%@",basicURL,applicationId,deviceId,campaignId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)registerDeviceOnSuccess:(void (^)(NSMutableDictionary *phoneInfo))success
                        failure:(void (^)(NSError *error))failure{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@device/v2/%@",basicURL,applicationId]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    struct utsname systemInfo;
    uname(&systemInfo);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *param;
    if (![defaults objectForKey:@"PROFILE_SETTINGS"]){
        NSString *vendorId = [[UIDevice currentDevice] identifierForVendor].UUIDString;

        param = [@{@"type": [NSString stringWithCString:systemInfo.machine
                                                                              encoding:NSUTF8StringEncoding],
                                                  @"udid": vendorId,
                                                  @"imei": @"",
                                                  @"detail": [@{
                                                                @"maxHeight": [NSString stringWithFormat:@"%.0f",[UIScreen mainScreen].bounds.size.height],
                                                                @"maxWidth": [NSString stringWithFormat:@"%.0f",[UIScreen mainScreen].bounds.size.width],
                                                                @"name": @"",
                                                                @"email": @"",
                                                                @"phone": @"",
                                                                @"tokenId": @"eed1e28819f7f4eb12b",
                                                                @"notification": [@{
                                                                                    @"push": @"no",
                                                                                    @"email": @"no",
                                                                                    @"text": @"no"
                                                                                    } mutableCopy]
                                                                } mutableCopy]
                                                  } mutableCopy];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:param];
        [defaults setObject:data forKey:@"PROFILE_SETTINGS"];
        [defaults synchronize];
    }
    else{
        NSData *loadData = [[NSUserDefaults standardUserDefaults] objectForKey:@"PROFILE_SETTINGS"];
        param = [NSKeyedUnarchiver unarchiveObjectWithData:loadData];
    }
    NSString *jsonParam = [param jsonStringFromDictionary];
    [request setHTTPBody:[jsonParam dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          failure(error);
                                          return;
                                      }
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      
                                      NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                      NSLog(@"Response Body:\n%@\n", body);
                                      NSError *error1;
                                      NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                           options:kNilOptions
                                                                                             error:&error1];
                                      NSLog(@"%@",json);
                                      if (json[@"data"]){
                                          [defaults setObject:json[@"data"][@"id"] forKey:@"DEVICE_ID"];
                                          [defaults synchronize];
                                          success(json[@"data"]);
                                      }
                                  }];
    [task resume];
}

+ (void)setAccountInfo:(NSDictionary*)info
               success:(void (^)())success
               failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@device/v2/%@/%@",basicURL,applicationId,deviceId]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"PUT"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *jsonParam = [info jsonStringFromDictionary];
    [request setHTTPBody:[jsonParam dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          failure(error);
                                          return;
                                      }
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }
                                      
                                      NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                      NSLog(@"Response Body:\n%@\n", body);
                                      
                                      success();
                                  }];
    [task resume];
}

+ (void)inBoundsWithOffer:(NSString*)offerId
                  success:(void (^)())success
                  failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@offers/v2/%@/%@/in/%@",basicURL,applicationId,deviceId,offerId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)outFromBoundsWithOffer:(NSString*)offerId
                       success:(void (^)())success
                       failure:(void (^)(NSError *error))failure{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceId = [defaults objectForKey:@"DEVICE_ID"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:[NSString stringWithFormat:@"%@offers/v2/%@/%@/out/%@",basicURL,applicationId,deviceId,offerId] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)getPassDataWithId:(NSString*)offerId
                  success:(void (^)(NSData *passData))success
                  failure:(void (^)(NSError *error))failure{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/mypass.pkpass",basicPassURL,applicationId,offerId]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          failure(error);
                                          return;
                                      }
                                      success(data);
                                  }];
    [task resume];

}

@end
