//
//  WebViewController.h
//  Onseen

//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *nameString;
@property (nonatomic, strong) NSString *urlString;

@end
