

#import <Foundation/Foundation.h>
@import UIKit;

@interface Account : NSObject

@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *media;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
