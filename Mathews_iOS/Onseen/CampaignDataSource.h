//
//  CampaignDataSource.h
//  Onseen
//
//  Created by CherryPie on 9/29/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeaconDetails.h"
#import "BeaconDetailsCloudFactory.h"
#import "CachingContentFactory.h"
#import "ProximityContentManager.h"
#import <EstimoteSDK/EstimoteSDK.h>

@interface CampaignDataSource : NSObject

@property (nonatomic, strong) NSMutableArray *campaigns;
@property (nonatomic, strong) NSMutableArray *nearOffers;

+ (instancetype)sharedDataSource;
- (void)updateDataSource:(NSArray*)campaigns;
- (void)didFindBeacon:(NSArray*)beacon;
- (void)didEnterBeacon:(CLBeaconRegion*)beacon;
- (void)didExitBeacon:(CLBeaconRegion*)beacon;

@end
