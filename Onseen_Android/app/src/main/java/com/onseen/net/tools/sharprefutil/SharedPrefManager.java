package com.onseen.net.tools.sharprefutil;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * For SAVE/READ in Shared Preferences
 *
 * USE:
 * SharedPrefManager sharedPrefManager = new SharedPrefManager();
 * sharedPrefManager.init(this);
 * String id = sharedPrefManager.getIdString();
 */
public class SharedPrefManager implements Manager {

    private static final String SHARED_PREFERENCES_NAME = "sharedPrefs"; // file name

    // KEY in to file
    private static final String PROFILE_STATE = "profile_state";
    private static final String WALLET_STATE = "wallet_state";
    private static final String ID = "id";
    private static final String DEVICE = "device";
    private static final String NAME = "name";
    private static final String USER_NAME = "user_name";
    private static final String USER_PHOTO_URL = "user_photo_url";
    private static final String EMAIL = "email";

    private static final String PUSH_NOTIFICATIONS = "push_notifications";
    private static final String SMS_NOTIFICATIONS = "sms_notifications";
    private static final String EMAIL_NOTIFICATIONS = "email_notifications";

    private static final String FIREBASE_TOKEN = "firebaseToken";

    private SharedPreferences mPreferences;
    private Set<CachedValue> mCachedValues;

    //String, Integer, Float, Long, Boolean.class
    private CachedValue<Boolean> mProfileState;
    private CachedValue<Boolean> mWalletState;

    private CachedValue<String> mIdString;
    private CachedValue<String> mDeviceString;
    private CachedValue<String> mUserPhotoUrlString;

    private CachedValue<String> mNameString;
    private CachedValue<String> mUsernameString;
    private CachedValue<String> mEmailString;
    private CachedValue<Boolean> mPushNotificationsState;
    private CachedValue<Boolean> mSmsNotificationsState;
    private CachedValue<Boolean> mEmailNotificationsState;
    private CachedValue<String> mFirebaseTokenString;

    @Override
    public void init(Context context) {
        mPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        CachedValue.initialize(mPreferences);
        mCachedValues = new HashSet<>();
        mCachedValues.add(mProfileState = new CachedValue<>(PROFILE_STATE, false, Boolean.class));
        mCachedValues.add(mWalletState = new CachedValue<>(WALLET_STATE, false, Boolean.class));
        mCachedValues.add(mIdString = new CachedValue<>(ID, "-1", String.class));
        mCachedValues.add(mDeviceString = new CachedValue<>(DEVICE, "-1", String.class));
        mCachedValues.add(mUserPhotoUrlString = new CachedValue<>(USER_PHOTO_URL, "", String.class));

        mCachedValues.add(mNameString = new CachedValue<>(NAME, "", String.class));
        mCachedValues.add(mUsernameString = new CachedValue<>(USER_NAME, "", String.class));
        mCachedValues.add(mEmailString = new CachedValue<>(EMAIL, "", String.class));
        mCachedValues.add(mPushNotificationsState = new CachedValue<>(PUSH_NOTIFICATIONS, true, Boolean.class));
        mCachedValues.add(mSmsNotificationsState = new CachedValue<>(SMS_NOTIFICATIONS, true, Boolean.class));
        mCachedValues.add(mEmailNotificationsState = new CachedValue<>(EMAIL_NOTIFICATIONS, true, Boolean.class));
        mCachedValues.add(mFirebaseTokenString = new CachedValue<>(FIREBASE_TOKEN, "-1", String.class));

    }

    public void setAllProfile(String mNameString, String mUsernameString, String mEmailString, boolean mPushNotificationsState, boolean mSmsNotificationsState, boolean mEmailNotificationsState) {
        setNameString(mNameString);
        setUsernameString(mUsernameString);
        setEmailString(mEmailString);
        setPushNotificationsState(mPushNotificationsState);
        setSmsNotificationsState(mSmsNotificationsState);
        setEmailNotificationsState(mEmailNotificationsState);
    }

    public void setProfileState(Boolean bool) {
        this.mProfileState.setValue(bool);
    }
    public Boolean getProfileState() {
        return mProfileState.getValue();
    }

    public void setWalletState(Boolean bool) {
        this.mWalletState.setValue(bool);
    }
    public Boolean getWalletState() {
        return mWalletState.getValue();
    }

    public void setIdString(String string) {
        this.mIdString.setValue(string);
    }
    public String getIdString() {
        return mIdString.getValue();
    }

    public void setDeviceString(String string) {
        this.mDeviceString.setValue(string);
    }
    public String getDeviceString() {
        return mDeviceString.getValue();
    }

    public void setUserPhotoUrl(String string) {
        this.mUserPhotoUrlString.setValue(string);
    }
    public String getUserPhotoUrl() {
        return mUserPhotoUrlString.getValue();
    }

    public void setNameString(String string) {
        this.mNameString.setValue(string);
    }
    public String getNameString() {
        return mNameString.getValue();
    }

    public void setUsernameString(String string) {
        this.mUsernameString.setValue(string);
    }
    public String getUsernameString() {
        return mUsernameString.getValue();
    }

    public void setEmailString(String string) {
        this.mEmailString.setValue(string);
    }
    public String getEmailString() {
        return mEmailString.getValue();
    }

    public void setPushNotificationsState(Boolean bool) {
        this.mPushNotificationsState.setValue(bool);
    }
    public boolean getPushNotificationsState() {
        return mPushNotificationsState.getValue();
    }

    public void setSmsNotificationsState(Boolean bool) {
        this.mSmsNotificationsState.setValue(bool);
    }
    public boolean getSmsNotificationsState() {
        return mSmsNotificationsState.getValue();
    }

    public void setEmailNotificationsState(Boolean bool) {
        this.mEmailNotificationsState.setValue(bool);
    }
    public boolean getEmailNotificationsState() {
        return mEmailNotificationsState.getValue();
    }

    public void setFirebaseTokenString(String string) {
        this.mFirebaseTokenString.setValue(string);
    }
    public String getFirebaseTokenString() {
        return mFirebaseTokenString.getValue();
    }

    @Override
    public void clear() {  // skip file to default
        for (CachedValue value : mCachedValues) {
            value.delete();
        }
    }


}