package com.onseen.net.tools.wifiutil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;

import com.onseen.net.MyApplication;
import com.onseen.net.R;
import com.onseen.net.network.NetworkCallBack;
import com.onseen.net.network.NetworkController;
import com.onseen.net.network.model.DeviceByOffers;
import com.onseen.net.tools.CPConstan;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.ui.MapsActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.onseen.net.MyApplication.logD;
import static com.onseen.net.tools.CPConstan.VALID_TIME_OFFERS;

/**
 * Get SSID & MAC from WiFi
 */
public class WifiService extends Service {
    private Context mContext = this;
    private static final long DELAY = CPConstan.DELAY_WIFI_GET_MAC; //sec.
    private static final long DELAYED_BOUNDS = CPConstan.DELAY_WIFI_OUT;  // min
    private ArrayList<DeviceByOffers> deviceByOfferFounds;
    private List<DeviceByOffers> offersFromDevice;
    private List<DeviceByOffers> deviceByOffersList;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logD("WiFi", "WiFi Service onStartCommand OOOOO");

        final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        deviceByOfferFounds = new ArrayList<>();
        offersFromDevice = CPGlobalData.getInstance().offersFromDevice;
        if (offersFromDevice == null) {
            offersFromDevice = new ArrayList<>();
            CPGlobalData.getInstance().offersFromDevice = offersFromDevice;
        }
        WifiInfo info = wifiManager.getConnectionInfo();
        if (info != null && info.getBSSID() != null) {
            String mac = info.getBSSID().toLowerCase();
            if (mac != null && !mac.isEmpty()) {
                if (!mac.equalsIgnoreCase(CPGlobalData.getInstance().mWiFiMacOld)) { // TODO  // if new wifi -> check WiFi
                    logD("onEnteredRegion WiFi: " + mac);
                    if (foundWiFiInArr(mac)) {
                        if (!deviceByOfferFounds.isEmpty()) {
                            checkFoundsOffersAndSendBroadcast();
                        }
                    }
                }
            }
        }

        stopSelf();                                                                         // TODO EXIT
        return START_NOT_STICKY;
    }

    private void wifiDelay(final WifiManager wifiManager) {
        // Need to wait a bit for the SSID to get picked up;
        // if done immediately all we'll get is null
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                WifiInfo info = wifiManager.getConnectionInfo();
                if (info != null) {
                    String mac = info.getBSSID().toLowerCase();
                    if (mac != null && !mac.isEmpty()) {
                       // if (!mac.equalsIgnoreCase(CPGlobalData.getInstance().mWiFiMacOld)) { // TODO  // if new wifi -> check WiFi
                            logD("onEnteredRegion WiFi: " + mac);
                            if (foundWiFiInArr(mac)) {
                                if (!deviceByOfferFounds.isEmpty()) {
                                    checkFoundsOffersAndSendBroadcast();
                                }
                            }
                       // }
                    }
                }
                stopSelf();                                                                         // TODO EXIT
            }
        }, DELAY);
    }

    // found all offers if equals UUID from beacon
    private boolean foundWiFiInArr(String mac) {
        boolean found = false;
        CPGlobalData.getInstance().mWiFiMacOld = mac;
        CPGlobalData.getInstance().mWiFiMillis = System.currentTimeMillis();
        deviceByOffersList = CPGlobalData.getInstance().deviceByOffersList;
        if (deviceByOffersList != null && !deviceByOffersList.isEmpty()) {
            for (int i = 0; i < deviceByOffersList.size(); i++) {
                DeviceByOffers deviceByOffer = deviceByOffersList.get(i);
                if (deviceByOffer.wifi && deviceByOffer.deviceMac != null && !deviceByOffer.deviceMac.isEmpty()) {
                    boolean b = deviceByOffer.deviceMac.equals(mac);
                    if (b) {
                        if (!deviceByOffer.boundsInState) {
                            deviceByOffer.boundsInState = true;
                            deviceByOffer.boundsOutState = false;
                            deviceByOffer.boundsInMillisecond = System.currentTimeMillis();
                            //sendBoundsRequest("in", deviceByOffers.offerId);                                // TODO IN
                            deviceByOfferFounds.add(deviceByOffer);
                            found = true;
                            logD("found Offer WIFI In deviceByOffersList");
                        }
  /*                    } else if (deviceByOffers.boundsInState && !deviceByOffers.boundsOutState) {
                        if (System.currentTimeMillis() > deviceByOffers.boundsInMillisecond + DELAYED_BOUNDS) {
                            deviceByOffers.boundsInState = false;
                            deviceByOffers.boundsOutState = true;
                            deviceByOffers.boundsOutMillisecond = System.currentTimeMillis();
                            //sendBoundsRequest("out", deviceByOffers.offerId);                              // TODO OUT
                        }*/
                    }
                }
            }
        }
        if (!found) deviceByOfferFounds = new ArrayList<>();
        return found;
    }

    @Override
    public void onDestroy() {
        logD("WiFi", "WiFi Service onDestroy XXXXX");
        super.onDestroy();
    }

    private void checkFoundsOffersAndSendBroadcast() {
        boolean offerFoundState = false;
        cleanOfferFromDevice();
        for (int i = 0; i < deviceByOfferFounds.size(); i++) {
            if (containsOfferId(deviceByOfferFounds.get(i).offerId)) {
                logD("WiFi OffersFromDevice contains OfferId");
            } else {
                logD("WiFi OffersFromDevice add OfferId" + deviceByOfferFounds.get(i).offerId);
                offerFoundState = true;
                offersFromDevice.add(deviceByOfferFounds.get(i));                                        // add if not equals OfferId
            }
        }
        if (offerFoundState)
            sendOfferBroadcast();                                                                        // send if found new valid offer
    }

    private void cleanOfferFromDevice() {
        long validTime = System.currentTimeMillis() - VALID_TIME_OFFERS;
        Iterator<DeviceByOffers> iterator = CPGlobalData.getInstance().offersFromDevice.iterator();
        while (iterator.hasNext()) {
            DeviceByOffers offerByDevice = iterator.next();
            if (validTime > offerByDevice.boundsInMillisecond) {     //TODO check offer validTime
                iterator.remove();
            }
        }
    }

    private boolean containsOfferId(String offerId) {
        if (!offersFromDevice.isEmpty())
            for (int i = 0; i < offersFromDevice.size(); i++) {
                if (offersFromDevice.get(i).offerId.equalsIgnoreCase(offerId))
                    return true;
            }
        return false;
    }

    private void sendOfferBroadcast() {
        if (!CPGlobalData.getInstance().activityVisible) {
            MyApplication.showNotificationBeacon(mContext.getString(R.string.app_name), mContext.getString(R.string.offer_notification));
        } else {
            Intent intent = new Intent(MapsActivity.BROADCAST_FOUND_OFFER);
            intent.putExtra("indexFromService", "wifi");
            mContext.sendBroadcast(intent);
        }
    }


    private void sendBoundsRequest(final String action, final String offerId) {                             // action = in/out
        NetworkController.getInstance().sendBoundsRequest(this, action, offerId, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
            }
        });
    }

}
