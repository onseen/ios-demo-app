package com.onseen.net.ui.adapter;

import android.view.View;

import com.onseen.net.network.model.MyOffersCard;


public interface ItemClickListener {

    void onItemClick(View view, int position, MyOffersCard myOffersCard);

    void onButtonClick(View view, int position, MyOffersCard myOffersCard);

}
