package com.onseen.net.tools.btutil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.onseen.net.tools.CPGlobalData;

public class BTReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

/*        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

            if (state == BluetoothAdapter.STATE_TURNING_OFF || state == BluetoothAdapter.STATE_OFF) {
                // Bluetooth OFF — STOP Service
                if (CPGlobalData.getInstance().BTServiceIntent != null) {
                    context.stopService(CPGlobalData.getInstance().BTServiceIntent);
                    CPGlobalData.getInstance().BTServiceIntent = null;
                }
            }
            if (state == BluetoothAdapter.STATE_TURNING_ON || state == BluetoothAdapter.STATE_ON ) {
                // Bluetooth ON — START Service
                if (CPGlobalData.getInstance().BTServiceIntent == null) {
                    CPGlobalData.getInstance().BTServiceIntent = new Intent(context, BtEstimoteService.class);
                    context.startService(CPGlobalData.getInstance().BTServiceIntent);
                }
            }
        }*/

        if (action.equals("ru.racoondeveloper.beaconnotifyer.WAKE_RECEIVER")) {
            // App START — START Service
            if (CPGlobalData.getInstance().BTServiceIntent == null) {
                CPGlobalData.getInstance().BTServiceIntent = new Intent(context, BtEstimoteService.class);
                context.startService(CPGlobalData.getInstance().BTServiceIntent);
            }
        }

    }
}

