package com.onseen.net.network;


import android.graphics.Bitmap;

public interface NetworkCallBackImage {
    void onResponse(Bitmap response);
}
