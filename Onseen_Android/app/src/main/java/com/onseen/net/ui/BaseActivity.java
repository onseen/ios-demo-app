package com.onseen.net.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.onseen.net.MyApplication;
import com.onseen.net.R;
import com.onseen.net.network.model.DeviceByOffers;
import com.onseen.net.tools.CPConstan;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.ui.fragment.MapOfferDialogFragment;
import com.onseen.net.ui.fragment.ProgressDialogFragment;
import com.onseen.net.network.NetworkCallBack;
import com.onseen.net.network.NetworkController;
import com.onseen.net.network.model.OfferCard;
import com.onseen.net.tools.FontAvenirConfigure;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.onseen.net.MyApplication.logD;

public abstract class BaseActivity extends AppCompatActivity implements  MapOfferDialogFragment.OnMapOfferDialogFragmentSelectedListener {

    private long start;
    protected String TAG = "MYAPP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        FontAvenirConfigure.getInstance(getApplicationContext());  // initial
    }

    protected abstract int getLayoutResourceId();

    protected void start() { //TODO FOR TEST TIME
        start = System.currentTimeMillis();
    }

    protected void stop() { //TODO FOR TEST TIME
        long finish = System.currentTimeMillis();
        long timeConsumedMillis = finish - start;
        Log.d(TAG, "DROP_TIME " + String.valueOf(timeConsumedMillis) + " м.сек.");
    }

    protected boolean isOnline() {           //TODO INTERNET TEST
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    protected static void makeToast(Context context, String massage){
        Toast.makeText(context, massage, Toast.LENGTH_SHORT).show();
    }

    // prepare uri from resource
    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }

    protected void sendCampaignActionRequest(final String action, final String idCampaign) {
        NetworkController.getInstance().sendCampaignActionRequest(this, action, idCampaign, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            //    ActionResponseModel actionResponseModel = new Gson().fromJson(jsonString, ActionResponseModel.class);
            //    Toast.makeText(getApplicationContext(), action + " = " + actionResponseModel.success, Toast.LENGTH_SHORT).show();      // TODO DEBUG
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
            //    Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show(); // TODO DEBUG
            }
        });
    }

    protected void sendOffersActionRequest(final String action, final String idOffer) {
        NetworkController.getInstance().sendOffersActionRequest(this, action, idOffer, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            //   ActionResponseModel actionResponseModel = new Gson().fromJson(jsonString, ActionResponseModel.class);
            //    Toast.makeText(getApplicationContext(), action + " = " + actionResponseModel.success, Toast.LENGTH_SHORT).show();      // TODO DEBUG
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
            //    Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show(); // TODO DEBUG
            }
        });
    }

    protected void startFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.show_transition, R.anim.hide_transition)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    protected void endFragment(Fragment fragment) {
        if (fragment != null)
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.show_transition, R.anim.hide_transition)
                .remove(fragment)
                .commit();
    }

    protected void shareTwitter(OfferCard offerCard, Bitmap bitmap, boolean haveImage) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        if (haveImage){
            tweetIntent.setType("image/*");
            tweetIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
        }
        tweetIntent.setType("text/plain");
        tweetIntent.putExtra(Intent.EXTRA_TEXT,  offerCard.name + "\n" + offerCard.title /*+ "\n" + offerCard.subtitle*/);
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(tweetIntent);
        } else {
            Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();              // TODO Add Alert?
            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
            marketIntent.setData(Uri.parse(getString(R.string.twitter_url_market)));
            startActivity(marketIntent);
        }

    }

    protected Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void showLoader() {
        new ProgressDialogFragment().show(getSupportFragmentManager(), ProgressDialogFragment.class.getName());
    }

    public void hideLoader() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ProgressDialogFragment.DISMISS_ACTION));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        MyApplication.activityPaused();
        super.onPause();
    }

    private List<OfferCard> cleanOfferFromDevice() {
        long validTime = System.currentTimeMillis() - CPConstan.VALID_TIME_OFFERS;
        List<OfferCard> offersDeviceCardList = new ArrayList<>();

        Iterator<DeviceByOffers> iter = CPGlobalData.getInstance().offersFromDevice.iterator();
        while (iter.hasNext()) {
            DeviceByOffers offerByDevice = iter.next();
            if (offerByDevice.boundsInState && validTime < offerByDevice.boundsInMillisecond) {     //TODO check offer validTime
                if (!offerByDevice.visibleState) {                                                  //TODO check offer visibleState
                    offerByDevice.visibleState = true;
                    offersDeviceCardList.add(new OfferCard(offerByDevice.offer));
                }
            } else {
                iter.remove();
            }
        }
        return offersDeviceCardList;
    }

    public void checkOfferFromDevice(final Fragment mapOfferFragment) {

        CPGlobalData globalData = CPGlobalData.getInstance();

        if (globalData.offersFromDevice != null) {                               //TODO FIX WIFI
            if (!globalData.offersFromDevice.isEmpty()) {

                logD("checkOfferFromDevice " + globalData.offersFromDevice.size());

                List<OfferCard> offersDeviceCardList = cleanOfferFromDevice();

                if (!offersDeviceCardList.isEmpty()) {
                    globalData.isOfferFromDevice = true;
                    if (globalData.offersCardList != null) {

                        for (int i = 0; i < offersDeviceCardList.size(); i++) {
                            globalData.offersCardList.add(offersDeviceCardList.get(i));
                            logD("globalData.offersCardList.add(offers) (for)" + (i));
                        }
                    } else {
                        globalData.offersCardList = offersDeviceCardList;
                        logD("globalData.offersCardList = (offers) " + offersDeviceCardList.size());
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startFragment(mapOfferFragment);                                                           //TODO Offers By Device
                        }
                    }, 100);
                }
            }
        }
    }
}
