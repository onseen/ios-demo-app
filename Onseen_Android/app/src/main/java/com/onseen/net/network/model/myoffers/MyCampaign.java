package com.onseen.net.network.model.myoffers;

import com.onseen.net.network.model.getoffers.Markers;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyCampaign {
    @SerializedName("id")
    public String id;
    @SerializedName("userId")
    public String userId;
    @SerializedName("accountId")
    public String accountId;
    @SerializedName("accountName")
    public String accountName;
    @SerializedName("name")
    public String name;
    @SerializedName("effectiveDate")
    public String effectiveDate;
    @SerializedName("expirationDate")
    public String expirationDate;
    @SerializedName("description")
    public String description;
    @SerializedName("categories")
    public String categories;
    @SerializedName("ages")
    public String ages;
    @SerializedName("media")
    public List<String> media = new ArrayList<String>();
    @SerializedName("like")
    public Boolean like;
    @SerializedName("markers")
    public List<Markers> markers = new ArrayList<Markers>();
    @SerializedName("campaignId")
    public String campaignId;
    @SerializedName("applicationId")
    public String applicationId;
    @SerializedName("deviceId")
    public String deviceId;
}
