
package com.onseen.net.network.model;

import com.google.gson.annotations.SerializedName;

public class DeviceRegModel {
    @SerializedName("success")
    public Boolean success;
    @SerializedName("data")
    public Data data;
    @SerializedName("error")
    public String error;

        public class Data {
            @SerializedName("id")
            public String id;
            @SerializedName("type")
            public String type;
            @SerializedName("udid")
            public String udid;
            @SerializedName("imei")
            public String imei;
            @SerializedName("detail")
            public Detail detail;
        }

            public class Detail {
                @SerializedName("phone")
                public String phone;
                @SerializedName("notification")
                public Notification notification;
                @SerializedName("maxHeight")
                public Integer maxHeight;
                @SerializedName("maxWidth")
                public Integer maxWidth;
                @SerializedName("tokenId")
                public String tokenId;
                @SerializedName("name")
                public String name;
                @SerializedName("username")
                public String username;
                @SerializedName("email")
                public String email;
            }

                public class Notification {
                    @SerializedName("push")
                    public String push;
                    @SerializedName("email")
                    public String email;
                    @SerializedName("text")
                    public String text;
                }
}
