package com.onseen.net.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bottlerocketstudios.barcode.generation.ui.BarcodeView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.onseen.net.R;
import com.onseen.net.network.model.MyOffersCard;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.tools.FontAvenirConfigure;

import java.util.Random;


public class CardStackOffersAdapter extends ArrayAdapter<String> {

    private MyOffersCard myOffersCard;

    public CardStackOffersAdapter(Context context, MyOffersCard myOffersCard) {
        super(context, 0);
        this.myOffersCard = myOffersCard;
    }

    @Override
    public View getView(int position, final View contentView, final ViewGroup parent) {

        ImageView logoImg = (ImageView) contentView.findViewById(R.id.logo_img);
        if (myOffersCard.logoImgUrl != null && !myOffersCard.logoImgUrl.isEmpty())
            Glide.with(contentView.getContext())
                    .load(myOffersCard.logoImgUrl)
                    .crossFade()
                    .into(logoImg);

        TextView title = (TextView) contentView.findViewById(R.id.title);
        title.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
        if (!myOffersCard.title.isEmpty())
            title.setText(myOffersCard.title);
        else
            title.setVisibility(View.GONE);

        TextView subtitle = (TextView) contentView.findViewById(R.id.subtitle);
        subtitle.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
        subtitle.setText(myOffersCard.description);
        if (!myOffersCard.description.isEmpty())
            subtitle.setText(myOffersCard.description);
        else
            subtitle.setVisibility(View.GONE);

        ImageView img = (ImageView) contentView.findViewById(R.id.content_img);

        if (myOffersCard.imgUrl != null && !myOffersCard.imgUrl.isEmpty())
            Glide.with(contentView.getContext())
                    // .load(urlsAll[new Random().nextInt(urlsAll.length)]) // TODO TEST
                    .load(myOffersCard.imgUrl)
                    .fitCenter()
                    .crossFade()
                    //    .placeholder(R.drawable.placeholder)
                    //    .override(1100, 1800)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            CPGlobalData.getInstance().myUpdateButtonFragment.onUpdateButton(parent);
                            return false;
                        }
                    })
                    .into(img);

        int first = new Random().nextInt();
        int second = new Random().nextInt();
        String barcodeString = String.valueOf(Math.abs(first + second));                 // TODO TEST RANDOM

        BarcodeView barcodeView = (BarcodeView) contentView.findViewById(R.id.generation_barcode_image);
        barcodeView.setBarcodeText(barcodeString); // "1690593088791"

        TextView barcodeText = (TextView) contentView.findViewById(R.id.barcode_text);
        barcodeText.setText(barcodeString); // "1690593088791"
        barcodeText.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);

        CPGlobalData.getInstance().myUpdateButtonFragment.onUpdateButton(parent);

        return contentView;
    }

}
