package com.onseen.net.network.model;

import com.onseen.net.network.model.getoffers.Campaign;
import com.onseen.net.network.model.getoffers.Offer;

import java.util.ArrayList;
import java.util.List;

/**
 * Card Campaigns and Offers model
 */

public class OfferCard {

    public OfferCard() {                                      // TODO FOR TEST
        this.isCampaignType = false;
        this.isLike = false;
        this.optIn = false;
        this.mCampaign = null;
        this.mOffersList = null;
        this.mOffer = new Offer();
        this.media = "";
        this.name = "";
        this.title = "";
        this.subtitle = "";
        this.imgUrl = "";
        this.campaignId = "";
        this.offerId = "";
    }

    public boolean isCampaignType;
    public boolean isLike;
    public boolean optIn;

    public Campaign mCampaign;
    public List<Offer> mOffersList;

    public Offer mOffer;

    public String media;
    public String name;
    public String title;
    public String subtitle;
    public String imgUrl;

    public String campaignId;
    public String offerId;

    public double latitude;
    public double longitude;


    public OfferCard(Campaign campaign) {
        try {
            this.isCampaignType = true;
            this.mCampaign = campaign;
            this.isLike = campaign.like;

            if (campaign.offers != null && campaign.offers.size() > 0) {
                mOffersList = new ArrayList<>();
                for (int i = 0; i < campaign.offers.size(); i++) {
                    mOffersList.add(campaign.offers.get(i));
                }
            } else {
                this.mOffersList = null;
            }

            if (campaign.account != null && campaign.account.media != null && campaign.account.media.length() > 0)
                this.media = campaign.account.media;


            if (campaign.account != null && campaign.account.name != null && campaign.account.name.length() > 0)
                this.name = campaign.account.name;
            else
                this.name = "";

            this.title = campaign.name;
            this.subtitle = campaign.description;

            if (campaign.media != null && campaign.media.size() > 0)
                this.imgUrl = campaign.media.get(0);

            this.campaignId = campaign.campaignId;

            if (campaign.markers != null && campaign.markers.size() > 0) {
                this.latitude = campaign.markers.get(0).lat;
                this.longitude = campaign.markers.get(0).lng;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public OfferCard(Offer offer) {
        try {
            this.isCampaignType = false;
            this.mOffer = offer;
            this.optIn = offer.optIn;
            this.mOffersList = null;

            if (offer.account != null && offer.account.media != null && offer.account.media.length() > 0)
                this.media = offer.account.media;

            if (offer.account != null && offer.account.name != null && offer.account.name.length() > 0)
                this.name = offer.account.name;
            
            this.title = offer.name;
            this.subtitle = offer.description;

            if (offer.media != null && offer.media.size() > 0)
                this.imgUrl = offer.media.get(0);

            this.campaignId = offer.campaignId;
            this.offerId = offer.offerId;

            if (offer.markers != null && offer.markers.size() > 0) {
                this.latitude = offer.markers.get(0).lat;
                this.longitude = offer.markers.get(0).lng;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
