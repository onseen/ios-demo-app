package com.onseen.net.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onseen.net.R;
import com.onseen.net.network.model.MarkerCard;
import com.onseen.net.network.model.getoffers.Campaign;
import com.onseen.net.network.model.getoffers.Offer;
import com.onseen.net.tools.FontAvenirConfigure;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import static com.onseen.net.tools.FontAvenirConfigure.setFontViewGroup;


public class InfoAdapter implements GoogleMap.InfoWindowAdapter {
    private LayoutInflater inflater = null;
    private Campaign campaign;
    private Offer offer;
    boolean isCampaignType;
    private List<MarkerCard> markerCardsList;
    private MarkerCard markerCard;

    public InfoAdapter(LayoutInflater inflater, List<MarkerCard> markerCardsList) {
        this.inflater = inflater;
        this.markerCardsList = markerCardsList;
        this.isCampaignType = true;
    }

    public InfoAdapter(LayoutInflater inflater, Offer offer) {
        this.inflater = inflater;
        this.offer = offer;
        this.isCampaignType = false;
    }

    public InfoAdapter(LayoutInflater inflater, Campaign campaign) {
        this.inflater = inflater;
        this.campaign = campaign;
        this.isCampaignType = true;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        if (marker != null) {
            View view = null;

            for (int i = 0; i < markerCardsList.size(); i++) {
                if (markerCardsList.get(i).markerId.equals(marker.getId()))
                    markerCard = markerCardsList.get(i);
            }
            if (markerCard != null) {
                if (markerCard.isCampaignType) {
                    view = inflater.inflate(R.layout.marker_campaigns_layout, null);

                    TextView companyTextView = (TextView) view.findViewById(R.id.title);
                    if (markerCard.mCampaign.account != null && markerCard.mCampaign.account.name != null && markerCard.mCampaign.account.name.length() > 0)
                        companyTextView.setText(markerCard.mCampaign.account.name);
                    else
                        companyTextView.setVisibility(View.GONE);


                    TextView descriptionTextView = (TextView) view.findViewById(R.id.subtitle);
                    if (markerCard.mCampaign.name != null && markerCard.mCampaign.name.length() > 0)
                        descriptionTextView.setText(markerCard.mCampaign.name);
                    else
                        descriptionTextView.setVisibility(View.GONE);

                    TextView offerTextView = (TextView) view.findViewById(R.id.description);
/*                    if (markerCard.mCampaign.description != null && markerCard.mCampaign.description.length() > 0)
                        offerTextView.setText(markerCard.mCampaign.description);
                    else*/
                        offerTextView.setVisibility(View.GONE);

                    TextView addressTextView = (TextView) view.findViewById(R.id.address);
                    // addressTextView.setText(markerCard.mCampaign.address);
                    addressTextView.setVisibility(View.GONE);                                        //TODO WHITE ADDRESS

                } else {

                    view = inflater.inflate(R.layout.marker_offer_layout, null);

                    TextView companyTextView = (TextView) view.findViewById(R.id.title);
                    if (markerCard.mOffer.account != null && markerCard.mOffer.account.name != null && markerCard.mOffer.account.name.length() > 0)
                        companyTextView.setText(markerCard.mOffer.account.name);
                    else
                        companyTextView.setVisibility(View.GONE);

                    TextView descriptionTextView = (TextView) view.findViewById(R.id.subtitle);
                    if (markerCard.mOffer.description != null && markerCard.mOffer.description.length() > 0)
                        descriptionTextView.setText(markerCard.mOffer.description);
                    else
                        descriptionTextView.setVisibility(View.GONE);

                    TextView offerTextView = (TextView) view.findViewById(R.id.description);
                    if (markerCard.mOffer.name != null && markerCard.mOffer.name.length() > 0)
                        offerTextView.setText(markerCard.mOffer.name);
                    else
                        offerTextView.setVisibility(View.GONE);


                    TextView addressTextView = (TextView) view.findViewById(R.id.address);
                    // addressTextView.setText(markerCard.mCampaign.address);
                    addressTextView.setVisibility(View.GONE);                                        //TODO WHITE ADDRESS

                }
                setFontViewGroup((ViewGroup) view, FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
            }
            return (view);
        }
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return (null);
    }
}
