package com.onseen.net.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.kyleduo.switchbutton.SwitchButton;
import com.mvc.imagepicker.ImagePicker;
import com.onseen.net.MyApplication;
import com.onseen.net.R;
import com.onseen.net.network.NetworkCallBack;
import com.onseen.net.network.NetworkCallBackImage;
import com.onseen.net.network.NetworkController;
import com.onseen.net.network.model.DeviceRegModel;
import com.onseen.net.network.model.OfferCard;
import com.onseen.net.network.model.getoffers.Offer;
import com.onseen.net.tools.AmazonS3ImageUpload;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.tools.FontAvenirConfigure;
import com.onseen.net.tools.sharprefutil.SharedPrefManager;
import com.onseen.net.ui.fragment.MapOfferDialogFragment;

import java.util.HashMap;
import java.util.Map;

import static com.onseen.net.tools.AmazonS3ImageUpload.BASE_PHOTO_URL;
import static com.onseen.net.tools.FontAvenirConfigure.setFontTextView;
import static com.onseen.net.tools.FontAvenirConfigure.setFontViewGroup;
import static com.onseen.net.ui.MapsActivity.BROADCAST_FOUND_OFFER;

public class ProfileActivity extends BaseActivity {

    private ImageView mUserIconImg;

    private EditText mNameEditText;
    private EditText mUsernameEditText;
    private EditText mEmailEditText;
    private SwitchButton /*mPushNotificationsSwitch,*/ mSmsNotificationsSwitch, mEmailNotificationsSwitch;
    private Button mSaveChangeButton;

    private String mNameString = "";
    private String mUsernameString = "";
    private String mEmailString = "";

//    private boolean mPushNotificationsState;
    private boolean mSmsNotificationsState;
    private boolean mEmailNotificationsState;

    private SharedPrefManager sharedPrefManager;
    private MapOfferDialogFragment mapOfferFragment;
    private IntentFilter intentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean mapOfferFragmentState;
    private String id;

    private Map<String, String> params;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(getLayoutResourceId());

        setFontViewGroup((ViewGroup) findViewById(R.id.profile_layout), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
        setFontTextView((TextView) findViewById(R.id.notifications_text_view), FontAvenirConfigure.FONT_AVENIR_NEXT_BOLD);

        mapOfferFragment = MapOfferDialogFragment.newInstance();
        sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(this);

        initReceiver();

        initView();

        initUserImage();

        getSharedPrefData();

    }

    private void initView() {
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mUserIconImg = (ImageView) findViewById(R.id.user_icon_img);
        mUserIconImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.pickImage(ProfileActivity.this, "Select your image:");
            }
        });

        mNameEditText = (EditText) findViewById(R.id.name_edit_text);
        mUsernameEditText = (EditText) findViewById(R.id.username_edit_text);
        mEmailEditText = (EditText) findViewById(R.id.email_edit_text);

//        mPushNotificationsSwitch = (SwitchButton) findViewById(R.id.push_notifications_switch);
        mSmsNotificationsSwitch = (SwitchButton) findViewById(R.id.sms_notifications_switch);
        mEmailNotificationsSwitch = (SwitchButton) findViewById(R.id.email_notifications_switch);
//        mPushNotificationsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
//                mPushNotificationsState = state;
//            }
//        });
        mSmsNotificationsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                mSmsNotificationsState = state;
            }
        });
        mEmailNotificationsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                mEmailNotificationsState = state;
            }
        });

        mSaveChangeButton = (Button) findViewById(R.id.save_change_button);
        mSaveChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRegisterData();
                long currentTimeMillis = System.currentTimeMillis();
                uploadImageToAmazonS3(CPGlobalData.getInstance().getImageCropFilePath(), currentTimeMillis);
                //   startFragment(mapOfferFragment);                         // TODO TEST
            }
        });

    }

    private void uploadImageToAmazonS3(String filePath, long currentTimeMillis) {
        if (filePath != null && !filePath.isEmpty()) {
            AmazonS3ImageUpload imageUpload = new AmazonS3ImageUpload(filePath, currentTimeMillis);
            imageUpload.execute();

            saveUserPhotoUrl(currentTimeMillis);
        }
    }

    private void saveUserPhotoUrl(long currentTimeMillis) {
        String PHOTO_URL = BASE_PHOTO_URL + NetworkController.getInstance().getDeviceId()+ "_" + currentTimeMillis + ".jpg";
        sharedPrefManager.setUserPhotoUrl(PHOTO_URL);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
            if (bitmap != null) {
                CPGlobalData.getInstance().setPickerBitmap(bitmap);
                startActivity(new Intent(this, ImageCropActivity.class));
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.error_user_img, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void initUserImage() {
        if (CPGlobalData.getInstance().getCroppedBitmap() != null && mUserIconImg != null) {
            mUserIconImg.setImageBitmap(CPGlobalData.getInstance().getCroppedBitmap());
        } else if (!sharedPrefManager.getUserPhotoUrl().isEmpty()) {
            Glide.with(this).load(sharedPrefManager.getUserPhotoUrl()).into(mUserIconImg);
        }
    }

    private void sendUpdateDevice(Map<String, String> params) {
        id = sharedPrefManager.getIdString();
        if (id != null && !id.equals("-1")) {
            showLoader();
            NetworkController.getInstance().sendUpdateDeviceRequest(this, params, new NetworkCallBack() {
                @Override
                public void onCompleteString(String jsonString) {
                    hideLoader();
                    setSharedPrefDataFromResponse(jsonString);
                    getSharedPrefData();
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    hideLoader();
                    //    Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_LONG).show(); // TODO DEBUG
                    //  makeToast(ProfileActivity.this, "Data not save.");
                }
            });
        }
    }

    private void setSharedPrefDataFromResponse(String jsonString) {
        if (!jsonString.isEmpty()) {
            try {
                DeviceRegModel deviceModel = new Gson().fromJson(jsonString, DeviceRegModel.class);

                sharedPrefManager.setNameString(deviceModel.data.detail.name);
                sharedPrefManager.setUsernameString(deviceModel.data.detail.username);
                sharedPrefManager.setEmailString(deviceModel.data.detail.email);

                sharedPrefManager.setPushNotificationsState(convertYesNoToBoolean(deviceModel.data.detail.notification.push));
                sharedPrefManager.setSmsNotificationsState(convertYesNoToBoolean(deviceModel.data.detail.notification.text));
                sharedPrefManager.setEmailNotificationsState(convertYesNoToBoolean(deviceModel.data.detail.notification.email));

                sharedPrefManager.init(this);

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public void getSharedPrefData() {
        mNameEditText.setText(sharedPrefManager.getNameString());
        mUsernameEditText.setText(sharedPrefManager.getUsernameString());
        mEmailEditText.setText(sharedPrefManager.getEmailString());

//        mPushNotificationsSwitch.setChecked(sharedPrefManager.getPushNotificationsState());
        mSmsNotificationsSwitch.setChecked(sharedPrefManager.getSmsNotificationsState());
        mEmailNotificationsSwitch.setChecked(sharedPrefManager.getEmailNotificationsState());

    }

    // Get data from EditText
    private void getRegisterData() {
        mNameString = mNameEditText.getText().toString();
        mUsernameString = mUsernameEditText.getText().toString();
        mEmailString = mEmailEditText.getText().toString();

        params = new HashMap<>();
        params.put("name", mNameString);
        params.put("username", mUsernameString);
        params.put("email", mEmailString);

//        params.put("push_n", convertBooleanToYesNo(mPushNotificationsState));
        params.put("sms_n", convertBooleanToYesNo(mSmsNotificationsState));
        params.put("email_n", convertBooleanToYesNo(mEmailNotificationsState));

        sendUpdateDevice(params);
    }

    private String convertBooleanToYesNo(boolean switchState) {
        if (switchState)
            return "yes";
        else
            return "no";
    }

    private boolean convertYesNoToBoolean(String notificationState) {
        if (notificationState.equals("yes"))
            return true;
        else
            return false;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.profile_layout;   // set layout instead of setContentView
    }

    public void initReceiver() {
        mapOfferFragment = MapOfferDialogFragment.newInstance();
        mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                checkOfferFromDevice(mapOfferFragment);                                             //TODO Offers From Device
            }
        };
        intentFilter = new IntentFilter(BROADCAST_FOUND_OFFER);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

/*    private void checkOfferByDevice() {
        if (!CPGlobalData.getInstance().offersFromDevice.isEmpty() && !firstWiFiView) {
            CPGlobalData.getInstance().isOfferFromDevice = true;
            startFragment(mapOfferFragment);
        }
    }*/


    @Override
    public void onDislikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("dislike", offerCard.campaignId);
        } else {
            sendOffersActionRequest("dislike", offerCard.offerId);
        }
    }

    @Override
    public void onLikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("like", offerCard.campaignId);
        } else {
            sendOffersActionRequest("like", offerCard.offerId);
            CPGlobalData.getInstance().myOffersListDeprecated = true;                           // My Offers List is Deprecated
        }
    }

    @Override
    public void onMaybeButtonSelected() {
        mapOfferFragmentState = false;
        endFragment(mapOfferFragment);
    }

    @Override
    public void onShareTwitterButtonSelected(final OfferCard offerCard) {

        if (offerCard.mOffer.media != null && offerCard.mOffer.media.size() > 0) {
            String imgUrl = offerCard.mOffer.media.get(0).replace(" ", "+");
            showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                    hideLoader();
                    shareTwitter(offerCard, response, true);
                }
            });
        } else {
            shareTwitter(offerCard, null, false);
        }
    }

    @Override
    public void onBackPressed() {
        if (mapOfferFragmentState) {
            mapOfferFragmentState = false;
            endFragment(mapOfferFragment);
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        registerReceiver(mBroadcastReceiver, intentFilter);
        initUserImage();
    }

    @Override
    protected void onPause() {
        MyApplication.activityPaused();
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    private void initCardList(Offer currentOffer) {                          // TODO: TEST TINDER
        OfferCard offerCard = new OfferCard(currentOffer);
        CPGlobalData.getInstance().offerCard = offerCard;
    }
}
