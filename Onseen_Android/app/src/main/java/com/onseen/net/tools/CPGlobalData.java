package com.onseen.net.tools;


import android.content.Intent;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;

import com.estimote.sdk.BeaconManager;
import com.onseen.net.network.model.DeviceByOffers;
import com.onseen.net.network.model.MarkerCard;
import com.onseen.net.network.model.MyOffersCard;
import com.onseen.net.network.model.OfferCard;
import com.onseen.net.ui.fragment.UpdateMapButtonFragment;
import com.onseen.net.ui.fragment.UpdateMyButtonFragment;

import java.util.List;

public class CPGlobalData {



    private CPGlobalData() {
    }

    private static class SingletonHelper {
        private static final CPGlobalData INSTANCE = new CPGlobalData();
    }

    public static CPGlobalData getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public BeaconManager beaconManager;
    public boolean beaconManagerIsReady;

    public boolean activityVisible;

    public String refreshedToken;

    public float mScreenHeight;
    public DisplayMetrics dm;
    public int widthDisplayPix;
    public int heightDisplayPix;

    public String mWiFiMacNew = "";
    public long mWiFiMillis;
    public String mWiFiMacOld = "";
    public String mBeaconMac;

    public List<MarkerCard> markerCardsList;
    public int indexCurrentMarker;
    public boolean isCurrentMarker;
    public boolean myOffersListDeprecated = true;

    public OfferCard offerCard;

    public boolean isOfferFromDevice;
    public List<OfferCard> offersCardList;
    public List<OfferCard> offerCards;

    public List<MyOffersCard> myOfferCardsList;

    public Intent BTServiceIntent;

    public MyOffersCard myOffersCard;
    public List<DeviceByOffers> deviceByOffersList;

    public List<DeviceByOffers> offersFromDevice;

    public UpdateMapButtonFragment mapUpdateButtonFragment;
    public UpdateMyButtonFragment myUpdateButtonFragment;




    private Bitmap mPickerBitmap;
    private Bitmap mCroppedBitmap;
    private String imageCropFilePath;

    public Bitmap getCroppedBitmap() {
        return mCroppedBitmap;
    }
    public void setCroppedBitmap(Bitmap mCroppedBitmap) {
        this.mCroppedBitmap = mCroppedBitmap;
    }

    public Bitmap getPickerBitmap() {
        return mPickerBitmap;
    }
    public void setPickerBitmap(Bitmap mPickerBitmap) {
        this.mPickerBitmap = mPickerBitmap;
    }

    public String getImageCropFilePath() {
        return imageCropFilePath;
    }
    public void setImageCropFilePath(String imageCropFilePath) {
        this.imageCropFilePath = imageCropFilePath;
    }
}

