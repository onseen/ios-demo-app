
package com.onseen.net.network.model;

import com.google.gson.annotations.SerializedName;
import com.onseen.net.network.model.getoffers.Offer;

import java.util.ArrayList;
import java.util.List;

/**                            ^ Markers   ^ Markers
 * MyOffersListResponseModel -> Offer ->  DeviceOffers
 */
public class GPSOffersListResponseModel {

    @SerializedName("success")
    public Boolean success;
    @SerializedName("data")
    public List<Offer> data = new ArrayList<Offer>();
    @SerializedName("error")
    public String error;

}
