package com.onseen.net.tools.gpsutil;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.maps.android.SphericalUtil;
import com.onseen.net.MyApplication;
import com.onseen.net.R;
import com.onseen.net.network.NetworkCallBack;
import com.onseen.net.network.NetworkController;
import com.onseen.net.network.model.DeviceByOffers;
import com.onseen.net.network.model.GPSOffersListResponseModel;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.ui.MapsActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.onseen.net.MyApplication.logD;
import static com.onseen.net.tools.CPConstan.MIN_DISTANCE_CHANGE_FOR_UPDATES;
import static com.onseen.net.tools.CPConstan.MIN_TIME_BW_UPDATES;
import static com.onseen.net.tools.CPConstan.VALID_OFFERS_DISTANCE;
import static com.onseen.net.tools.CPConstan.VALID_TIME_OFFERS;

/**
 * GPS Tracker Service
 */

public class GPSService extends Service implements android.location.LocationListener {

    private final Context mContext = this;

    boolean isGPSEnabled = false; // flag for GPS status
    boolean isNetworkEnabled = false;// flag for network status
    boolean isPassiveEnabled = false; // flag for network status
    boolean canGetLocation = false; // flag for GPS status
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // Declaring a Location Manager
    protected LocationManager locationManager;

    // IntentFilter
    private final static String BROADCAST_GPS_ACTION = "com.example.java.googlemapsapi";

    private List<DeviceByOffers> deviceByOffersList;
    private List<DeviceByOffers> narestOfferFoundsList;
    private List<DeviceByOffers> offersFromDevice;
    private LatLng lastLocation;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        logD("GPS", "Start GPSService");

        offersFromDevice = CPGlobalData.getInstance().offersFromDevice;
        if (offersFromDevice == null) {
            offersFromDevice = new ArrayList<>();
            CPGlobalData.getInstance().offersFromDevice = offersFromDevice;
        }

        getLocation();

        return START_STICKY;
    }

    private Location getLocation() {
        logD("GPS", "GPS Service getLocation");
        try {
            if (locationManager == null)
                locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            if (locationManager != null)
                setLocationProviders();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    private void setLocationProvidersOld() {
        isPassiveEnabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER); // getting passive status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER); // getting network status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);         // getting GPS status

        if (isGPSEnabled || isNetworkEnabled) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e("GPS", "Not GPS permission");
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider (latitude: 48.461783,  longitude: 35.0439667)
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    logD("GPS", "Network Enabled");
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    logD("GPS", "GPS Enabled");
                }
            }
        } else if (isPassiveEnabled) {
            // Default get location from Passive Provider
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            logD("GPS", "Passive GPS Enabled");
        }
    }

    private void setLocationProviders() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("GPS", "Not GPS permission");
        } else {
            logD("GPS", "Passive GPS Enabled"); // Default get location from Passive Provider
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            logD("GPS", "Network Enabled"); // First get location from Network Provider (latitude: 48.461783,  longitude: 35.0439667)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            logD("GPS", "GPS Enabled"); // if GPS Enabled get lat/long using GPS Services
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        logD("GPS", "GPS Service onCreate OOOOO");
    }

    @Override
    public void onDestroy() {
        stopUsingGPS();
        logD("GPS", "GPS Service onDestroy XXXXX");
        super.onDestroy();
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    private boolean stopUsingGPS() {
        if (locationManager != null) {
            int FINE = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
            int COARSE = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (FINE == PackageManager.PERMISSION_GRANTED && COARSE == PackageManager.PERMISSION_GRANTED) {
                locationManager.removeUpdates(this);

                stopSelf();
                return true;
            }
        }
        return false;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            updateLastKnownLocation(location.getProvider());
            getValidOffer();
        }
    }

    private void getValidOffer() {

        if (deviceByOffersList == null || CPGlobalData.getInstance().myOffersListDeprecated)
            getMyOffersList(lastLocation);
        else
            checkListDevice(lastLocation);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        logD("GPS", "GPS onStatusChanged " + s);
    }

    @Override
    public void onProviderEnabled(String s) {
        setLocationProvidersOld();  //// TODO: 16.12.2016
        logD("GPS", "GPS onProviderEnabled " + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        logD("GPS", "GPS onProviderDisabled " + s);
    }

    private LatLng updateLastKnownLocation(String locationManager_PROVIDER) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("GPS", "Not GPS permission");
        } else {
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(locationManager_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    LatLng lastKnownLocation = new LatLng(latitude, longitude);
                    lastLocation = new LatLng(latitude, longitude);
                    return lastKnownLocation;
                }
                logD("GPS", "GPS onLocationChanged  latitude: " + latitude + ",  " + "longitude: " + longitude + ",  " + location.getProvider());
            }
        }
        return null;
    }


    /**
     * Returns distance on the unit sphere; the arguments are in radians.
     */
    private double getDistance(LatLng offerLatLng, LatLng locationLatLng) {
        return SphericalUtil.computeDistanceBetween(offerLatLng, locationLatLng);
    }


    private void getMyOffersList(final LatLng locationLatLng) {
        NetworkController.getInstance().sendOffersRequest(this, "", new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {

              //  jsonString = JSON_MY_OFFER_TEST;// TODO TEST_JSON

                GPSOffersListResponseModel model = new Gson().fromJson(jsonString, GPSOffersListResponseModel.class);
                deviceByOffersList = new ArrayList<DeviceByOffers>();

                if (model != null && model.success && !model.data.isEmpty()) {
                    for (int i = 0; i < model.data.size(); i++) {
                        deviceByOffersList.add(new DeviceByOffers(model.data.get(i)));
                    }
                    CPGlobalData.getInstance().myOffersListDeprecated = false;
                    checkListDevice(locationLatLng);

                } else if (model.error != null) {
                    //    Toast.makeText(getApplicationContext(), "" + model.error, Toast.LENGTH_SHORT).show();// TODO DEBUG
                }

            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                //  Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show(); // TODO DEBUG
            }
        });
    }

    private void checkListDevice(final LatLng locationLatLng) {
        if (!deviceByOffersList.isEmpty()) {
            if (foundNearestOffers(locationLatLng)) {
                if (!narestOfferFoundsList.isEmpty()) {
                    checkFoundsOffersAndSendBroadcast();
                }
            }
        }
    }



    // found all offers if equals UUID from beacon
    private boolean foundNearestOffers(final LatLng locationLatLng) {
        narestOfferFoundsList = new ArrayList<>();
        boolean found = false;

        if (deviceByOffersList != null && !deviceByOffersList.isEmpty()) {
            for (int i = 0; i < deviceByOffersList.size(); i++) {
                DeviceByOffers deviceByMyOffer = deviceByOffersList.get(i);
                if (deviceByMyOffer.location && deviceByMyOffer.offerLatLng != null) {

                    double distance = getDistance(deviceByMyOffer.offerLatLng, locationLatLng);
                    if (distance < VALID_OFFERS_DISTANCE) {
                        deviceByMyOffer.boundsInState = true;
                        deviceByMyOffer.boundsOutState = false;
                        deviceByMyOffer.boundsInMillisecond = System.currentTimeMillis();
                        narestOfferFoundsList.add(deviceByMyOffer);
                        found = true;
                        logD("GPS", "Found Nearest Offer");
                    }
                    logD("GPS", "i:" + i + " GPS distance: " + distance);
                }
            }
        }
        return found;
    }

    private void checkFoundsOffersAndSendBroadcast() {
        boolean offerFoundState = false;
        cleanOfferFromDevice();
        for (int i = 0; i < narestOfferFoundsList.size(); i++) {
            if (containsOfferId(narestOfferFoundsList.get(i).offerId)) {
                logD("GPS", "OffersFromDevice contains OfferId");
            } else {
                logD("GPS", "OffersFromDevice add OfferId " + narestOfferFoundsList.get(i).offerId);
                offerFoundState = true;
                offersFromDevice.add(narestOfferFoundsList.get(i));                                                     // add if not equals OfferId
            }
        }
        if (offerFoundState)
            sendOfferBroadcast();                                                                        // send if found new valid offer
    }

    private void cleanOfferFromDevice() {
        long validTime = System.currentTimeMillis() - VALID_TIME_OFFERS;
        Iterator<DeviceByOffers> iterator = CPGlobalData.getInstance().offersFromDevice.iterator();
        while (iterator.hasNext()) {
            DeviceByOffers offerByDevice = iterator.next();
            if (validTime > offerByDevice.boundsInMillisecond) {     //TODO check offer validTime
                iterator.remove();
            }
        }
    }

    private boolean containsOfferId(String offerId) {
        for (int i = 0; i < offersFromDevice.size(); i++) {
            if (offersFromDevice.get(i).offerId.equalsIgnoreCase(offerId))
                return true;
        }
        return false;
    }

    private void sendOfferBroadcast() {
        if (!CPGlobalData.getInstance().activityVisible) {
            MyApplication.showNotificationBeacon(mContext.getString(R.string.app_name), mContext.getString(R.string.offer_notification));
        } else {
            Intent intent = new Intent(MapsActivity.BROADCAST_FOUND_OFFER);
            intent.putExtra("indexFromService", "bt");
            mContext.sendBroadcast(intent);
        }
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    private void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
