package com.onseen.net.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;
import com.onseen.net.MyApplication;
import com.onseen.net.R;
import com.onseen.net.network.NetworkCallBack;
import com.onseen.net.network.NetworkCallBackImage;
import com.onseen.net.network.NetworkController;
import com.onseen.net.network.model.DeviceByOffers;
import com.onseen.net.network.model.GPSOffersListResponseModel;
import com.onseen.net.network.model.MyOffersCard;
import com.onseen.net.network.model.OfferCard;
import com.onseen.net.network.model.getoffers.Offer;
import com.onseen.net.network.model.myoffers.MyOffers;
import com.onseen.net.network.model.myoffers.MyOffersListResponseModel;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.tools.FontAvenirConfigure;
import com.onseen.net.ui.adapter.ItemClickListener;
import com.onseen.net.ui.adapter.OffersListAdapter;
import com.onseen.net.ui.fragment.MapOfferDialogFragment;
import com.onseen.net.ui.fragment.MyOfferDialogFragment;

import java.util.ArrayList;
import java.util.List;

import static com.onseen.net.tools.FontAvenirConfigure.setFontViewGroup;
import static com.onseen.net.ui.MapsActivity.BROADCAST_FOUND_OFFER;

public class MyOffersListActivity extends BaseActivity implements View.OnClickListener, ItemClickListener, MyOfferDialogFragment.OnOfferDialogFragmentSelectedListener {

    private OffersListAdapter mAdapter;
    private ImageButton backBtn, profileBtn;
    private MyOfferDialogFragment offerFragment;
    private MapOfferDialogFragment mapOfferFragment;
    private boolean offerFragmentState;
    private List<MyOffersCard> myOfferCardsList;

    private RecyclerView mRecyclerView;
    private IntentFilter intentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private RecyclerTouchListener onTouchListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(getLayoutResourceId());
        setFontViewGroup((ViewGroup) findViewById(R.id.offers_list_layout), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR); // TODO

        initView();

        initRecycler();

        notifyRecyclerView(); // notify RecyclerView Data

        initReceiver();

        checkMyOfferList();

    }

    private void checkMyOfferList() {
        if (CPGlobalData.getInstance().myOffersListDeprecated || myOfferCardsList.isEmpty())
            getMyOffersList();
        else if (myOfferCardsList != null && !myOfferCardsList.isEmpty()) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.offers_list_layout;          // set layout instead of setContentView
    }

    private void initView() {
        backBtn = (ImageButton) findViewById(R.id.back_btn);
        profileBtn = (ImageButton) findViewById(R.id.profile_btn);

        backBtn.setOnClickListener(this);
        profileBtn.setOnClickListener(this);

        offerFragment = MyOfferDialogFragment.newInstance();

        mapOfferFragment = MapOfferDialogFragment.newInstance();
    }

    private void initRecycler() {
        mRecyclerView = (RecyclerView) findViewById(R.id.offers_list_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        if (CPGlobalData.getInstance().myOfferCardsList == null) {
            myOfferCardsList = new ArrayList<MyOffersCard>();
            CPGlobalData.getInstance().myOfferCardsList = myOfferCardsList;
        } else {
            myOfferCardsList = CPGlobalData.getInstance().myOfferCardsList;
        }

        // create adapter
        mAdapter = new OffersListAdapter(this, myOfferCardsList);
        mRecyclerView.setAdapter(mAdapter);

        // click on RecyclerView
        mAdapter.setClickListener(this);


        onTouchListener = new RecyclerTouchListener(this, mRecyclerView);
        onTouchListener
                .setSwipeOptionViews(R.id.removeCard)
                .setSwipeable(R.id.rowFG, R.id.rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                    @Override
                    public void onSwipeOptionClicked(int viewID, int position) {
                        if (viewID == R.id.removeCard) {
                            sendOffersActionRequest("dislike", myOfferCardsList.get(position).offerId);
                            myOfferCardsList.remove(position);
                            notifyRecyclerView();
                        }
                    }
                });

        mRecyclerView.addOnItemTouchListener(onTouchListener);
    }

    private void getMyOffersList() {
        showLoader();
        NetworkController.getInstance().sendOffersRequest(this, "", new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
                hideLoader();
                //      jsonString = JSON_MY_OFFER_TEST;                                                                         // TODO TEST_JSON
                try {
                    MyOffersListResponseModel model = new Gson().fromJson(jsonString, MyOffersListResponseModel.class);
                    if (model != null && model.success && !model.data.isEmpty()) {
                        if (myOfferCardsList != null)
                            myOfferCardsList.clear();
                        for (int i = 0; i < model.data.size(); i++) {
                            myOfferCardsList.add(new MyOffersCard(model.data.get(i)));
                        }
                        mAdapter.notifyDataSetChanged();
                    } else if (model.error != null) {
                        //    Toast.makeText(getApplicationContext(), "" + model.error, Toast.LENGTH_SHORT).show();// TODO DEBUG
                    }


                    GPSOffersListResponseModel modelForLocation = new Gson().fromJson(jsonString, GPSOffersListResponseModel.class);
                    if (modelForLocation != null && modelForLocation.success && !modelForLocation.data.isEmpty()) {
                        ArrayList<DeviceByOffers> deviceByOffersList = new ArrayList<DeviceByOffers>();
                        for (int i = 0; i < modelForLocation.data.size(); i++) {
                            deviceByOffersList.add(new DeviceByOffers(modelForLocation.data.get(i)));
                        }
                        CPGlobalData.getInstance().myOffersListDeprecated = false;
                        CPGlobalData.getInstance().deviceByOffersList = deviceByOffersList;
                    } else if (modelForLocation.error != null) {
                        //    Toast.makeText(getApplicationContext(), "" + model.error, Toast.LENGTH_SHORT).show();// TODO DEBUG
                    }

                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                //  Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show(); // TODO DEBUG
                hideLoader();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.profile_btn:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
        }

    }


    @Override
    public void onCloseButtonSelected(MyOffersCard myOffersCard) {
        endFragment(offerFragment);
        offerFragmentState = false;
    }

    @Override
    public void onShareButtonSelected(MyOffersCard myOffersCard) {
        shareMyOffer(myOffersCard);
    }

    @Override
    public void onDislikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("dislike", offerCard.campaignId);
        } else {
            sendOffersActionRequest("dislike", offerCard.offerId);
        }
    }

    @Override
    public void onLikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("like", offerCard.campaignId);
        } else {
            sendOffersActionRequest("like", offerCard.offerId);
            CPGlobalData.getInstance().myOffersListDeprecated = true;                           // My Offers List is Deprecated
        }
    }

    @Override
    public void onMaybeButtonSelected() {
        endFragment(mapOfferFragment);
    }

    @Override
    public void onShareTwitterButtonSelected(final OfferCard offerCard) {

        if (offerCard.mOffer.media != null && offerCard.mOffer.media.size() > 0) {
            String imgUrl = offerCard.mOffer.media.get(0).replace(" ", "+");
            showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                    hideLoader();
                    shareTwitter(offerCard, response, true);
                }
            });
        } else {
            shareTwitter(offerCard, null, false);
        }
    }


    @Override
    public void onBackPressed() {
        if (offerFragmentState) {
            offerFragmentState = false;
            endFragment(offerFragment);
            endFragment(mapOfferFragment);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        MyApplication.activityPaused();
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    public void initReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                checkOfferFromDevice(mapOfferFragment);                                             //TODO Offers From Device
            }
        };
        intentFilter = new IntentFilter(BROADCAST_FOUND_OFFER);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void initCardList(Offer currentOffer) {                          // TODO: TEST TINDER
        OfferCard offerCard = new OfferCard(currentOffer);
        CPGlobalData.getInstance().offerCard = offerCard;
    }

    @Override
    public void onItemClick(View view, int position, MyOffersCard myOffersCard) {
        // RecyclerView Click here
//        goToGoogleMapsNavigator(position);
//        onButtonClick(view, position, myOffersCard);
    }

    private void goToGoogleMapsNavigator(int position) {
        if (myOfferCardsList != null) {
            MyOffers myOffer = myOfferCardsList.get(position).mOffer;
            if (myOffer.markers != null && !myOffer.markers.isEmpty()
                    && myOffer.markers.get(0).lat != null && myOffer.markers.get(0).lng != null) {
                String navigationTo = "google.navigation:q=" + myOffer.markers.get(0).lat + ", " + myOffer.markers.get(0).lng;
                Uri gmmIntentUri = Uri.parse(navigationTo);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            } else {
                Toast.makeText(this, R.string.route_not_found, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onButtonClick(View view, int position, MyOffersCard myOffersCard) {
        if (myOffersCard != null) {
            CPGlobalData.getInstance().myOffersCard = myOffersCard;
            startFragment(offerFragment);
            offerFragmentState = true;
        }
    }

    private void shareMyOffer(final MyOffersCard myOffersCard) {
        if (myOffersCard.imgUrl != null && !myOffersCard.imgUrl.isEmpty()) {
            String imgUrl = myOffersCard.imgUrl.replace(" ", "+");
            showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                    hideLoader();
                    final Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    String textToSend = myOffersCard.title + "\n" + myOffersCard.description;
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(response));
                    String textTitle = getResources().getString(R.string.share_title);
                    String textError = getResources().getString(R.string.share_error);
                    intent.putExtra(Intent.EXTRA_TEXT, textToSend);
                    try {
                        startActivity(Intent.createChooser(intent, textTitle));
                    } catch (android.content.ActivityNotFoundException ex) {
                        //    Toast.makeText(getApplicationContext(), textError, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (myOffersCard.title != null && myOffersCard.description != null) {
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            String textToSend = myOffersCard.title + "\n" + myOffersCard.description;
            String textTitle = getResources().getString(R.string.share_title);
            String textError = getResources().getString(R.string.share_error);
            intent.putExtra(Intent.EXTRA_TEXT, textToSend);
            try {
                startActivity(Intent.createChooser(intent, textTitle));
            } catch (android.content.ActivityNotFoundException ex) {
                //    Toast.makeText(getApplicationContext(), textError, Toast.LENGTH_SHORT).show();
            }
        }
    }

    // notify RecyclerView Data
    private void notifyRecyclerView() {
        mAdapter.notifyDataSetChanged();
    }

}
