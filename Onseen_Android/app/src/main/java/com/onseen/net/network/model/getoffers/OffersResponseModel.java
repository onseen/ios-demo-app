
package com.onseen.net.network.model.getoffers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**                       ^ Account   ^ Markers
 * OffersResponseModel -> Campaign -> Offer -> DeviceOffers
 *                        ^ Markers
 */

public class OffersResponseModel {

    @SerializedName("success")
    public Boolean success;
    @SerializedName("data")
    public List<Campaign> data = new ArrayList<Campaign>();
    @SerializedName("error")
    public String error;

}
