package com.onseen.net.tools.gpsutil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

public class GPSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.matches("android.location.PROVIDERS_CHANGED")) {
            LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
            // boolean gps_enabled = false;
            boolean network_enabled = false;

            try {
             //   gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch(Exception ex) {}

            if (/*gps_enabled ||*/ network_enabled) {
                context.startService(new Intent(context, GPSService.class));
            }
        }

        if (action.equals("ru.racoondeveloper.beaconnotifyer.WAKE_GPS_RECEIVER")) {
                context.startService(new Intent(context, GPSService.class));
        }
    }
}

