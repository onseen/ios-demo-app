package com.onseen.net.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;

import com.onseen.net.R;


public class ProgressDialogFragment extends DialogFragment {
    public static final String DISMISS_ACTION = "ProgressDialogFragment.DISMISS_ACTION";

    private boolean isWaitingForDismiss = false;
    private BroadcastReceiver mDismissActionReceiver;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mDismissActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                dismiss();
            }
        };

        LocalBroadcastManager.getInstance(activity)
                .registerReceiver(mDismissActionReceiver, new IntentFilter(DISMISS_ACTION));
    }

    @Override
    public void onDetach() {
        if (mDismissActionReceiver != null)
            LocalBroadcastManager.getInstance(getActivity())
                    .unregisterReceiver(mDismissActionReceiver);
        super.onDetach();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final android.app.ProgressDialog dialog = new android.app.ProgressDialog(getActivity()) {
            @Override
            public void onBackPressed() {
                // do nothing
            }
        };
        dialog.setMessage(getResources().getString(R.string.please_wait));
        dialog.setProgressDrawable(getResources().getDrawable(R.drawable.spinner_ring));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isWaitingForDismiss)
            dismiss();
    }

    @Override
    public void dismiss() {
        if(isResumed())
            super.dismiss();
        else
            isWaitingForDismiss = true;
    }
}
