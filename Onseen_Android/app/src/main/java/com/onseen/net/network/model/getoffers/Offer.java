package com.onseen.net.network.model.getoffers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**                       ^ Account   ^ Markers
 * OffersResponseModel -> Campaign -> Offer -> DeviceOffers
 *                        ^ Markers
 */

public class Offer {
    @SerializedName("id")
    public String id;
    @SerializedName("media")
    public List<String> media = new ArrayList<String>();
    @SerializedName("account")
    public Account account;
    @SerializedName("name")
    public String name;
    @SerializedName("description")
    public String description;
    @SerializedName("apikey")
    public String apikey;
    @SerializedName("userId")
    public String userId;
    @SerializedName("campaignId")
    public String campaignId;
    @SerializedName("campaignName")
    public String campaignName;
    @SerializedName("effectiveDate")
    public String effectiveDate;
    @SerializedName("expirationDate")
    public String expirationDate;
    @SerializedName("markers")
    public List<Markers> markers = new ArrayList<Markers>();
    @SerializedName("devices")
    public List<DeviceOffers> devices = new ArrayList<DeviceOffers>();
    @SerializedName("offerId")
    public String offerId;
    @SerializedName("optIn")
    public Boolean optIn;
    @SerializedName("applicationId")
    public String applicationId;
    @SerializedName("deviceId")
    public String deviceId;
}
