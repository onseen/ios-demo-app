
package com.onseen.net.network.model.getoffers;

import com.google.gson.annotations.SerializedName;

public class Account {

    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("media")
    public String media;

}
