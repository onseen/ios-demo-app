
package com.onseen.net.network.model.myoffers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
/**                             ^ Markers   ^ Markers
 * MyOffersListResponseModel -> MyOffers -> MyCampaign
 *
 */
public class MyOffersListResponseModel {

    @SerializedName("success")
    public Boolean success;
    @SerializedName("data")
    public List<MyOffers> data = new ArrayList<MyOffers>();
    @SerializedName("error")
    public String error;

}
