package com.onseen.net.network.model;

import com.onseen.net.network.model.myoffers.MyOffers;

public class MyOffersCard {

    public boolean isMyOffersType;
    public boolean optIn;

    public MyOffers mOffer;

    public String accountName;
    public String logoImgUrl;

    public String title;
    public String description;
    public String imgUrl;

    public String campaignName;
    public String campaignId;
    public String offerId;

    public double latitude;
    public double longitude;

    public MyOffersCard(MyOffers mOffer) {
        if (mOffer != null)
        try {
            this.isMyOffersType = true;
            this.mOffer = mOffer;
            this.optIn = mOffer.optIn;

            if (mOffer.account != null && mOffer.account.media != null && !mOffer.account.media.isEmpty())
                this.logoImgUrl = mOffer.account.media;

            if (mOffer.account != null && mOffer.account.name != null && !mOffer.account.name.isEmpty())
                this.accountName = mOffer.account.name;

            this.title = mOffer.name;
            this.description = mOffer.description;

            if (mOffer.media != null && !mOffer.media.isEmpty())
                this.imgUrl = mOffer.media.get(0);

            this.campaignName = mOffer.campaignName;
            this.campaignId = mOffer.campaignId;
            this.offerId = mOffer.offerId;

            if (mOffer.markers != null && !mOffer.markers.isEmpty()) {
                this.latitude = mOffer.markers.get(0).lat;
                this.longitude = mOffer.markers.get(0).lng;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
