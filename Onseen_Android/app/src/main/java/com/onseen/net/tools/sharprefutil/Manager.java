package com.onseen.net.tools.sharprefutil;

import android.content.Context;

public interface Manager {
    void init(Context context);
    void clear();
}
