package com.onseen.net.ui.fragment;

import android.view.ViewGroup;

/**
 * Created by Anton on 27.12.2016.
 */

public interface UpdateMyButtonFragment {
        void onUpdateButton(ViewGroup view);
}
