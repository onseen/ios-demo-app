package com.onseen.net.network.model;


import com.google.android.gms.maps.model.LatLng;
import com.onseen.net.network.model.getoffers.DeviceOffers;
import com.onseen.net.network.model.getoffers.Offer;

public class DeviceByOffers {

    public Offer offer;
    public DeviceOffers device;

    public boolean iBeacon;
    public boolean wifi;
    public boolean location;
    public String campaignId;
    public String offerId;
    public String deviceId;
    public String deviceType;
    public String deviceName;
    public String deviceMac;
    public String deviceUdid;

    public int major;
    public int minor;

    public double latitude;
    public double longitude;
    public LatLng offerLatLng;

    public Boolean boundsInState;
    public long boundsInMillisecond;
    public long visibleInMillisecond;
    public Boolean boundsOutState;
    public long boundsOutMillisecond;

    public long timeGetDevise;
    public Boolean visibleState;

    public DeviceByOffers(Offer offer, DeviceOffers device) {
        try {
            this.offer = offer;
            this.device = device;

            this.campaignId = offer.campaignId;
            this.offerId = offer.offerId;
            this.deviceId = offer.deviceId;

            this.deviceType = device.type;
            this.deviceName = device.deviceName;
            this.deviceMac = device.identifier.toLowerCase();
            this.deviceUdid = device.udid;
            this.timeGetDevise = System.currentTimeMillis();
            this.boundsInState = false;
            this.boundsOutState = false;
            this.visibleState = false;

            if (this.deviceType != null && !this.deviceType.isEmpty()) {
                this.iBeacon = device.type.equalsIgnoreCase("iBeacon");
                this.wifi = device.type.equalsIgnoreCase("macaddress");
                this.location = false;
            }

            if (this.iBeacon && device.udid != null && !device.udid.isEmpty()) {    // && device.udid.length() > 36
                String[] udidMajorMinor = (device.udid).trim().split(":");
                this.deviceUdid = udidMajorMinor[0];
                this.major = Integer.parseInt(udidMajorMinor[1]);
                this.minor = Integer.parseInt(udidMajorMinor[2]);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DeviceByOffers(Offer offer) {
        try {
            this.offer = offer;

            this.iBeacon = false;
            this.wifi = false;
            this.location = true;

            this.campaignId = offer.campaignId;
            this.offerId = offer.offerId;
            this.deviceId = offer.deviceId;

            this.timeGetDevise = System.currentTimeMillis();
            this.boundsInState = false;
            this.boundsOutState = false;
            this.visibleState = false;

            if (offer.markers != null && !offer.markers.isEmpty()) {
                this.latitude = offer.markers.get(0).lat;
                this.longitude = offer.markers.get(0).lng;
                this.offerLatLng = new LatLng(latitude, longitude);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
