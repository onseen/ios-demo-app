package com.onseen.net.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.onseen.net.R;
import com.onseen.net.network.model.MyOffersCard;
import com.onseen.net.tools.CPConstan;
import com.onseen.net.tools.CPGlobalData;
import com.onseen.net.ui.adapter.CardStackOffersAdapter;
import com.onseen.net.ui.cardstatck.cardstack.CardStack;

import static com.onseen.net.tools.CPConstan.END_FRAGMENT_DURATION;
import static com.onseen.net.tools.CPConstan.UPDATE_BUTTON_DELAY;

public class MyOfferDialogFragment extends Fragment {

    private Context mContext;
    private OnOfferDialogFragmentSelectedListener mListener;
    private Button closeButton, shareButton;

    private CardStack.CardEventListener lis;
    private CardStack cardStack;

    private MyOffersCard myOffersCard;
    //this class is using for swipe the AdapterView
    private CardStackOffersAdapter mCardAdapter;
    private UpdateMyButtonFragment myUpdateButtonFragment;

    private LinearLayout buttonGroup;


    public static MyOfferDialogFragment newInstance() {
        return new MyOfferDialogFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        // callback for move buttons view below card view
        myUpdateButtonFragment = new UpdateMyButtonFragment() {
            @Override
            public void onUpdateButton(ViewGroup view) {
                updateButtonDelay(view);
            }
        };

        CPGlobalData.getInstance().myUpdateButtonFragment = myUpdateButtonFragment;

        this.myOffersCard = CPGlobalData.getInstance().myOffersCard;

        try {
            mListener = (OnOfferDialogFragmentSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity should implement " + OnOfferDialogFragmentSelectedListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        CPGlobalData.getInstance().myOffersCard = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.offer_fragment_layout, container, false);

        createCardUI(view);

        callCardStack();

        setOnClickToButton(view);

        return view;
    }

    private void setOnClickToButton(View view) {

        closeButton = (Button) view.findViewById(R.id.close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCloseButtonSelected(myOffersCard);
            }
        });

        shareButton = (Button) view.findViewById(R.id.share_btn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onShareButtonSelected(myOffersCard);
            }
        });

    }

    private void createCardUI(View view) {
        //cardStack initialization
        cardStack = (CardStack) view.findViewById(R.id.frame_my_offer);
        buttonGroup = (LinearLayout) view.findViewById(R.id.buttonGroup);

        //creating adapter
        if (myOffersCard != null)
            mCardAdapter = new CardStackOffersAdapter(getActivity().getApplicationContext(), myOffersCard);
    }

    private void updateButtonDelay(final ViewGroup parent) {
        if (buttonGroup != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (parent.getChildAt(3) != null && parent.getChildAt(3).getHeight() > 0) {
                        updateAnimButton(parent);
                    } else {
                        updateButtonDelay(parent);
                    }
                }
            }, UPDATE_BUTTON_DELAY);
        }
    }

    // move buttons view below card view
    private void updateAnimButton(ViewGroup parent) {
        int screenWidth = CPGlobalData.getInstance().widthDisplayPix;
        int screenHeight = CPGlobalData.getInstance().heightDisplayPix;
        int buttonGroupWidth = buttonGroup.getWidth();
        int cardViewHeight = parent.getChildAt(3).getHeight();
        int cardViewTopPadding = mContext.getResources().getDimensionPixelSize(R.dimen.top_cardstack_padding);
        int X = screenWidth / 2 - buttonGroupWidth / 2;
        int Y = cardViewTopPadding + cardViewHeight;
        int maxY = screenHeight - buttonGroup.getHeight();
        if (Y > maxY) Y = maxY;
        buttonGroup.bringToFront();
        buttonGroup.animate().setDuration(CPConstan.BUTTON_GROUP_DURATION).x(X).y(Y);   // anim выезда
    }

    //cardStack view will set it as visible and load the information into stack.
    private void callCardStack() {

        //Setting Resource of CardStack
        cardStack.setContentResource(R.layout.cardview_offer_fragment);

        //Adding first item for CardStack
        mCardAdapter.add("");
        cardStack.setAdapter(mCardAdapter);

        lis = new CardStack.CardEventListener() {

            @Override
            public boolean swipeEnd(int section, float distance) {
                if (distance > CPConstan.threshold)
                    endFragment();
                return distance > CPConstan.threshold;
            }

            @Override
            public boolean swipeStart(int section, float distance) {
                return true;
            }

            @Override
            public boolean swipeContinue(int section, float distanceX, float distanceY) {
                return true;
            }

            @Override
            public void discarded(int mIndex, int direction) {
                cardStack.reset(false);
            }

            @Override
            public void topCardTapped() {
            }

        };

        cardStack.setListener(lis);

        //Setting Listener and passing distance as a parameter ,
        //based on the distance card will discard
        //if dragging card distance would be more than specified distance(100) then card will discard or else card will reverse on same position.
        //cardStack.setListener(new DefaultStackEventListener(300));

    }

    private void endFragment() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListener.onCloseButtonSelected(myOffersCard);
            }
        }, END_FRAGMENT_DURATION);
    }

    public interface OnOfferDialogFragmentSelectedListener {

        void onCloseButtonSelected(MyOffersCard myOffersCard);

        void onShareButtonSelected(MyOffersCard myOffersCard);

    }

}
