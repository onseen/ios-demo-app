package com.onseen.net.network;


public interface NetworkCallBack {
    void onCompleteString(String jsonString);
    void onError(String errorCode, String errorMessage);
}
