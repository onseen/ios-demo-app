
#import <UIKit/UIKit.h>
#import "YSLCardView.h"
#import "Marker.h"

@interface OfferView : YSLCardView

@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subtitle;
@property (nonatomic, strong) UIView *selectedView;
@property (nonatomic, strong) NSString *expireDate;
@property (nonatomic, strong) Marker *marker;

@end
