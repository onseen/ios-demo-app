//
//  OfferDetailViewController.m
//  Onseen
//
//  Created by CherryPie on 9/21/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "OfferDetailViewController.h"
#import "OfferView.h"
#import "YSLDraggableCardContainer.h"
#import "UIImageView+WebCache.h"
#import "Campaign.h"
#import <MapKit/MapKit.h>
#import <Social/Social.h>
#import "DataFetcher.h"
#import "BarCodeView.h"
#import "AppDelegate.h"
#import "CampaignDataSource.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

#import <PassKit/PassKit.h>


@interface OfferDetailViewController ()<YSLDraggableCardContainerDelegate, YSLDraggableCardContainerDataSource>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonsToTop;
@property (strong, nonatomic) IBOutlet YSLDraggableCardContainer *container;

@property (weak, nonatomic) IBOutlet UIView *viewForOfferList;
@property (weak, nonatomic) IBOutlet UIView *viewForMapOffer;
@property (weak, nonatomic) IBOutlet UIView *viewForMapCampaign;

@property (nonatomic, strong) OfferView *myView;

@end

@implementation OfferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    switch (_popUpType) {
        case CampaignsMap:
            _viewForOfferList.hidden = YES;
            _viewForMapOffer.hidden = YES;
            break;
        case OffersMap:
            _viewForOfferList.hidden = YES;
            _viewForMapCampaign.hidden = YES;
            break;
        case OffersView:
            _viewForMapCampaign.hidden = YES;
            _viewForMapOffer.hidden = YES;
        default:
            break;
    }
    _container.dataSource = self;
    _container.delegate = self;
    _container.canDraggableDirection = YSLDraggableDirectionLeft | YSLDraggableDirectionRight | YSLDraggableDirectionUp | YSLDraggableDirectionDown;
    [_container reloadCardContainer];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLocation" object:nil];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

#pragma mark -- YSLDraggableCardContainer DataSource

- (UIView *)cardContainerViewNextViewWithIndex:(NSInteger)index{
    Campaign *camp;
    Offer *offer;
    if (_popUpType == CampaignsMap)
        camp = _datas[index];
    else
        offer = _datas[index];
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width-100, MAXFLOAT);
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    NSDictionary *attrTitle = @{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Medium" size:17]};
    NSDictionary *attrMessage = @{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Regular" size:17]};
    NSString *tempString = (_popUpType == CampaignsMap) ? camp.account.name : offer.name;
    CGRect labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                                  options:options
                                               attributes:attrTitle
                                                  context:nil];
    CGFloat heightTitle = ceilf(labelBounds.size.height);
    tempString = (_popUpType == CampaignsMap) ? camp.name : offer.title;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                                  options:options
                                               attributes:attrMessage
                                                  context:nil];
    CGFloat heightSubtitle = ceilf(labelBounds.size.height);
    CGFloat heightHeader = MAX(77, heightTitle + heightSubtitle + 28);
    CGFloat heightFooter = 0;
    if (_popUpType == OffersView)
        heightFooter = 140;
    _myView = [[OfferView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width - 20, heightHeader + heightFooter)];
    _myView.clipsToBounds = YES;
    _myView.backgroundColor = [UIColor whiteColor];
    NSURL *imageUrl = [NSURL URLWithString:(_popUpType == CampaignsMap) ? camp.media : offer.media];
    imageUrl = imageUrl ? imageUrl : [NSURL URLWithString:@""];
    [_myView.mainImageView sd_setImageWithURL:imageUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        CGFloat mult = 0;
        if (image)
            mult = _myView.frame.size.width / image.size.width;
        _myView.frame = CGRectMake(10, 20, self.view.frame.size.width - 20, heightHeader + heightFooter + image.size.height * mult);
        _myView.mainImageView.frame = CGRectMake(0, heightHeader, _myView.frame.size.width, _myView.frame.size.height - heightHeader - heightFooter);
        _container.defaultFrame = _myView.frame;
        _constraintButtonsToTop.constant = 30 + _myView.frame.origin.y + _myView.frame.size.height;
        if (_popUpType == OffersView) {
            BarCodeView *barCodeView = [[BarCodeView alloc] initWithFrame:CGRectMake(0, heightHeader + _myView.mainImageView.frame.size.height, _myView.frame.size.width, heightFooter)];
            [_myView addSubview:barCodeView];
            NSString *result = @"";
            NSInteger sum = 0;
            for (NSInteger i = 12; i >= 1; i--)
            {
                NSInteger m = (i % 2) == 1 ? 3 : 1;
                NSInteger value = arc4random() % 10;
                sum += (m*value);
                result = [result stringByAppendingFormat:@"%li", (long)value];
            }
            NSInteger cs = 10 - (sum % 10);
            result = [result stringByAppendingFormat:@"%li", (long)(cs == 10 ? 0 : cs)];
            [barCodeView setBarCode:result];
        }
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];

    _myView.title.frame = CGRectMake(68, 13, [UIScreen mainScreen].bounds.size.width - 100, heightTitle);
    _myView.subtitle.frame = CGRectMake(68, _myView.title.frame.origin.y+_myView.title.frame.size.height+2, [UIScreen mainScreen].bounds.size.width - 100, heightSubtitle);
    NSURL *imUrl = [NSURL URLWithString:(_popUpType == CampaignsMap) ? camp.account.media : offer.logoImage];
    if (imUrl)
        [_myView.logoImageView sd_setImageWithURL:imUrl];
    _myView.title.text = (_popUpType == CampaignsMap) ? camp.account.name : offer.name;
    _myView.subtitle.text = (_popUpType == CampaignsMap) ? camp.name : offer.title;
    _myView.expireDate = (_popUpType == CampaignsMap) ? @"" : offer.expirationDate;
    _myView.layer.zPosition = 1;
    _myView.marker = (_popUpType == CampaignsMap) ? nil : offer.marker;
    if ([_datas count] > index + 1){
        NSURL *cashUrl = [NSURL URLWithString:[_datas[index + 1] media]];
        if (cashUrl)
            [[UIImageView new] sd_setImageWithURL:cashUrl];
    }
    return _myView;
}

- (NSInteger)cardContainerViewNumberOfViewInIndex:(NSInteger)index {
    return [_datas count];
}

#pragma mark -- YSLDraggableCardContainer Delegate
- (void)cardContainerView:(YSLDraggableCardContainer *)cardContainerView didEndDraggingAtIndex:(NSInteger)index draggableView:(UIView *)draggableView draggableDirection:(YSLDraggableDirection)draggableDirection
{
    if(draggableDirection == YSLDraggableDirectionLeft){
        if (_popUpType == CampaignsMap){
                [DataFetcher dislikeCampaigns:[_datas[_container.currentIndex] uuid] success:^{
                } failure:^(NSError *error) {
                }];
        }
        else{
            [DataFetcher dislikeOffer:[_datas[_container.currentIndex] uuid] success:^{
            } failure:^(NSError *error) {
                
            }];
        }
        [self dismissViewWithNextOffer];
    }
    else if (draggableDirection == YSLDraggableDirectionRight){
        if (_popUpType == CampaignsMap){
                [DataFetcher likeCampaigns:[_datas[_container.currentIndex] uuid] success:^{
                } failure:^(NSError *error) {
                }];
            [self dismissViewControllerAnimated:YES completion:^{
                self.likeCampaign();
            }];
        }
        else{
            [DataFetcher likeOffer:[_datas[_container.currentIndex] uuid] success:^{
            } failure:^(NSError *error) {
                
            }];
            [cardContainerView movePositionWithDirection:draggableDirection
                                             isAutomatic:NO];
        }

    }
    
}

- (void)cardContainerViewDidCompleteAll:(YSLDraggableCardContainer *)container{
    [self dismissViewWithNextOffer];
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewWithNextOffer];
}

- (IBAction)shareAction:(id)sender {
    OfferView *currentView = (OfferView*)[_container getCurrentView];
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    NSURL *shareURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@/mypass.pkpass",basicPassURL,applicationId,[_datas[_container.currentIndex] uuid]]];
            
    NSString *string = [NSString stringWithFormat:@"%@\n%@",appName,currentView.subtitle.text];
    UIImage *shareImage = [UIImage imageNamed:@"AppIcon40x40"];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[string, shareImage, shareURL]
                                            applicationActivities:nil];
    [self presentViewController:activityViewController
                       animated:YES
                     completion:^{
                            }];
}

- (IBAction)dislikeAction:(id)sender {
    if (_popUpType == CampaignsMap){
            [DataFetcher dislikeCampaigns:[_datas[_container.currentIndex] uuid] success:^{
                [self dismissViewWithNextOffer];
            } failure:^(NSError *error) {
            }];
    }
    else{
        [DataFetcher dislikeOffer:[_datas[_container.currentIndex] uuid] success:^{
            [self dismissViewWithNextOffer];
        } failure:^(NSError *error) {
            
        }];
    }
}

- (IBAction)likeAction:(id)sender {
    if (_popUpType == CampaignsMap){
            [DataFetcher likeCampaigns:[_datas[_container.currentIndex] uuid] success:^{
                [self dismissViewControllerAnimated:YES completion:^{
                    self.likeCampaign();
                }];
            } failure:^(NSError *error) {
            }];
    }
    else{
        [DataFetcher likeOffer:[_datas[_container.currentIndex] uuid] success:^{
            [_container movePositionWithDirection:YSLDraggableDirectionRight
                                          isAutomatic:YES];
        } failure:^(NSError *error) {
        }];
    }
}

- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (IBAction)skipAction:(id)sender {
    [self dismissViewWithNextOffer];
}

- (void)dismissViewWithNextOffer{
    [self dismissViewControllerAnimated:YES completion:^{
        if ([[CampaignDataSource sharedDataSource].nearOffers count]){
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            OfferDetailViewController *vc = [delegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
            vc.providesPresentationContextTransitionStyle = YES;
            vc.definesPresentationContext = YES;
            vc.datas = [[CampaignDataSource sharedDataSource].nearOffers lastObject];
            [[CampaignDataSource sharedDataSource].nearOffers removeLastObject];
            vc.popUpType = OffersMap;
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [((UINavigationController*)delegate.window.rootViewController).topViewController presentViewController:vc animated:YES completion:nil];
        }
    }];
}
- (IBAction)addToWalletAction:(id)sender {
    [SVProgressHUD show];
    OfferView *currentView = (OfferView*)[_container getCurrentView];
    [DataFetcher getPassDataWithId:[_datas[_container.currentIndex] uuid] success:^(NSData *passData){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
        NSError *error = [NSError new];
        PKPass *pass = [[PKPass alloc] initWithData:passData error:&error];
        
        if(!error){
            PKAddPassesViewController *pkvc = [[PKAddPassesViewController alloc] initWithPass:pass];
            pkvc.delegate = self;
            [self presentViewController:pkvc
                               animated:YES
                             completion:^{
                             }
             ];
        }
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
        NSLog(@"%@",error.description);
    }];
}

@end
