//
//  AccountViewController.m
//  Onseen
//
//  Created by CherryPie on 9/23/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "AccountViewController.h"
#import "DVSwitch.h"
#import "DataFetcher.h"
#import "SVProgressHUD.h"
#import "SDImageCache.h"
#import <AWSS3.h>
#import "AWSUploader.h"

@interface AccountViewController ()<UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *collectionTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageUserAvatar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) DVSwitch *switchForPushNotification;
@property (strong, nonatomic) DVSwitch *switchForSMSNotification;
@property (strong, nonatomic) DVSwitch *switchForEmailNotification;
@property (strong, nonatomic) NSMutableDictionary *accountInfo;
@property (nonatomic) BOOL isLoad;

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    _imageUserAvatar.layer.masksToBounds = YES;
    _imageUserAvatar.layer.cornerRadius = 65;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"USER_AVATAR"]){
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_AVATAR"];
        UIImage* image = [UIImage imageWithData:imageData];
        _imageUserAvatar.image = image;
    }
    _accountInfo = [NSMutableDictionary dictionary];
    NSData *loadData = [[NSUserDefaults standardUserDefaults] objectForKey:@"PROFILE_SETTINGS"];
    _accountInfo = [NSKeyedUnarchiver unarchiveObjectWithData:loadData];
    _isLoad = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlerKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlerKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"DEVICE_ID"]){
        [DataFetcher registerDeviceOnSuccess:^(NSDictionary *phoneInfo) {
        } failure:^(NSError *error) {
        }];
    }

    
    if (_accountInfo[@"detail"]){
        _textFieldName.text = _accountInfo[@"detail"][@"name"];
        _textFieldUsername.text = _accountInfo[@"detail"][@"phone"];
        _textFieldEmail.text = _accountInfo[@"detail"][@"email"];
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    if (_isLoad){
        _isLoad = NO;
        _switchForPushNotification = [DVSwitch switchWithStringsArray:@[@" On", @""]];
        _switchForEmailNotification = [DVSwitch switchWithStringsArray:@[@" On", @""]];
        _switchForSMSNotification = [DVSwitch switchWithStringsArray:@[@" On", @""]];
//        [self addSwitch:_switchForPushNotification withRect:CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 420, 52, 26)];
//        [self addSwitch:_switchForSMSNotification withRect:CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 461, 52, 26)];
//        [self addSwitch:_switchForEmailNotification withRect:CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 502, 52, 26)];
        [self addSwitch:_switchForSMSNotification withRect:CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 420, 52, 26)];
        [self addSwitch:_switchForEmailNotification withRect:CGRectMake([UIScreen mainScreen].bounds.size.width - 70, 461, 52, 26)];
        if (_accountInfo[@"detail"])
            if (_accountInfo[@"detail"][@"notification"]){
                [_switchForPushNotification forceSelectedIndex:[_accountInfo[@"detail"][@"notification"][@"push"] isEqualToString:@"yes"] ? 1 : 0 animated:NO];
                [_switchForSMSNotification forceSelectedIndex:[_accountInfo[@"detail"][@"notification"][@"text"] isEqualToString:@"yes"] ? 1 : 0  animated:NO];
                [_switchForEmailNotification forceSelectedIndex:[_accountInfo[@"detail"][@"notification"][@"email"] isEqualToString:@"yes"] ? 1 : 0  animated:NO];
            }
    }
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

#pragma mark - methods

- (void)addSwitch:(DVSwitch*)dvSwitch withRect:(CGRect)rect{
    dvSwitch.frame = rect;
    dvSwitch.backgroundColor = [UIColor whiteColor];
    dvSwitch.sliderColor = [UIColor colorWithRed:183.0/255.0 green:227.0/255.0 blue:246.0/255.0 alpha:1];
    dvSwitch.sliderOffset = 3;
    dvSwitch.labelTextColorInsideSlider = [UIColor colorWithRed:183.0/255.0 green:227.0/255.0 blue:246.0/255.0 alpha:1];
    dvSwitch.labelTextColorOutsideSlider = [UIColor whiteColor];
    dvSwitch.cornerRadius = 13;
    dvSwitch.font = [UIFont fontWithName:@"AvenirNext-Regular" size:13];
    [self.scrollView.subviews[0] addSubview:dvSwitch];
    __weak typeof(dvSwitch)weakSwitch = dvSwitch;
    [dvSwitch setWillBePressedHandler:^(NSUInteger index) {
        if (index == 0){
            weakSwitch.backgroundColor = [UIColor whiteColor];
            weakSwitch.sliderColor = [UIColor colorWithRed:183.0/255.0 green:227.0/255.0 blue:246.0/255.0 alpha:1];
            weakSwitch.labelTextColorInsideSlider = [UIColor colorWithRed:183.0/255.0 green:227.0/255.0 blue:246.0/255.0 alpha:1];
            
        }
        else{
            weakSwitch.backgroundColor = [UIColor colorWithRed:183.0/255.0 green:227.0/255.0 blue:246.0/255.0 alpha:1];
            weakSwitch.sliderColor = [UIColor whiteColor];
            weakSwitch.labelTextColorInsideSlider = [UIColor whiteColor];
        }
        [weakSwitch changeParameters];
        
    }];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender {
    [SVProgressHUD show];
    if (_accountInfo[@"detail"]){
        _accountInfo[@"detail"][@"name"] = _textFieldName.text;
        _accountInfo[@"detail"][@"phone"] = _textFieldUsername.text;
        _accountInfo[@"detail"][@"email"] = _textFieldEmail.text;
        if (_accountInfo[@"detail"][@"notification"]){
            _accountInfo[@"detail"][@"notification"][@"push"] = _switchForPushNotification.selectedIndex == 1 ? @"yes" : @"no";
            _accountInfo[@"detail"][@"notification"][@"email"] = _switchForEmailNotification.selectedIndex == 1 ? @"yes" : @"no";
            _accountInfo[@"detail"][@"notification"][@"text"] = _switchForSMSNotification.selectedIndex == 1 ? @"yes" : @"no";
        }
    }

    [DataFetcher setAccountInfo:_accountInfo success:^() {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_accountInfo];
        [defaults setObject:data forKey:@"PROFILE_SETTINGS"];
        [defaults synchronize];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
    }];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSUInteger ind = [_collectionTextField indexOfObject:textField];
    if (ind != NSNotFound) {
        if (++ind < _collectionTextField.count) {
            [_collectionTextField[ind] becomeFirstResponder];
            [self checkForMovement:_collectionTextField[ind]];
        }
        else {
            [textField resignFirstResponder];
        }
    }
    return YES;
}

#pragma mark - keyboard methods

static CGFloat DH = 0;
static CGRect keyboardAbsRect;

-(void) checkForMovement:(UIView*)elemView {
    CGRect kRect = [elemView convertRect:keyboardAbsRect fromView:nil];
    
    CGFloat dh = elemView.frame.size.height + 4 - kRect.origin.y;
    if (dh > 0) {
        CGPoint offset = _scrollView.contentOffset;
        offset.y += dh;
        
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentOffset = offset;
        }];
    }
}

-(void)handlerKeyboardWillShow:(NSNotification*) notification {
    NSDictionary *userInfo = [notification userInfo];
    keyboardAbsRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keybRect = [_scrollView convertRect:keyboardAbsRect fromView:nil];
    CGFloat dh  = _scrollView.frame.size.height + _scrollView.contentOffset.y - keybRect.origin.y - DH;
    
    if (dh != 0) {
        CGSize contentSize = _scrollView.contentSize;
        contentSize.height += dh;
        _scrollView.contentSize = contentSize;
        
        DH += dh;
    }
    
    for (UITextField *tf in _collectionTextField) {
        if ([tf isFirstResponder]) {
            [self checkForMovement:tf];
            break;
        }
    }
}

-(void)handlerKeyboardWillHide:(NSNotification*) notification {
    if (DH) {
        CGSize contentSize = _scrollView.contentSize;
        contentSize.height -= DH;
        DH = 0.0;
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentSize = contentSize;
            _scrollView.contentOffset = CGPointZero;
        }];
    }
}
- (IBAction)changeAvatarImageAction:(id)sender {
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take from Camera",@"Choose from Library", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Take photo");
        [self showPhotosPickerWithCamera:YES];
    }
    else if (buttonIndex == 1)
    {
        [self showPhotosPickerWithCamera:NO];
        NSLog(@"Library");
    }
}

-(void)showPhotosPickerWithCamera:(BOOL)isCamera
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = !isCamera ? UIImagePickerControllerSourceTypePhotoLibrary: UIImagePickerControllerSourceTypeCamera;
    controller.delegate = self;
    controller.allowsEditing = true;
    [self presentViewController:controller animated:YES completion:^{
    }];
}

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (image){
        _imageUserAvatar.image = image;
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"USER_AVATAR"];
        AWSUploader *uploader = [[AWSUploader alloc] initWithBucket:@"onseen-users"];
        [uploader uploadImage:image completionHandler:^(NSString *imageUrl) {
            [[NSUserDefaults standardUserDefaults] setObject:imageUrl forKey:@"USER_AVATAR_URL"];
        }];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
