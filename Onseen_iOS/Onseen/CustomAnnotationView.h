//
//  CustomAnnotationView.h
//  Onseen
//
//  Created by CherryPie on 9/22/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAnnotationView : UIView

@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSString *subtitleString;
@property (strong, nonatomic) NSString *textString;
@property (strong, nonatomic) NSString *addressString;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

- (void)setParameters;
@end
