//
//  AWSUploader.m
//  Mercedes
//
//  Created by Mike Timashov on 2/8/17.
//  Copyright © 2017 CherryPie Studio. All rights reserved.
//

#import "AWSUploader.h"
#import <AWSS3/AWSS3.h>

@interface AWSUploader()
@property NSString *bucket;
@end

@implementation AWSUploader

- (instancetype)initWithBucket:(NSString*)bucket
{
    self = [super init];
    if (self) {
        self.bucket = bucket;
    }
    return self;
}

- (void)uploadImage:(UIImage *)image completionHandler:(AWSUploaderCompletionHandler)completionHandler{
    
    UIDevice *device = [UIDevice currentDevice];
    NSString  *currentDeviceId = [NSString stringWithFormat:@"%@_%f.jpg", [[device identifierForVendor] UUIDString], ([[NSDate date] timeIntervalSince1970] * 1000)];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:currentDeviceId];
    [UIImageJPEGRepresentation(image, 0.7) writeToFile:filePath atomically:YES];
    
    NSURL* fileUrl = [NSURL fileURLWithPath:filePath];
    
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = self.bucket;
    uploadRequest.key = currentDeviceId;
    uploadRequest.contentType = @"image/jpeg";
    uploadRequest.body = fileUrl;
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"AWSUploader: Error %@", task.error);
            completionHandler(NULL);
        }
        
        if (task.result) {
            NSLog(@"AWSUploader: The file uploaded successfully");
            NSString *imageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/%@", self.bucket, currentDeviceId];
            completionHandler(imageUrl);
        }
        return nil;
    }];
}

@end
