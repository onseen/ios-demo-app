//
//  OffersViewController.h
//  Onseen
//
//  Created by CherryPie on 9/21/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *offersArray;

@end
