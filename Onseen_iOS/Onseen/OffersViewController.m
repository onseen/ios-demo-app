//
//  OffersViewController.m
//  Onseen
//
//  Created by CherryPie on 9/21/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "OffersViewController.h"
#import "OffersTableViewCell.h"
#import "OfferDetailViewController.h"
#import "Offer.h"
#import "DataFetcher.h"
#import <MapKit/MapKit.h>
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"

@interface OffersViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _offersArray = [NSMutableArray array];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getOffersList];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

- (void)getOffersList{
    [SVProgressHUD show];
    NSMutableArray *tempArray = [NSMutableArray array];
    [DataFetcher getMyOffersListOnSuccess:^(NSArray *compaigns) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
        if (compaigns && ![compaigns isEqual:[NSNull null]]){
        for (NSDictionary *dict in compaigns)
            [tempArray addObject:[[Offer alloc] initWithDictionary:dict]];
        }
        _offersArray = [tempArray mutableCopy];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        });
        
    }];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource, UITableDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _offersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OffersTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"OfferCell" forIndexPath:indexPath];
    Offer *item = _offersArray[indexPath.row];
    NSURL *imageUrl = [NSURL URLWithString:item.logoImage];
    if (imageUrl){
        [((OffersTableViewCell*)cell).logoImage sd_setImageWithURL:imageUrl];
    }
    cell.nameLabel.text = item.name;
    cell.messageLabel.text = item.title;
    cell.descriptionLabel.text = item.text;
    cell.addressLabel.text = item.address;
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Offer *offer = [_offersArray objectAtIndex:indexPath.row];
        [_offersArray removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [DataFetcher dislikeOffer:offer.uuid success:^{
            
        } failure:^(NSError *error) {
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getCellSizeFotItem:_offersArray[indexPath.row] margin:118] + 28;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:((Offer*)_offersArray[indexPath.row]).address completionHandler:^(NSArray* placemarks, NSError* error){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
        MKPlacemark *mapPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                              addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:mapPlacemark];
        mapItem.name = @"Thanks for coming in!";
        [mapItem openInMapsWithLaunchOptions:nil];
    }];
}

- (CGFloat)getCellSizeFotItem:(Offer*)item margin:(CGFloat)margin{
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - margin, MAXFLOAT);
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    NSDictionary *attrTitle = @{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Medium" size:17]};
    NSDictionary *attrMessage = @{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Regular" size:17]};
    NSDictionary *attrOther = @{NSFontAttributeName: [UIFont fontWithName:@"AvenirNext-Regular" size:15]};
    NSString *tempString = item.name;
    CGRect labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                                  options:options
                                               attributes:attrTitle
                                                  context:nil];
    CGFloat height = ceilf(labelBounds.size.height);
    tempString = item.title;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                           options:options
                                        attributes:attrMessage
                                           context:nil];
    height = height + ceilf(labelBounds.size.height);
    tempString = item.text;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                           options:options
                                        attributes:attrOther
                                           context:nil];
    height = height + (tempString.length ? ceilf(labelBounds.size.height) + 2 : 0);
    tempString = item.address;
    labelBounds = [tempString boundingRectWithSize:maximumLabelSize
                                           options:options
                                        attributes:attrOther
                                           context:nil];
    height = height + (tempString.length ? ceilf(labelBounds.size.height) + 2 : 0);
    return height;
}
- (IBAction)favoriteButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    OfferDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
    vc.popUpType = OffersView;
    vc.datas = [@[_offersArray[indexPath.row]] mutableCopy];
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
