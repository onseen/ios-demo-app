

#import <Foundation/Foundation.h>

@interface Marker : NSObject

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) double radius;

@end
