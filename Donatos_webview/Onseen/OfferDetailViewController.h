//
//  OfferDetailViewController.h
//  Onseen
//
//  Created by CherryPie on 9/21/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CampaignsMap = 0,
    OffersMap = 1,
    OffersView = 2,
} PopUpType;

@interface OfferDetailViewController : UIViewController

@property (nonatomic) PopUpType popUpType;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, copy) void (^likeCampaign)();

@end
