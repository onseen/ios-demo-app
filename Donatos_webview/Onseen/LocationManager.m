

#import "LocationManager.h"
#import "Campaign.h"
#import "Offer.h"
#import "CampaignDataSource.h"
#import "AppDelegate.h"
#import "OfferDetailViewController.h"

@interface LocationManager()

@end

@implementation LocationManager

+ (LocationManager*)shared
{
    static LocationManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LocationManager alloc] init];
        _sharedInstance.locationManager = [[CLLocationManager alloc]init];
        [ _sharedInstance.locationManager requestAlwaysAuthorization];
        _sharedInstance.locationManager.delegate = _sharedInstance;
    });
    return _sharedInstance;
}
 
- (void)initCurrentLocation {
    self.locationManager.distanceFilter = 1;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [self.locationManager startUpdatingLocation];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
        [self.locationManager requestAlwaysAuthorization];
//    if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)])
//        self.locationManager.allowsBackgroundLocationUpdates = YES;
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSTimeInterval closestLocationAge = [[NSDate date] timeIntervalSince1970];
    int i = 0;
    for(CLLocation *loc in locations)
    {
        CLLocationAccuracy accuracy = loc.horizontalAccuracy;
        NSTimeInterval locationAge = -[loc.timestamp timeIntervalSinceNow];
        ++i;
        if (locationAge > 15.0)
            continue;
        if (locationAge > closestLocationAge)
            continue;
        if (accuracy > 0 && accuracy < 1000.0 && (loc.coordinate.latitude != 0.0 || loc.coordinate.longitude != 0.0)) {
            closestLocationAge = locationAge;
            CLLocationDistance dis = [self.lastValidLocation distanceFromLocation:loc];
            if (!_lastValidLocation || [@(dis/1000) doubleValue] > 10){
                self.lastValidLocation = loc;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLocation" object:nil];
            }
        }
    }
}

@end
