//
//  AppDelegate.m
//  Onseen
//
//  Created by admin on 21.09.16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "AppDelegate.h"
#import "OfferDetailViewController.h"
#import "CampaignDataSource.h"
#import "Campaign.h"
#import "Offer.h"
#import "DataFetcher.h"
#import <sys/utsname.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "OffersViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Regular" size:24]}];
    [self registerForPushNotifications];
    
    [ESTConfig setupAppID:@"cherrypie-studio-s-proximi-n2y" andAppToken:@"57d252d7796506a35f13f152affcdd59"];
    _beaconManager = [ESTBeaconManager new];
    _beaconManager.delegate = self;
    _beaconManager.returnAllRangedBeaconsAtOnce = YES;
    [_beaconManager requestAlwaysAuthorization];
    [[UIApplication sharedApplication]
     registerUserNotificationSettings:[UIUserNotificationSettings
                                       settingsForTypes:UIUserNotificationTypeAlert
                                       categories:nil]];
    return YES;
}

- (void)startRegionsMonitoring:(NSArray*)regions{
    [self.beaconManager stopRangingBeaconsInAllRegions];
    [self.beaconManager stopMonitoringForAllRegions];
    for (CLBeaconRegion *reg in regions){
        [self.beaconManager startRangingBeaconsInRegion:reg];
        [self.beaconManager startMonitoringForRegion:reg];
    }
}

- (void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region{
    if ([beacons count] && _isStart){
        [[CampaignDataSource sharedDataSource] didFindBeacon:[beacons firstObject]];
        _isStart = NO;
    }
}

- (void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region{
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
        [[CampaignDataSource sharedDataSource] didEnterBeacon:region];
    }
    else{
        UILocalNotification *notification = [UILocalNotification new];
        notification.alertBody = [NSString stringWithFormat:@"New offer is available."];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

-(void)beaconManager:(id)manager didExitRegion:(CLBeaconRegion *)region{
//    UILocalNotification *notification = [UILocalNotification new];
//    notification.alertBody = [NSString stringWithFormat:@"Bla is exit."];
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//    [[CampaignDataSource sharedDataSource] didExitBeacon:region];
}

- (void)registerForPushNotifications {
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeAlert |
                                                UIUserNotificationTypeBadge |
                                                UIUserNotificationTypeSound
                                                                            categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    NSString *strToken = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    struct utsname systemInfo;
    uname(&systemInfo);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"PROFILE_SETTINGS"]){
        NSMutableDictionary *profileSettings = [@{@"type": [NSString stringWithCString:systemInfo.machine
                                                                  encoding:NSUTF8StringEncoding],
                                                  @"udid": @"",
                                                  @"imei": @"",
                                                  @"detail": [@{
                                                                @"maxHeight": [NSString stringWithFormat:@"%.0f",[UIScreen mainScreen].bounds.size.height],
                                                                @"maxWidth": [NSString stringWithFormat:@"%.0f",[UIScreen mainScreen].bounds.size.width],
                                                                @"name": @"",
                                                                @"email": @"",
                                                                @"phone": @"",
                                                                @"tokenId": deviceTokenStr,
                                                                @"notification": [@{
                                                                                    @"push": @"no",
                                                                                    @"email": @"no",
                                                                                    @"text": @"no"
                                                                                    } mutableCopy]
                                                                } mutableCopy]
                                                  } mutableCopy];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:profileSettings];
        [defaults setObject:data forKey:@"PROFILE_SETTINGS"];
        [defaults synchronize];
        [DataFetcher registerDeviceOnSuccess:^(NSMutableDictionary *phoneInfo) {
        } failure:^(NSError *error) {
        }];
    }
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo{
    if (application.applicationState != UIApplicationStateActive){
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    application.applicationIconBadgeNumber = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _isStart = YES;
    if (![defaults objectForKey:@"DEVICE_ID"]){
        [DataFetcher registerDeviceOnSuccess:^(NSDictionary *phoneInfo) {
        } failure:^(NSError *error) {
            
        }];
    }
    NSString *wifiMacAddress = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"BSSID"]) {
            wifiMacAddress = [self getValidMacAddress:info[@"BSSID"]] ;
        }
    }
    NSMutableArray *offersArray = [NSMutableArray array];
    if ([wifiMacAddress length]){
        for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
            for (Offer *offer in camp.offers){
                if ([offer.macAddress containsObject:wifiMacAddress])
                    [offersArray addObject:offer];
            }
    }
    if ([offersArray count]){
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
            [((UINavigationController*)self.window.rootViewController).topViewController dismissViewControllerAnimated:YES completion:nil];
            
            OfferDetailViewController *vc = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
            vc.providesPresentationContextTransitionStyle = YES;
            vc.definesPresentationContext = YES;
            vc.datas = [offersArray mutableCopy];
            vc.popUpType = OffersMap;
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [[CampaignDataSource sharedDataSource].nearOffers removeAllObjects];
            [((UINavigationController*)self.window.rootViewController).topViewController presentViewController:vc animated:YES completion:^{
            }];
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    application.applicationIconBadgeNumber = 0;
}

- (NSString*)getValidMacAddress:(NSString*)macAddress{
    NSMutableString *validMacAddress = [NSMutableString string];
    NSArray *tempArray = [macAddress componentsSeparatedByString:@":"];
    for (NSString *str in tempArray){
        NSString *tempString = str.length > 1 ? [str uppercaseString] : [[NSString stringWithFormat:@"0%@",str] uppercaseString];
        [validMacAddress appendString:[NSString stringWithFormat:@":%@",tempString]];
    }
    return [validMacAddress substringFromIndex:1];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if ([url.absoluteString isEqualToString:@"donatos://offers"]){
        [((UINavigationController*)self.window.rootViewController).topViewController dismissViewControllerAnimated:YES completion:nil];
        OffersViewController *controller = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"OffersViewController"];
        [((UINavigationController*)self.window.rootViewController) pushViewController:controller animated:YES];
    }
    return YES;
}

@end
