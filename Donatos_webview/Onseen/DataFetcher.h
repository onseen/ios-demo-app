//
//  DataFetcher.h
//  Onseen
//
//  Created by CherryPie on 9/27/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static const NSString *basicURL = @"https://api.onseen.com/";
static const NSString *basicPassURL = @"https://pass.onseen.com/";
static const NSString *applicationId = @"5231840d-8a86-4d42-9c7f-e40fb74dadc8";

@interface DataFetcher : NSObject

+ (void)getNewCampaignAndOffersLat:(CGFloat)lat
                                lon:(CGFloat)lon
                            success:(void (^)(NSArray *compaigns))success
                            failure:(void (^)(NSError *error))failure;

+ (void)getMyOffersListOnSuccess:(void (^)(NSArray *compaigns))success
                         failure:(void (^)(NSError *error))failure;

+ (void)likeOffer:(NSString*)offerId
          success:(void (^)())success
          failure:(void (^)(NSError *error))failure;

+ (void)dislikeOffer:(NSString*)offerId
             success:(void (^)())success
             failure:(void (^)(NSError *error))failure;

+ (void)likeCampaigns:(NSString*)campaignId
              success:(void (^)())success
              failure:(void (^)(NSError *error))failure;

+ (void)dislikeCampaigns:(NSString*)campaignId
                 success:(void (^)())success
                 failure:(void (^)(NSError *error))failure;

+ (void)registerDeviceOnSuccess:(void (^)(NSMutableDictionary *phoneInfo))success
                        failure:(void (^)(NSError *error))failure;

+ (void)setAccountInfo:(NSDictionary*)info
               success:(void (^)())success
               failure:(void (^)(NSError *error))failure;

+ (void)inBoundsWithOffer:(NSString*)offerId
                  success:(void (^)())success
                  failure:(void (^)(NSError *error))failure;

+ (void)getPassDataWithId:(NSString*)offerId
                  success:(void (^)(NSData *passData))success
                  failure:(void (^)(NSError *error))failure;

+ (void)outFromBoundsWithOffer:(NSString*)offerId
                       success:(void (^)())success
                       failure:(void (^)(NSError *error))failure;

@end
