//
//  ReachabilityManager.h
//  Onseen
//
//  Created by CherryPie on 9/29/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ReachabilityManager : NSObject

+ (instancetype)shared;
- (void)initReachabilityStatus;

@end
