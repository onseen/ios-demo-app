

#import "Offer.h"

@implementation Offer

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.uuid = dictionary[@"offerId"];
        self.title = dictionary[@"name"] ? dictionary[@"name"] : @"";
        self.text = dictionary[@"description"] ? dictionary[@"description"] : @":";
        self.iBeacons = [NSMutableSet set];
        self.macAddress = [NSMutableSet set];
        self.expirationDate = dictionary[@"expirationDate"];
        
        for (NSDictionary *b in dictionary[@"devices"]){
            if ([[b[@"type"] uppercaseString] isEqualToString:@"IBEACON"])
                [self.iBeacons addObject:b[@"udid"]];
            else
                [self.macAddress addObject:[b[@"identifier"] uppercaseString]];
        }
        
        
        if (dictionary[@"media"])
            if ([dictionary[@"media"] respondsToSelector:@selector(count)])
                if ([dictionary[@"media"] count])
                    self.media = [[dictionary[@"media"] firstObject] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.address = dictionary[@"address"];
        
        self.optedIn = [dictionary[@"optIn"] boolValue];
        self.like = (dictionary[@"like"] == [NSNull null]) ? @"" : dictionary[@"like"];
        
        self.marker = [[Marker alloc] init];
        if ([dictionary[@"markers"] count]){
            self.marker.latitude = [dictionary[@"markers"][0][@"lat"] doubleValue];
            self.marker.longitude = [dictionary[@"markers"][0][@"lng"] doubleValue];
            self.marker.radius = [dictionary[@"markers"][0][@"radius"] doubleValue];
        }
        if (dictionary[@"account"]){
            self.logoImage = dictionary[@"account"][@"media"];
            self.name = dictionary[@"account"][@"name"];
        }
//        self.logoImage = dictionary[@"account_media"];
//        self.name = dictionary[@"campaignName"];
    }
    return self;
}

@end
