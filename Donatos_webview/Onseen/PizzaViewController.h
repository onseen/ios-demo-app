//
//  ViewController.h
//  Donatos
//
//  Created by Mike Timashov on 12/22/16.
//  Copyright © 2016 CherryPie Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PizzaViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

