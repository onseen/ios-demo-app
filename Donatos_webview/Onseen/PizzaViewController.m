//
//  ViewController.m
//  Donatos
//
//  Created by Mike Timashov on 12/22/16.
//  Copyright © 2016 CherryPie Studio. All rights reserved.
//

#import "PizzaViewController.h"
#import "OffersViewController.h"
#import "LocationManager.h"
#import "DataFetcher.h"
#import "ReachabilityManager.h"
#import "CampaignDataSource.h"

@interface PizzaViewController ()
@property UIButton *linkButton;
@end

@implementation PizzaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createLinkButton];
    
    self.webview.scrollView.bounces = NO;
    NSURL *url = [NSURL URLWithString:@"https://donatos.com"];
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
    
    self.navigationController.navigationBarHidden = true;
    
    [[LocationManager shared] initCurrentLocation];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"DEVICE_ID"]){
        [DataFetcher registerDeviceOnSuccess:^(NSDictionary *phoneInfo) {
            [self getMarkers];
        } failure:^(NSError *error) {
        }];
    }
    else{
        [self getMarkers];
    }
}

- (void)getMarkers{
    [DataFetcher getNewCampaignAndOffersLat:[LocationManager shared].lastValidLocation.coordinate.latitude lon:[LocationManager shared].lastValidLocation.coordinate.longitude success:^(NSArray *compaigns) {
        //[self.mainMapView removeAnnotations:[self.mainMapView annotations]];
        [[CampaignDataSource sharedDataSource] updateDataSource:compaigns];
        [[ReachabilityManager shared] initReachabilityStatus];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.description);
    }];
}

- (void) viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = true;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) createLinkButton {
    CGFloat BUTTON_WIDTH = self.view.frame.size.width * 0.09;
    CGFloat BUTTON_X = self.view.frame.size.width * 0.37;
    CGFloat BUTTON_Y = self.view.frame.size.width * 0.04 + [UIApplication sharedApplication].statusBarFrame.size.height;
    
    
    self.linkButton = [[UIButton alloc] initWithFrame:CGRectMake(BUTTON_X, BUTTON_Y, BUTTON_WIDTH, BUTTON_WIDTH)];
    [self.linkButton setBackgroundImage:[UIImage imageNamed:@"linkbutton"] forState:UIControlStateNormal];
    [self.linkButton addTarget:self action:@selector(clickedLinkButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.linkButton];
}

- (IBAction)clickedLinkButton:(id)sender{
    NSLog(@"clickedLinkButton");
    
    self.navigationController.navigationBarHidden = false;
    
    OffersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OffersViewController"];
    [self.navigationController pushViewController:controller animated:YES];

}

- (BOOL)prefersStatusBarHidden {
    return YES;
}



@end
