
#import "OfferView.h"

@implementation OfferView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _logoImageView = [[UIImageView alloc] init];
    _logoImageView.frame = CGRectMake(10, 13, 50, 50);
    _logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_logoImageView];
    _title = [[UILabel alloc]init];
    _title.numberOfLines = 0;
    _title.backgroundColor = [UIColor clearColor];
    _title.textColor = [UIColor blackColor];
    _title.font = [UIFont fontWithName:@"AvenirNext-Medium" size:17];
    _title.minimumScaleFactor = 0.5;
    [self addSubview:_title];
    _subtitle = [[UILabel alloc]init];
    _subtitle.numberOfLines = 0;
    _subtitle.backgroundColor = [UIColor clearColor];
    _subtitle.textColor = [UIColor blackColor];
    _subtitle.font = [UIFont fontWithName:@"AvenirNext-Regular" size:17];
    _subtitle.minimumScaleFactor = 0.5;
    [self addSubview:_subtitle];
    _mainImageView = [[UIImageView alloc]init];
    _mainImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:_mainImageView];
}

@end
