//
//  POLocationManager.h
//  MapBookmarksApp
//
//  Created by Pavel Ostanin on 10/11/2015.
//  Copyright © 2015 PavelOstanin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocation *lastValidLocation;
@property (strong, nonatomic) CLLocationManager *locationManager;

+ (instancetype)shared;
- (void)initCurrentLocation;

@end
