//
//  CampaignDataSource.m
//  Onseen
//
//  Created by CherryPie on 9/29/16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import "CampaignDataSource.h"
#import "Campaign.h"
#import "LocationManager.h"
#import "AppDelegate.h"
#import "OfferDetailViewController.h"
#import "DataFetcher.h"

@implementation CampaignDataSource

+ (CampaignDataSource*)sharedDataSource {
    static CampaignDataSource *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[CampaignDataSource alloc] init];
        _sharedInstance.campaigns = [NSMutableArray array];
        _sharedInstance.nearOffers = [NSMutableArray array];
    });
    return _sharedInstance;
}

- (void)updateDataSource:(NSArray*)campaigns{
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *d in campaigns)
        [tempArray addObject:[[Campaign alloc] initWithDictionary:d]];
    self.campaigns = [tempArray mutableCopy];
    [self startMonitoringIBeacone];
}

- (void)startMonitoringIBeacone{
    int i = 0;
    NSMutableSet *tempSet = [NSMutableSet set];
    NSMutableArray *beaconsIds = [NSMutableArray array];
    for (Campaign *camp in _campaigns)
        for (Offer *offer in camp.offers){
            for (NSString *str in offer.iBeacons){
                if (![tempSet containsObject:str]){
                    NSArray *temp = [str componentsSeparatedByString:@":"];
                    if (temp.count >2){
                        [beaconsIds addObject:[[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:temp[0]]
                                                                                      major:[temp[1] integerValue]
                                                                                      minor:[temp[2] integerValue]
                                                                                 identifier:[NSString stringWithFormat:@"region_%d",i]]];
                        i++;
                    }
                    [tempSet addObject:str];
                }
            }
        }
    AppDelegate *deleg = [UIApplication sharedApplication].delegate;
    [deleg startRegionsMonitoring:beaconsIds];
    
    
}

- (void)didFindBeacon:(CLBeacon*)beacon{
    int major = [beacon.major integerValue];
    int minor = [beacon.minor integerValue];
    NSString *beaconIden = [NSString stringWithFormat:@"%@:%d:%d",beacon.proximityUUID.UUIDString,major,minor];
    [self findBeaconInList:beaconIden];
}

- (void)didEnterBeacon:(CLBeaconRegion*)beacon{
    int major = [beacon.major integerValue];
    int minor = [beacon.minor integerValue];
    NSString *beaconIden = [NSString stringWithFormat:@"%@:%d:%d",beacon.proximityUUID.UUIDString,major,minor];
    [self findBeaconInList:beaconIden];
//    [self postInBoundsRequest:beaconIden];
}

- (void)didExitBeacon:(CLBeaconRegion*)beacon{
//    int major = [beacon.major integerValue];
//    int minor = [beacon.minor integerValue];
//    NSString *beaconIden = [NSString stringWithFormat:@"%@:%d:%d",beacon.proximityUUID.UUIDString,major,minor];
//    [self findBeaconInList:beaconIden];
//    [self postOutFromBoundsRequest:beaconIden];
}

- (NSArray*)offerIdsByBeacon:(NSString*)uuid{
    NSMutableArray *offersArray = [NSMutableArray array];
    for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
        for (Offer *offer in camp.offers){
            if ([offer.iBeacons containsObject:uuid]){
                [offersArray addObject:offer.uuid];
            }
        }
    return offersArray;

}

- (void)findBeaconInList:(NSString*)uuid{
    NSMutableArray *offersArray = [NSMutableArray array];
    for (Campaign *camp in [CampaignDataSource sharedDataSource].campaigns)
    for (Offer *offer in camp.offers){
        if ([offer.iBeacons containsObject:uuid]){
            [offersArray addObject:offer];
        }
    }
    if ([offersArray count]){
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
            [[CampaignDataSource sharedDataSource].nearOffers addObject:[offersArray mutableCopy]];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            OfferDetailViewController *vc = [delegate.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailViewController"];
            vc.providesPresentationContextTransitionStyle = YES;
            vc.definesPresentationContext = YES;
            vc.datas = [[CampaignDataSource sharedDataSource].nearOffers lastObject];
            vc.popUpType = OffersMap;
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [((UINavigationController*)delegate.window.rootViewController).topViewController presentViewController:vc animated:YES completion:^{
                [[CampaignDataSource sharedDataSource].nearOffers removeLastObject];
            }];
        }
    }
}

- (void)postInBoundsRequest:(NSString*)uuid{
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = [NSString stringWithFormat:@"%@ is available.",uuid];
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    NSArray *offers = [self offerIdsByBeacon:uuid];
    for (NSString *str in offers){
        [DataFetcher inBoundsWithOffer:str success:^{
        } failure:^(NSError *error) {
        }];
    }
}

- (void)postOutFromBoundsRequest:(NSString*)uuid{
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = [NSString stringWithFormat:@"%@ is unavailable.",uuid];
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    NSArray *offers = [self offerIdsByBeacon:uuid];
    for (NSString *str in offers){
        [DataFetcher outFromBoundsWithOffer:str success:^{
        } failure:^(NSError *error) {
        }];
    }
}

@end
