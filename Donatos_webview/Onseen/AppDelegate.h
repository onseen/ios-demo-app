//
//  AppDelegate.h
//  Onseen
//
//  Created by admin on 21.09.16.
//  Copyright © 2016 CherryPieStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,ESTBeaconManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) ESTBeaconManager *beaconManager;
@property (nonatomic) BOOL isStart;
- (void)startRegionsMonitoring:(NSArray*)regions;
@end

