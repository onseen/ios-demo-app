

#import <Foundation/Foundation.h>
#import "Account.h"
#import "Offer.h"

@interface Campaign : NSObject

@property (strong, nonatomic) NSString *uuid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *descriptions;
@property (strong, nonatomic) NSString *like;
@property (strong, nonatomic) Account *account;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *media;
@property (strong, nonatomic) NSDate *effectiveDate;
@property (strong, nonatomic) NSDate *expirationDate;
@property (strong, nonatomic) NSArray *offers;
@property (strong, nonatomic) NSString *createdBy;

@property (strong, nonatomic) NSArray *markers;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
