

#import "Campaign.h"

@implementation Campaign

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (!dictionary) return nil;
        
        self.uuid = dictionary[@"campaignId"];
        self.name = dictionary[@"name"] ? dictionary[@"name"] : @"";
        self.like = (dictionary[@"like"] == [NSNull null]) ? @"" : [NSString stringWithFormat:@"%@",dictionary[@"like"]];
        self.descriptions = dictionary[@"description"];
#warning change
        self.account = [[Account alloc] initWithDictionary:dictionary[@"account"]];
//        self.account = [[Account alloc] initWithDictionary:@{@"id":dictionary[@"accountId"] ? dictionary[@"accountId"] : @"",@"name":dictionary[@"accountName"]?dictionary[@"accountName"]:@"",@"media":dictionary[@"account_media"]?dictionary[@"account_media"]:@""}];
        self.url = [NSURL URLWithString:dictionary[@"url"]];
        
        if (dictionary[@"media"])
            if ([dictionary[@"media"] respondsToSelector:@selector(count)])
                if ([dictionary[@"media"] count])
                    self.media = [[dictionary[@"media"] firstObject] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        self.effectiveDate = [[NSDate alloc] initWithTimeIntervalSince1970:[dictionary[@"effectiveDate"] integerValue]/1000];
        self.expirationDate = [[NSDate alloc] initWithTimeIntervalSince1970:[dictionary[@"expirationDate"] integerValue]/1000];
        
        self.createdBy = dictionary[@"createdBy"];
        
        NSMutableArray *offers = [[NSMutableArray alloc] init];
        for (NSDictionary *offer in dictionary[@"offers"]) {
            Offer *newOffer = [[Offer alloc] initWithDictionary:offer];
            newOffer.logoImage = self.account.media;
            newOffer.name = self.account.name;
            [offers addObject:newOffer];
        }
        self.offers = (NSArray *)offers;
        
        NSMutableArray *markers = [[NSMutableArray alloc] init];
        for (NSDictionary *d in dictionary[@"markers"]) {
            Marker *marker = [[Marker alloc] init];
            marker.latitude = [d[@"lat"] doubleValue];
            marker.longitude = [d[@"lng"] doubleValue];
            marker.radius = [d[@"radius"] doubleValue];
            [markers addObject:marker];
        }
        self.markers = (NSArray *)markers;
    }
    return self;
}

@end
