package com.mattford.net.tools.btutil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Nearable;
import com.estimote.sdk.Region;
import com.estimote.sdk.SecureRegion;
import com.mattford.net.MyApplication;
import com.mattford.net.R;
import com.mattford.net.network.NetworkCallBack;
import com.mattford.net.network.NetworkController;
import com.mattford.net.network.model.DeviceByOffers;
import com.mattford.net.tools.CPConstan;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.tools.sharprefutil.SharedPrefManager;
import com.mattford.net.ui.MapsActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.mattford.net.MyApplication.logD;
import static com.mattford.net.tools.CPConstan.VALID_TIME_OFFERS;


public class BtEstimoteService extends Service {

    private static final long DELAYED_BOUNDS = CPConstan.DELAY_BT_OUT;

    private Context mContext = this;
    private BeaconManager beaconManager;
    private SharedPrefManager sharedPrefManager;
    private List<DeviceByOffers> deviceByOffersList;
    private List<DeviceByOffers> deviceByOfferFounds;
    private List<DeviceByOffers> offersFromDevice;

    private long count;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(this);

        beaconManager = new BeaconManager(getApplicationContext());
        CPGlobalData.getInstance().beaconManager = beaconManager;
        offersFromDevice = CPGlobalData.getInstance().offersFromDevice;
        if (offersFromDevice == null) {
            offersFromDevice = new ArrayList<>();
            CPGlobalData.getInstance().offersFromDevice = offersFromDevice;
        }
        deviceByOfferFounds = new ArrayList<>();

        startMonitoringRegion();
//        startMonitoringRegions();

        return START_STICKY;
    }


    private void startMonitoringRegion() {
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                CPGlobalData.getInstance().beaconManagerIsReady = true;
            }
        });

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {

                logD("onEnteredRegion minor: " + region.getMinor());

                if (!list.isEmpty()) {
                    deviceByOffersList = CPGlobalData.getInstance().deviceByOffersList;
                    for (int i = 0; i < list.size(); i++) {
                        Beacon beacon = list.get(i);
                        if (foundIBeaconInArr(beacon.getProximityUUID().toString(), beacon.getMajor(), beacon.getMinor())) {
                            if (!deviceByOfferFounds.isEmpty()) {
                                // sendBoundsRequest("in", deviceByOfferFounds.offerId);                                // TODO IN
                                checkFoundsOffersAndSendBroadcast();
                            }
                        }
                    }
                }
            }

            @Override
            public void onExitedRegion(Region region) {

                logD("onExitedRegion minor: " + region.getMinor());

            }
        });
    }

    // found all offers if equals UUID from beacon
    private boolean foundIBeaconInArr(String uuid, int major, int minor) {
        boolean found = false;
        if (deviceByOffersList != null && deviceByOffersList.size() > 0) {
            for (int i = 0; i < deviceByOffersList.size(); i++) {
                DeviceByOffers deviceByOffer = deviceByOffersList.get(i);
                if (deviceByOffer.iBeacon) {

                    if (deviceByOffer.deviceUdid != null && !deviceByOffer.deviceUdid.isEmpty()
                            && deviceByOffer.deviceUdid.equalsIgnoreCase(uuid)
                            && deviceByOffer.major == major && deviceByOffer.minor == minor) {
                        deviceByOffer.boundsInState = true;
                        deviceByOffer.boundsOutState = false;
                        deviceByOffer.boundsInMillisecond = System.currentTimeMillis();
                        deviceByOfferFounds.add(deviceByOffer);
                        found = true;
                        logD("found Offer iBeacon In deviceByOffersList");
                    }

                    /*} else if (deviceByOffer.boundsInState && !deviceByOffer.boundsOutState) {
                        if (System.currentTimeMillis() > deviceByOffer.boundsInMillisecond + DELAYED_BOUNDS) {
                            deviceByOffer.boundsInState = false;
                            deviceByOffer.boundsOutState = true;
                            deviceByOffer.boundsOutMillisecond = System.currentTimeMillis();
                            //   sendBoundsRequest("out", deviceByOffer.offerId);                                // TODO OUT
                        }
                    }*/
                }
            }
        }
        if (!found) deviceByOfferFounds = new ArrayList<>();
        return found;
    }

    private void checkFoundsOffersAndSendBroadcast() {
        boolean offerFoundState = false;
        cleanOfferFromDevice();
        for (int i = 0; i < deviceByOfferFounds.size(); i++) {
            if (containsOfferId(deviceByOfferFounds.get(i).offerId)) {
                logD("OffersFromDevice contains OfferId");
            } else {
                logD("OffersFromDevice add OfferId " + deviceByOfferFounds.get(i).offerId);
                offerFoundState = true;
                offersFromDevice.add(deviceByOfferFounds.get(i));                                                     // add if not equals OfferId
            }
        }
        if (offerFoundState)
            sendOfferBroadcast();                                                                        // send if found new valid offer
    }

    private void cleanOfferFromDevice() {
        long validTime = System.currentTimeMillis() - VALID_TIME_OFFERS;
        Iterator<DeviceByOffers> iterator = CPGlobalData.getInstance().offersFromDevice.iterator();
        while (iterator.hasNext()) {
            DeviceByOffers offerByDevice = iterator.next();
            if (validTime > offerByDevice.boundsInMillisecond) {     //TODO check offer validTime
                iterator.remove();
            }
        }
    }

    private boolean containsOfferId(String offerId) {
        for (int i = 0; i < offersFromDevice.size(); i++) {
            if (offersFromDevice.get(i).offerId.equalsIgnoreCase(offerId))
                return true;
        }
        return false;
    }

    private void sendOfferBroadcast() {
        if (!CPGlobalData.getInstance().activityVisible) {
            MyApplication.showNotificationBeacon(mContext.getString(R.string.app_name), mContext.getString(R.string.offer_notification));
        } else {
            Intent intent = new Intent(MapsActivity.BROADCAST_FOUND_OFFER);
            intent.putExtra("indexFromService", "bt");
            mContext.sendBroadcast(intent);
        }
    }

    @Override
    public void onDestroy() {
        CPGlobalData.getInstance().beaconManager = null;
        CPGlobalData.getInstance().beaconManagerIsReady = false;
        super.onDestroy();
    }

    private void sendBoundsRequest(final String action, final String offerId) {                             // TODO IN/OUT (action = in/out)
        NetworkController.getInstance().sendBoundsRequest(this, action, offerId, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
            }
        });
    }


    // TODO: Ranging Listener replace "<major>:<minor>" strings to match your own beacons.
    private static final Map<String, List<String>> PLACES_BY_BEACONS;

    static {
        Map<String, List<String>> placesByBeacons = new HashMap<>();
        placesByBeacons.put("3561:11270", new ArrayList<String>() {{
            add("First iBeacon");
        }});
        placesByBeacons.put("9872:59925", new ArrayList<String>() {{
            add("Secons iBeacon");
        }});
        PLACES_BY_BEACONS = Collections.unmodifiableMap(placesByBeacons);
    }

    private void startMonitoringRegions() {
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                CPGlobalData.getInstance().beaconManagerIsReady = true;
            }
        });

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                count++;
                SecureRegion secureRegion;
                Log.d("Airport", "index: " + count + "  " + region.toString() + "  List<Beacon> " + list.size());
                if (!list.isEmpty()) {
                    Beacon nearestBeacon = list.get(0);
                    List<String> places = placesNearBeacon(nearestBeacon);
                    // TODO: update the UI here
                    Log.d("Airport", "Nearest places: " + places);

                }
            }
        });


        beaconManager.setNearableListener(new BeaconManager.NearableListener() {
            @Override
            public void onNearablesDiscovered(List<Nearable> list) {

            }
        });

        beaconManager.startNearableDiscovery();
    }

    private List<String> placesNearBeacon(Beacon beacon) {
        String beaconKey = String.format("%d:%d", beacon.getMajor(), beacon.getMinor());
        if (PLACES_BY_BEACONS.containsKey(beaconKey)) {
            return PLACES_BY_BEACONS.get(beaconKey);
        }
        return Collections.emptyList();
    }

}
