package com.mattford.net.tools.wifiutil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.mattford.net.network.model.DeviceByOffers;
import com.mattford.net.tools.CPGlobalData;

import java.util.List;

/**
 * Stars WifiService if WiFi Enable end CHANGE_STATE
 */

public class WifiReceiver extends BroadcastReceiver {

    private WifiManager wifiManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        List<DeviceByOffers> deviceByOffersList = CPGlobalData.getInstance().deviceByOffersList;
        if (deviceByOffersList != null && !deviceByOffersList.isEmpty()) {

/*          //If found new WiFi
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION) ) {
            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            List<ScanResult> results = wifiManager.getScanResults();
        }
*/
            // If WiFi RECONNECTED and STATE == CONNECTED
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null)
                    if (networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                        context.startService(new Intent(context, WifiService.class));
                    }
            }

            // If WiFi CHANGE_STATE and Enable
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action) || WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                    context.startService(new Intent(context, WifiService.class));
                }
            }

            if (action.equals("ru.racoondeveloper.beaconnotifyer.WAKE_WIFI_RECEIVER")) {
                // App START — START Service
                wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                    context.startService(new Intent(context, WifiService.class));
                }
            }
        }
    }
}
