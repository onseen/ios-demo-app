package com.mattford.net.tools;

import android.os.AsyncTask;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.mattford.net.network.NetworkController;

import java.io.File;

/**
 * Created by Anton on 02.03.2017
 */

public class AmazonS3ImageUpload extends AsyncTask<Void, Void, Void> {
    private static final String MY_ACCESS_KEY_ID = "AKIAJRMLSX5WIJ4MXFDQ";
    private static final String MY_SECRET_KEY = "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";
    private static final String MY_PICTURE_BUCKET = "onseen-users";
    public static final String BASE_PHOTO_URL = "https://s3.amazonaws.com/onseen-users/";

    private final String deviceId = NetworkController.getInstance().getDeviceId();

    private String filePath;
    private long currentTimeMillis = 0;

    public AmazonS3ImageUpload(String filePath, long currentTimeMillis) {
        this.filePath = filePath;
        this.currentTimeMillis = currentTimeMillis;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(MY_ACCESS_KEY_ID, MY_SECRET_KEY));
        PutObjectRequest object = new PutObjectRequest(MY_PICTURE_BUCKET, generatePhotoNameString(), new File(filePath));
        object.withCannedAcl(CannedAccessControlList.PublicRead);  // PublicRead permissions
        s3Client.putObject(object);
        return null;
    }

    private String generatePhotoNameString() {
        String PHOTO_NAME = deviceId + "_" + currentTimeMillis + ".jpg";
        return PHOTO_NAME;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

}
