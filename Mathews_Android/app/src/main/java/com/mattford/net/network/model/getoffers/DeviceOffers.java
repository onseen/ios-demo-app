package com.mattford.net.network.model.getoffers;

import com.google.gson.annotations.SerializedName;

/**                       ^ Account   ^ Markers
 * OffersResponseModel -> Campaign -> Offer -> DeviceOffers
 *                        ^ Markers
 */

public class DeviceOffers {
    @SerializedName("type")
    public String type;
    @SerializedName("deviceName")
    public String deviceName;
    @SerializedName("identifier")
    public String identifier;
    @SerializedName("udid")
    public String udid;

}
