package com.mattford.net.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;
import android.util.DisplayMetrics;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mattford.net.tools.sharprefutil.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Request.Method.POST;
import static com.mattford.net.MyApplication.getAppContext;
import static com.mattford.net.MyApplication.getAppResources;
import static com.mattford.net.MyApplication.logD;

public class NetworkController {

    private RequestQueue requestQueue;
    private static final int MY_SOCKET_TIMEOUT_MS = 60000;

    // private static final String BASIC_URL = "https://private-anon-6ed52eacf2-onseen0.apiary-mock.com/";
    private static final String BASIC_URL = "https://api.onseen.com/";
    private static final String VERSION = "/v2/";
    private static final String OFFERS = "offers";
    private static final String CAMPAIGNS = "campaigns";
    private static final String DEVICE = "device";
    private static final String IN = "in";
    private static final String OUT = "out";
    private static final String APPLICATION_ID = "5231840d-8a86-4d42-9c7f-e40fb74dadc6";
    private String ID;
    private String deviceId = "";

    private NetworkController() {
    }

    private static class SingletonHelper {
        private static final NetworkController INSTANCE = new NetworkController();
    }

    public static NetworkController getInstance() {
        return NetworkController.SingletonHelper.INSTANCE;
    }

    private void initRequestQueue(Context context) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
    }

    private String getId(Context context) {
        if (ID == null || ID.equals("-1")) {
            SharedPrefManager sharedPrefManager = new SharedPrefManager();
            sharedPrefManager.init(context);
            ID = sharedPrefManager.getIdString();
        }
        return ID;
    }


    public String getDeviceId() {

        if (deviceId.isEmpty())
            deviceId = Settings.Secure.getString(getAppContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        return deviceId;
    }


    /**
     * My Offer List - GET - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}
     * Get New Campaign/ Offers - GET - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}/{lat}/{lng}/{Rad}
     * GET /offers/{applicationId}/{deviceId}/
     * GET /offers/{applicationId}/{deviceId}/{lat}/{lng}/{Rad}
     */
    public void sendOffersRequest(Context context, String params, final NetworkCallBack callBack) {
        // getBaseUrl();                                                                            // TODO TEST
        initRequestQueue(context);
        String methodConcat = OFFERS + VERSION + APPLICATION_ID + "/" + getId(context) + "/";
        sendGetStringRequest(context, methodConcat, params, callBack);
    }


    /**
     * Like Offer - POST - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}/like/{offerId}
     * In Bounds - POST - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}/in/{offerId}
     * Out of Bounds - POST - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}/out/{offerId}
     * Dislike Offer - POST - https://api.onseen.com/offers/v1/{applicationId}/{deviceId}/dislike/{offerId}
     * POST /offers/{applicationId}/{deviceId}/like/{offerId}
     * POST /offers/{applicationId}/{deviceId}/in/{offerId}
     * POST /offers/{applicationId}/{deviceId}/out/{offerId}
     * POST /offers/{applicationId}/{deviceId}/dislike/{offerId}
     */
    public void sendOffersActionRequest(Context context, String action, String offerId, final NetworkCallBack callBack) {
        initRequestQueue(context);
        String methodConcat = OFFERS + VERSION + APPLICATION_ID + "/" + getId(context) + "/";
        String params = action + "/" + offerId;
        sendPostRequest(context, methodConcat, params, callBack);
    }

    /**
     * Like Campaigns - POST - https://api.onseen.com/campaigns/v1/{applicationId}/{deviceId}/like/{campaignId}
     * Dislike Campaigns - POST - https://api.onseen.com/campaigns/v1/{applicationId}/{deviceId}/dislike/{campaignId}
     * POST /campaigns/{applicationId}/{deviceId}/like/{campaignId}
     * POST /campaigns/{applicationId}/{deviceId}/dislike/{campaignId}
     */
    public void sendCampaignActionRequest(Context context, String action, String campaignId, final NetworkCallBack callBack) {
        initRequestQueue(context);
        String methodConcat = CAMPAIGNS + VERSION + APPLICATION_ID + "/" + getId(context) + "/";
        String params = action + "/" + campaignId;
        sendPostRequest(context, methodConcat, params, callBack);
    }

    /**
     * Register Device - POST - https://api.onseen.com/device/v1/{applicationId}/{deviceId}
     * POST /device/{applicationId}/{deviceId}
     */
    public void sendRegisterDeviceRequest(Context context, final NetworkCallBack callBack) {
        initRequestQueue(context);
        String methodConcat = DEVICE + VERSION + APPLICATION_ID;
        sendRegisterDeviceRequest(context, methodConcat, null, callBack);
    }

    public void sendUpdateDeviceRequest(Context context, Map<String, String> params, final NetworkCallBack callBack) {
        initRequestQueue(context);
        String methodConcat = DEVICE + VERSION + APPLICATION_ID + "/" + getId(context);
        sendUpdateDeviceRequest(context, methodConcat, params, callBack);
    }

    /**
     * In Bounds - POST - https://api.onseen.com/offers/v2/{applicationId}/{deviceId}/in/{offerId}
     * Out of Bounds - POST - https://api.onseen.com/offers/v2/{applicationId}/{deviceId}/out/{offerId}
     */
    public void sendBoundsRequest(Context context, String action, String offerId, final NetworkCallBack callBack) {
        initRequestQueue(context);
        String methodConcat = OFFERS + VERSION + APPLICATION_ID + "/" + getId(context) + "/";
        String params = action + "/" + offerId;
        sendPostRequest(context, methodConcat, params, callBack);
    }

    private void sendGetStringRequest(final Context context, final String method, final String params, final NetworkCallBack callBack) {

        String URL = BASIC_URL + method + params;
        logD("sendUrl: " + URL);

        StringRequest jsonRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callBack.onCompleteString(jsonString);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onError(error, callBack);
                    }
                });
        requestQueue.add(jsonRequest);
    }

    private void sendRegisterDeviceRequest(final Context context, final String method, final Map<String, String> params, final NetworkCallBack callBack) {

        String URL = BASIC_URL + method;
        logD("sendUrl: " + URL);

        JSONObject jsonDeviceParams = null;
        try {
            jsonDeviceParams = new JSONObject(getJsonDeviceParams(context, params));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonDeviceParams != null) {
            JsonObjectRequest registerDevice = new JsonObjectRequest(URL, jsonDeviceParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String jsonString = response.toString();
                            callBack.onCompleteString(jsonString);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            onError(error, callBack);
                        }
                    });
            requestQueue.add(registerDevice);
        } else {
            callBack.onError("NO statusCode", "jsonDeviceParams is NULL");
        }
    }

    private void sendUpdateDeviceRequest(final Context context, final String method, final Map<String, String> params, final NetworkCallBack callBack) {

        String URL = BASIC_URL + method;
        logD("sendUrl: " + URL);

        JSONObject jsonDeviceParams = null;
        try {
            jsonDeviceParams = new JSONObject(getJsonDeviceParams(context, params));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (jsonDeviceParams != null) {
            JsonObjectRequest registerDevice = new JsonObjectRequest(Request.Method.PUT, URL, jsonDeviceParams,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String jsonString = response.toString();
                            callBack.onCompleteString(jsonString);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            onError(error, callBack);
                        }
                    });
            requestQueue.add(registerDevice);
        } else {
            callBack.onError("NO statusCode", "jsonDeviceParams is NULL");
        }
    }


    private void sendPostRequest(final Context context, final String method, final String params, final NetworkCallBack callBack) {

        String URL = BASIC_URL + method + params;
        logD("sendUrl: " + URL);

        StringRequest jsonReq = new StringRequest(POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callBack.onCompleteString(jsonString);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onError(error, callBack);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        // add it to the RequestQueue
        requestQueue.add(jsonReq);
    }

    public void getImageFromUrl(Context context, String imgUrl, final NetworkCallBackImage callBack) {
        initRequestQueue(context);
        ImageRequest imageRequest = new ImageRequest(imgUrl, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                callBack.onResponse(response);
            }
        }, 0, 0, null, null);
        requestQueue.add(imageRequest);
    }

    private void onError(VolleyError error, NetworkCallBack callBack) {
        if (error.networkResponse != null) {
            VolleyLog.d("Error: " + error.getMessage());
            String statusCode = String.valueOf(error.networkResponse.statusCode);
            String errorMessage = error.getMessage();
            callBack.onError(statusCode, errorMessage);
        } else {
            callBack.onError("NO statusCode", "networkResponse is NULL");
        }
    }


    private String getJsonDeviceParams(Context context, final Map<String, String> params) {
        SharedPrefManager sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(context);

        DisplayMetrics dm = getAppResources().getDisplayMetrics();
        String jsonString = "";
        if (params == null) {
            jsonString = "{\n" +
                    "  \"type\":\"" + "Android OS API Level: " +  android.os.Build.VERSION.SDK_INT + "\",\n" +
                    "  \"udid\":\"" + Settings.Secure.getString(getAppContext().getContentResolver(), Settings.Secure.ANDROID_ID) + "\",\n" +
                    "  \"imei\":\"474111111111\",\n" +
                    "  \"detail\": {\n" +
                    "    \"phone\": \"" + android.os.Build.MODEL + " " + android.os.Build.PRODUCT + "\",\n" +
                    "    \"notification\": {\n" +
                    "      \"push\": \"" + "yes" + "\",\n" +
                    "      \"email\": \"" + "yes" + "\",\n" +
                    "      \"text\": \""+ "yes" + "\"\n" +
                    "    },\n" +
                    "    \"maxHeight\": " + dm.heightPixels + ",\n" +
                    "    \"maxWidth\": " + dm.widthPixels + ",\n" +
                    "    \"tokenId\": \"" + sharedPrefManager.getFirebaseTokenString() + "\"\n" +
                    "  }\n" +
                    "}";
        } else {
            jsonString = "{\n" +
                    "  \"detail\": {\n" +
                    "    \"phone\": \"" + android.os.Build.MODEL + " " + android.os.Build.PRODUCT + "\",\n" +
                    "    \"notification\": {\n" +
//                    "      \"push\": \"" +params.get("push_n") + "\",\n" +
                    "      \"email\": \"" + params.get("email_n") + "\",\n" +
                    "      \"text\": \""+ params.get("sms_n") + "\"\n" +
                    "    },\n" +
                    "    \"maxHeight\": \"" + dm.heightPixels + "\",\n" +
                    "    \"maxWidth\": \""+ dm.widthPixels + "\",\n" +
                    "    \"name\": \"" + params.get("name") + "\",\n" +
                    "    \"username\": \"" + params.get("username") + "\",\n" +
                    "    \"email\": \"" + params.get("email") + "\",\n" +
                    "    \"tokenId\": \"" + sharedPrefManager.getFirebaseTokenString() + "\"\n" +
                    "  },\n" +
                    "  \"type\": \"" + "Android OS API Level: " +  android.os.Build.VERSION.SDK_INT + "\",\n" +
                    "  \"udid\":\"" + Settings.Secure.getString(getAppContext().getContentResolver(), Settings.Secure.ANDROID_ID) + "\",\n" +
                    "  \"imei\": \"111111111111\"\n" +
                    "}";
        }
        return jsonString;

    }

//    private String getBaseUrl() {                                                                    //TODO TEST
//        DisplayMetrics dm = getAppResources().getDisplayMetrics();
//        String url = "http://ad.mitim.com.ua/ads_android.php?system=android"
//                + "&id=" + MyApplication.getAppContext().getPackageName()
//                + "&language=" + Locale.getDefault().getLanguage()
//                + "&width=" + dm.widthPixels
//                + "&height=" + dm.heightPixels
//                + "&vendor_id=" + Settings.Secure.getString(MyApplication.getAppContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        Log.d(CPConstan.TAG, "baseUrl: " + url);
//
//        TelephonyManager telephonyManager = (TelephonyManager) MyApplication.getAppContext()
//                .getSystemService(Context.TELEPHONY_SERVICE);
//
//        Map<String, String> params = new HashMap<>();
//        params.put("id", "ff7dd650-a8b6-11e6-b57a-c17a94059125");
//        params.put("resolutionId", "d0f85a30-a8b6-11e6-891d-eb71b64dd125");
//        params.put("type", android.os.Build.MODEL + " " + android.os.Build.PRODUCT);
//        params.put("udid", "25ec6fceed1e28819f7f4eb12bc3088a60e31235");
//        params.put("imei", getIMEI(telephonyManager));
//        params.put("createdBy", "1");
//        params.put("createdOn", "2016-11-12 09:04:20.000");
//        params.put("maxHeight", String.valueOf(dm.heightPixels));
//        params.put("maxWidth", String.valueOf(dm.widthPixels));
//
//        return url;
//    }
//
//    private String getIMEI(TelephonyManager phonyManager) {
//
//        String id = phonyManager.getDeviceId();
//        if (id == null) {
//            id = "not available";
//        }
//
//        int phoneType = phonyManager.getPhoneType();
//        switch (phoneType) {
//            case TelephonyManager.PHONE_TYPE_NONE:
//                return "NONE: " + id;
//
//            case TelephonyManager.PHONE_TYPE_GSM:
//                return "GSM: IMEI=" + id;
//
//            case TelephonyManager.PHONE_TYPE_CDMA:
//                return "CDMA: MEID/ESN=" + id;
//
//            default:
//                return "UNKNOWN DEVICE: ID=" + id;
//        }
//    }
}