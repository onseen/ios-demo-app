package com.mattford.net.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mattford.net.R;
import com.mattford.net.network.model.OfferCard;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.tools.FontAvenirConfigure;


public class CardStackMapOffersAdapter extends ArrayAdapter<String> {

    private OfferCard offerCard;
    private Context mContext;

    public CardStackMapOffersAdapter(Context context, OfferCard offerCard) {
        super(context, 0);
        mContext = context;
        this.offerCard = offerCard;
    }

    @Override
    public View getView(int position, final View contentView, final ViewGroup parent) {
        ImageView logoImg = (ImageView) contentView.findViewById(R.id.logo_img);
            if (offerCard.media != null && offerCard.media.length() > 0)
                Glide.with(contentView.getContext())
                        .load(offerCard.media)
                        .crossFade()
                        .into(logoImg);

        TextView title = (TextView) contentView.findViewById(R.id.title);
        title.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
        title.setText(offerCard.name);

        TextView massage = (TextView) contentView.findViewById(R.id.subtitle);
        massage.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
        massage.setText(offerCard.title);

        ImageView contentImageView = (ImageView) contentView.findViewById(R.id.content_img);
        Glide.with(contentView.getContext())
                .load(offerCard.imgUrl)
                .fitCenter()
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        CPGlobalData.getInstance().mapUpdateButtonFragment.onUpdateButton(parent);
                        return false;
                    }
                })
                .into(contentImageView);
        CPGlobalData.getInstance().mapUpdateButtonFragment.onUpdateButton(parent);
        return contentView;
    }

    public void addOffer(OfferCard offerCard) {
        this.offerCard = offerCard;
    }

}
