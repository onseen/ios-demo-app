package com.mattford.net.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mattford.net.R;
import com.mattford.net.network.model.MyOffersCard;
import com.mattford.net.tools.FontAvenirConfigure;

import java.util.List;

public class OffersListAdapter extends RecyclerView.Adapter<OffersListAdapter.MyViewHolder> {

    private List<MyOffersCard> myOfferCardsList;
    private Context context;
    private ItemClickListener itemClickListener;

    public OffersListAdapter(Context context, List<MyOffersCard> myOfferCardsList) {
        this.context = context;
        this.myOfferCardsList = myOfferCardsList;
    }

    @Override //Create View and Inflater from layout
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Create ViewHolder for your item
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offers_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override  //Bind data
    public void onBindViewHolder(MyViewHolder holder, int position) {

        MyOffersCard myOffersCard = myOfferCardsList.get(position);

        if (myOffersCard.logoImgUrl != null && myOffersCard.logoImgUrl.length() > 0)
            Glide.with(context).load(myOffersCard.logoImgUrl).into(holder.logoImg);

        if (!myOffersCard.accountName.isEmpty())
            holder.titleTextView.setText(myOffersCard.accountName);
        else
            holder.titleTextView.setVisibility(View.GONE);

        if (!myOffersCard.title.isEmpty())
            holder.subtitleTextView.setText(myOffersCard.title);
        else
            holder.subtitleTextView.setVisibility(View.GONE);

        if (!myOffersCard.description.isEmpty())
            holder.descriptionTextView.setText(myOffersCard.description);
        else
            holder.descriptionTextView.setVisibility(View.GONE);

        holder.addressTextView.setVisibility(View.GONE);                                                                     //TODO white address
    }

    @Override
    public int getItemCount() {
        return myOfferCardsList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    // Custom View Holder class
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View mItemView;
        private ImageView logoImg;
        private ImageButton offerBtn;
        private TextView titleTextView, subtitleTextView, descriptionTextView, addressTextView;


        public MyViewHolder(View view) {
            super(view);
            mItemView = view;
            view.setOnClickListener(this);

            logoImg = (ImageView) view.findViewById(R.id.logo_img);
            offerBtn = (ImageButton) view.findViewById(R.id.offer_btn);
            offerBtn.setOnClickListener(this);

            titleTextView = (TextView) view.findViewById(R.id.title);
            subtitleTextView = (TextView) view.findViewById(R.id.subtitle);
            descriptionTextView = (TextView) view.findViewById(R.id.description);
            addressTextView = (TextView) view.findViewById(R.id.address);

            // set font
            titleTextView.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
            subtitleTextView.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
            descriptionTextView.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
            addressTextView.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);

        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && itemClickListener != null)
                if (id == offerBtn.getId()) {
                    itemClickListener.onButtonClick(view, position, myOfferCardsList.get(position));
                } else if (id == mItemView.getId()) {
                    itemClickListener.onItemClick(view, position, myOfferCardsList.get(position));
                }
        }

    }

}

