package com.mattford.net.network.model;

import com.mattford.net.network.model.getoffers.Campaign;
import com.mattford.net.network.model.getoffers.Markers;
import com.mattford.net.network.model.getoffers.Offer;
import com.google.android.gms.maps.model.Marker;

public class MarkerCard {
    public Markers mMarker;
    public String markerId;
    public Marker marker;

    public boolean isCampaignType;
    public boolean isLike;
    public boolean optIn;

    public OfferCard offerCard;
    public Campaign mCampaign;
    public Offer mOffer;

    public String id;

    public String accountName;
    public String title;
    public String subtitle;

    public double latitude;
    public double longitude;

    public int index;

    public MarkerCard(Marker marker, Campaign campaign, Markers markers, int index) {
        if (marker != null) {
            this.marker = marker;
            this.markerId = marker.getId();
            this.mMarker = markers;
        }

        this.isCampaignType = true;
        this.offerCard = new OfferCard(campaign);
        this.mCampaign = campaign;
        this.isLike = campaign.like;

        this.id = campaign.campaignId;

        this.accountName = campaign.accountName;
        this.title = campaign.name;
        this.subtitle = campaign.description;

        this.latitude = markers.lat;
        this.longitude = markers.lng;

        this.index = index;
    }

    public MarkerCard(Marker marker, Offer offer, Markers markers, int index) {
        if (marker != null) {
            this.marker = marker;
            this.markerId = marker.getId();
            this.mMarker = markers;
        }

        this.isCampaignType = false;
        this.offerCard = new OfferCard(offer);
        this.mOffer = offer;
        this.optIn = offer.optIn;

        this.id = offer.offerId;

        this.title = offer.name;
        this.subtitle = offer.description;

        this.latitude = markers.lat;
        this.longitude = markers.lng;

        this.index = index;
    }

}
