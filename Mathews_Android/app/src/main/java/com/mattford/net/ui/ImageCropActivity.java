package com.mattford.net.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lyft.android.scissors.CropView;
import com.mattford.net.R;
import com.mattford.net.network.NetworkCallBackImage;
import com.mattford.net.network.NetworkController;
import com.mattford.net.network.model.OfferCard;
import com.mattford.net.tools.CPConstan;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.tools.FontAvenirConfigure;
import com.mattford.net.tools.sharprefutil.SharedPrefManager;
import com.mattford.net.ui.fragment.MapOfferDialogFragment;
import com.mvc.imagepicker.ImagePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static android.graphics.Bitmap.CompressFormat.JPEG;
import static com.mattford.net.tools.FontAvenirConfigure.setFontViewGroup;

public class ImageCropActivity extends BaseActivity {

    private boolean mapOfferFragmentState;
    private final static ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();
    private MapOfferDialogFragment mapOfferFragment;
    private SharedPrefManager sharedPrefManager;
    private CropView cropView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());

        mapOfferFragment = MapOfferDialogFragment.newInstance();
        sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(this);
        cropView = (CropView) findViewById(R.id.cropView);
        cropView.setViewportRatio(1.0f);

        setImageToCropView(CPGlobalData.getInstance().getPickerBitmap());

        initButtons();

        setFontViewGroup((ViewGroup) findViewById(R.id.activity_image_crop), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
    }

    private void initButtons() {
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.use_photo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cropImageClick();
            }
        });
    }

    private void setImageToCropView(Bitmap bitmap) {
        if (bitmap != null && cropView != null) {
            cropView.setImageBitmap(bitmap);
        } else {
            ImagePicker.pickImage(this, "Select your image:");
        }
    }

    public void cropImageClick() {

        saveImgFromCropViewToFile();

        if (setBitmapFromCropViewToGlobal()) {
            finish();
        }
    }

    private boolean setBitmapFromCropViewToGlobal() {
        Bitmap bitmap = cropView.crop();
        if (bitmap != null) {
            CPGlobalData.getInstance().setCroppedBitmap(bitmap);
            return true;
        }
        return false;
    }

    private void saveImgFromCropViewToFile() {
        final File croppedFile = new File(getCacheDir(), "cropped.jpg");
        CPGlobalData.getInstance().setImageCropFilePath(croppedFile.getPath());         // set CropFilePath to global
        saveToFile(resizeBitmapIfNeed(cropView.crop()));
    }

    private Bitmap resizeBitmapIfNeed(Bitmap bitmap) {
        if (bitmap.getWidth() > 500 || bitmap.getHeight() > 500)
            bitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);  // Resize Bitmap
        return bitmap;
    }

    private void saveToFile(Bitmap bitmap) {
        final File croppedFile = new File(getCacheDir(), "cropped.jpg");
        saveBitmapToFile(bitmap, JPEG, 75, croppedFile);
    }

    public static Future<Void> saveBitmapToFile(final Bitmap bitmap, final Bitmap.CompressFormat format, final int quality, final File file) {
        return EXECUTOR_SERVICE.submit(new Runnable() {
            @Override
            public void run() {
                OutputStream outputStream = null;
                try {
                    file.getParentFile().mkdirs();
                    outputStream = new FileOutputStream(file);
                    bitmap.compress(format, quality, outputStream);
                    outputStream.flush();
                } catch (final Throwable throwable) {
                    if (com.lyft.android.scissors.BuildConfig.DEBUG) {
                        Log.e(CPConstan.TAG, "Error attempting to save bitmap.", throwable);
                    }
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (Exception e) {
                        Log.e(CPConstan.TAG, "Error attempting to close stream.", e);
                    }
                }
            }
        }, null);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            final Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
            setImageToCropView(bitmap);
        } catch (Exception e) {
            Toast.makeText(this, R.string.error_user_img, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_image_crop;   // set layout instead of setContentView
    }

    @Override
    public void onDislikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("dislike", offerCard.campaignId);
        } else {
            sendOffersActionRequest("dislike", offerCard.offerId);
        }
    }

    @Override
    public void onLikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("like", offerCard.campaignId);
        } else {
            sendOffersActionRequest("like", offerCard.offerId);
            CPGlobalData.getInstance().myOffersListDeprecated = true;                           // My Offers List is Deprecated
        }
    }

    @Override
    public void onMaybeButtonSelected() {
        mapOfferFragmentState = false;
        endFragment(mapOfferFragment);
    }

    @Override
    public void onShareTwitterButtonSelected(final OfferCard offerCard) {

        if (offerCard.mOffer.media != null && offerCard.mOffer.media.size() > 0) {
            String imgUrl = offerCard.mOffer.media.get(0).replace(" ", "+");
            showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                    hideLoader();
                    shareTwitter(offerCard, response, true);
                }
            });
        } else {
            shareTwitter(offerCard, null, false);
        }
    }
}
