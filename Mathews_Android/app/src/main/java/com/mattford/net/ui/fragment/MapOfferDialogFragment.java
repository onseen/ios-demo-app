package com.mattford.net.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mattford.net.R;
import com.mattford.net.network.model.OfferCard;
import com.mattford.net.network.model.getoffers.Offer;
import com.mattford.net.tools.CPConstan;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.ui.adapter.CardStackMapOffersAdapter;
import com.mattford.net.ui.cardstatck.cardstack.CardStack;

import java.util.ArrayList;
import java.util.List;

import static com.mattford.net.R.id.dislike_btn;
import static com.mattford.net.R.id.like_btn;
import static com.mattford.net.R.id.maybe_btn;
import static com.mattford.net.R.id.share_twitter_btn;
import static com.mattford.net.R.layout.map_offer_fragment_layout;
import static com.mattford.net.tools.CPConstan.UPDATE_BUTTON_DELAY;

public class MapOfferDialogFragment extends Fragment {
    private static final int SWIPE_RIGHT = 1;
    private static final int SWIPE_LEFT = 0;

    private Context mContext;
    private OnMapOfferDialogFragmentSelectedListener mListener;
    private Button dislikeButton, likeButton, maybeButton, shareTwitterButton;

    private CardStack.CardEventListener mCardStackListener;
    private CardStack cardStack;

    //this class is using for swipe the AdapterView
    private CardStackMapOffersAdapter mCardAdapter;
    private int position = 0;
    private int cardIndex = 0;
    private OfferCard offerCard;
    private List<OfferCard> offersCardList;
    private List<OfferCard> offerCards;

    public UpdateMapButtonFragment mapUpdateButtonFragment;
    private LinearLayout buttonGroup;


    public static MapOfferDialogFragment newInstance() {
        return new MapOfferDialogFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        CPGlobalData globalData = CPGlobalData.getInstance();

        // callback for move buttons view below card view
        mapUpdateButtonFragment = new UpdateMapButtonFragment() {
            @Override
            public void onUpdateButton(ViewGroup view) {
                updateButtonDelay(view);
            }
        };
        globalData.mapUpdateButtonFragment = mapUpdateButtonFragment;

        if (globalData.isCurrentMarker) {
            this.offerCard = globalData.markerCardsList.get(globalData.indexCurrentMarker).offerCard;
        } else if (globalData.isOfferFromDevice) {
            this.offersCardList = globalData.offersCardList;
        } else {
            this.offerCard = globalData.offerCard;
        }
        try {
            mListener = (OnMapOfferDialogFragmentSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity should implement " + OnMapOfferDialogFragmentSelectedListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        CPGlobalData.getInstance().isCurrentMarker = false;
        CPGlobalData.getInstance().isOfferFromDevice = false;
        CPGlobalData.getInstance().offersCardList = null;
        position = 0;
        cardIndex = 0;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(map_offer_fragment_layout, container, false);

        createCardUI(view);

        callCardStack();

        setOnClickToButton(view);

        return view;
    }

    private void updateButtonDelay(final ViewGroup parent) {
        if (buttonGroup != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (parent.getChildAt(3) != null && parent.getChildAt(3).getHeight() > 0) {
                        updateAnimButton(parent);
                    } else {
                        updateButtonDelay(parent);
                    }
                }
            }, UPDATE_BUTTON_DELAY);
        }
    }

    // move buttons view below card view
    private void updateAnimButton(ViewGroup parent) {
        int screenWidth = CPGlobalData.getInstance().widthDisplayPix;
        int screenHeight = CPGlobalData.getInstance().heightDisplayPix;
        int buttonGroupWidth = buttonGroup.getWidth();
        int cardViewHeight = parent.getChildAt(3).getHeight();
        int cardViewTopPadding = mContext.getResources().getDimensionPixelSize(R.dimen.top_cardstack_padding);
        int X = screenWidth / 2 - buttonGroupWidth / 2;
        int Y = cardViewTopPadding + cardViewHeight;
        int maxY = screenHeight - buttonGroup.getHeight();
        if (Y > maxY) Y = maxY;
        buttonGroup.bringToFront();
        buttonGroup.animate().setDuration(CPConstan.BUTTON_GROUP_DURATION).x(X).y(Y);   // anim выезда
    }

    private void createCardUI(View view) {
        //cardStack initialization
        cardStack = (CardStack) view.findViewById(R.id.frame_map_offer);
        buttonGroup = (LinearLayout) view.findViewById(R.id.buttonGroup);

        if (CPGlobalData.getInstance().isOfferFromDevice) {

            offerCards = CPGlobalData.getInstance().offersCardList;

        } else if (offerCard != null) {

            offerCards = new ArrayList<>();
            if (offerCard.isCampaignType) {                                                     //TODO add Campaign to list if not Like
                if (offerCard.isLike == CPConstan.LIKE)
                    offerCards.add(new OfferCard(offerCard.mCampaign));
                if (offerCard.mCampaign.offers != null && offerCard.mCampaign.offers.size() > 0)
                    for (int i = 0; i < offerCard.mCampaign.offers.size(); i++) {
                        Offer offer = offerCard.mCampaign.offers.get(i);
                        if (offer.optIn == CPConstan.OPT_IN)                                    //TODO add Offer to list if optIn false
                            offerCards.add(new OfferCard(offer));
                    }
            } else if (!offerCard.isCampaignType && offerCard.mOffer != null && offerCard.optIn == CPConstan.OPT_IN) {
                offerCards.add(new OfferCard(offerCard.mOffer));                               //TODO add Offer to list
            }
        }

        //creating adapter and add firs offerCard
        if (offerCards != null && offerCards.size() > 0) {
            mCardAdapter = new CardStackMapOffersAdapter(getActivity().getApplicationContext(), offerCards.get(position));
        } else {                                                                                 //TODO test empty Offer
            mCardAdapter = new CardStackMapOffersAdapter(getActivity().getApplicationContext(), new OfferCard());
        }

    }

    private void setOnClickToButton(View view) {

        dislikeButton = (Button) view.findViewById(dislike_btn);
        dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.discardTopCard(SWIPE_LEFT);
            }
        });

        likeButton = (Button) view.findViewById(like_btn);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.discardTopCard(SWIPE_RIGHT);
            }
        });

        maybeButton = (Button) view.findViewById(maybe_btn);
        maybeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onMaybeButtonSelected();
            }
        });

        shareTwitterButton = (Button) view.findViewById(share_twitter_btn);
        shareTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onShareTwitterButtonSelected(offerCards.get(cardIndex));
            }
        });

        setTwitterBtn(position);

    }


    //cardStack view will set it as visible and load the information into stack.
    private void callCardStack() {

        //Setting Resource of CardStack
        cardStack.setContentResource(R.layout.cardview_map_offer_fragment);

        //Adding first item for CardStack
        mCardAdapter.add("");
        cardStack.setAdapter(mCardAdapter);

        mCardStackListener = new CardStack.CardEventListener() {

            @Override
            public boolean swipeEnd(int section, float distance) {
                return distance > CPConstan.threshold;
            }

            @Override
            public boolean swipeStart(int section, float distance) {

                return true;
            }

            @Override
            public boolean swipeContinue(int section, float distanceX, float distanceY) {
                return true;
            }

            //section
            // 0 | 1
            //--------
            // 2 | 3
            @Override
            public void discarded(int mIndex, int direction) {
                checkDirectionForSendAction(direction);
            }

            @Override
            public void topCardTapped() {

            }

        };

        cardStack.setListener(mCardStackListener);

    }

    private void checkDirectionForSendAction(int direction) {
        switch (direction) {
            case 0:
            case 2:
                mListener.onDislikeButtonSelected(offerCards.get(cardIndex));
                if ((offerCards.get(cardIndex).isCampaignType)) {
                    position = offerCards.size();
                } else {
                    reinitOfferCards();
                }
                break;

            case 1:
            case 3:
                mListener.onLikeButtonSelected(offerCards.get(cardIndex));
                reinitOfferCards();
                break;
        }

        if (position < offerCards.size()) {
            cardStack.reset(true);
            cardIndex++;
            updateButtonDelay(cardStack);
        } else {
            cardStack.reset(false);
            endFragment(CPConstan.END_FRAGMENT_DURATION);
        }
    }

    private boolean reinitOfferCards() {
        position += 1;
        setTwitterBtn(position);
        if (offerCards.size() > 1 && position < offerCards.size()) {
            mCardAdapter.addOffer(offerCards.get(position));
            return true;
        }
        return false;
    }

    private void setTwitterBtn(int position) {
        if (offerCards != null && offerCards.size() > position && offerCards.get(position).isCampaignType) {
            shareTwitterButton.setVisibility(View.GONE);
        } else {
            shareTwitterButton.setVisibility(View.VISIBLE);
        }
    }

    private void endFragment(long DURATION) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListener.onMaybeButtonSelected();
            }
        }, DURATION);
    }

    public interface OnMapOfferDialogFragmentSelectedListener {

        void onDislikeButtonSelected(OfferCard offerCard);

        void onLikeButtonSelected(OfferCard offerCard);

        void onMaybeButtonSelected();

        void onShareTwitterButtonSelected(OfferCard offerCard);

    }
}

