package com.mattford.net.tools.firebaseutil;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.tools.sharprefutil.SharedPrefManager;


/**
 * Firebase Instance ID token refresh events
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseRefreshedToken";

    @Override
    public void onTokenRefresh() {

        String refreshedToken  = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        CPGlobalData.getInstance().refreshedToken = refreshedToken;
        SharedPrefManager sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(this);
        sharedPrefManager.setFirebaseTokenString(refreshedToken); // save Token to file
    }
}
