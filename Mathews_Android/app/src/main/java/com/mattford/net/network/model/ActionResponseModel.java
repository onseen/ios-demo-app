package com.mattford.net.network.model;

import com.google.gson.annotations.SerializedName;

public class ActionResponseModel {
    @SerializedName("success")
    public Boolean success;
    @SerializedName("data")
    public Object data;
    @SerializedName("error")
    public String error;
}
