package com.mattford.net.tools;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Sets the font on all TextViews and Button in the ViewGroup.
 *
 * Initial:  FontAvenirConfigure.getInstance(getApplicationContext());
 * Used from root layou:  setFontViewGroup((ViewGroup)findViewById(R.id.liner_layout), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
 * Used from one view:    setFontTextView((TextView)findViewById(R.id.text_view), FontAvenirConfigure.FONT_AVENIR_NEXT_BOLD);
 *                 or:    textView.setTypeface(FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
 *
 * and add fonts files to ..app\src\main\assets\fonts\*
 */

public class FontAvenirConfigure {

    public static Typeface FONT_AVENIR_NEXT_REGULAR;
    public static Typeface FONT_AVENIR_NEXT_BOLD;
    public static Typeface FONT_AVENIR_NEXT_ITALIC;
    public static Typeface FONT_AVENIR_NEXT_BOLD_ITALIC;
    private static Context context;

    private FontAvenirConfigure() {
        FONT_AVENIR_NEXT_REGULAR = Typeface.createFromAsset(context.getAssets(),     "fonts/AvenirNext-Regular.ttf");
        FONT_AVENIR_NEXT_BOLD = Typeface.createFromAsset(context.getAssets(),        "fonts/AvenirNext-Bold.ttf");
        FONT_AVENIR_NEXT_ITALIC = Typeface.createFromAsset(context.getAssets(),      "fonts/AvenirNext-Italic.ttf");
        FONT_AVENIR_NEXT_BOLD_ITALIC = Typeface.createFromAsset(context.getAssets(), "fonts/AvenirNext-BoldItalic.ttf");
    }

    private static class SingletonHelper {
        private static final FontAvenirConfigure INSTANCE = new FontAvenirConfigure();
    }

    public static FontAvenirConfigure getInstance(Context context) {
        FontAvenirConfigure.context = context;
        return FontAvenirConfigure.SingletonHelper.INSTANCE;
    }

    /**
     * Sets the font on all TextViews in the ViewGroup. Searches
     * recursively for all inner ViewGroups as well. Just add a check
     * for any other views you want to set as well (EditText, Button, etc.)
     */
    public static void setFontViewGroup(ViewGroup group, Typeface font) {
        if (FontAvenirConfigure.SingletonHelper.INSTANCE != null) {
            int count = group.getChildCount();
            View v;
            for (int i = 0; i < count; i++) {
                v = group.getChildAt(i);
                if (v instanceof TextView || v instanceof Button)
                    ((TextView) v).setTypeface(font);
                else if (v instanceof ViewGroup)
                    setFontViewGroup((ViewGroup) v, font);
            }
        }
    }

    public static void setFontTextView(TextView textView, Typeface font) {
        if (FontAvenirConfigure.SingletonHelper.INSTANCE != null && textView != null && font != null) {
            textView.setTypeface(font);
        }
    }

    public static void setFontButton(Button buttonView, Typeface font) {
        if (FontAvenirConfigure.SingletonHelper.INSTANCE != null && buttonView != null && font != null) {
            buttonView.setTypeface(font);
        }
    }
}
