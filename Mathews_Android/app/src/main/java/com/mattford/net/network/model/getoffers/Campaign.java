package com.mattford.net.network.model.getoffers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**                       ^ Account   ^ Markers
 * OffersResponseModel -> Campaign -> Offer -> DeviceOffers
 *                        ^ Markers
 */

public class Campaign {
    @SerializedName("id")
    public String id;
    @SerializedName("userId")
    public String userId;
    @SerializedName("accountId")
    public String accountId;
    @SerializedName("accountName")
    public String accountName;
    @SerializedName("name")
    public String name;
    @SerializedName("effectiveDate")
    public String effectiveDate;
    @SerializedName("expirationDate")
    public String expirationDate;
    @SerializedName("description")
    public String description;
    @SerializedName("categories")
    public String categories;
    @SerializedName("ages")
    public String ages;
    @SerializedName("media")
    public List<String> media = new ArrayList<String>();
    @SerializedName("markers")
    public List<Markers> markers = new ArrayList<Markers>();
    @SerializedName("campaignId")
    public String campaignId;
    @SerializedName("like")
    public Boolean like;
    @SerializedName("applicationId")
    public String applicationId;
    @SerializedName("deviceId")
    public String deviceId;
    @SerializedName("offers")
    public List<Offer> offers = new ArrayList<Offer>();
    @SerializedName("account")
    public Account account;

}
