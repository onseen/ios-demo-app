package com.mattford.net.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mattford.net.MyApplication;
import com.mattford.net.R;
import com.mattford.net.network.NetworkCallBack;
import com.mattford.net.network.NetworkCallBackImage;
import com.mattford.net.network.NetworkController;
import com.mattford.net.network.model.DeviceByOffers;
import com.mattford.net.network.model.DeviceRegModel;
import com.mattford.net.network.model.MarkerCard;
import com.mattford.net.network.model.OfferCard;
import com.mattford.net.network.model.getoffers.Campaign;
import com.mattford.net.network.model.getoffers.DeviceOffers;
import com.mattford.net.network.model.getoffers.Offer;
import com.mattford.net.network.model.getoffers.OffersResponseModel;
import com.mattford.net.tools.CPConstan;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.tools.FontAvenirConfigure;
import com.mattford.net.tools.btutil.BtEstimoteService;
import com.mattford.net.tools.gpsutil.GPSService;
import com.mattford.net.tools.sharprefutil.SharedPrefManager;
import com.mattford.net.tools.wifiutil.WifiService;
import com.mattford.net.ui.adapter.InfoAdapter;
import com.mattford.net.ui.fragment.MapOfferDialogFragment;
import com.mattford.net.ui.fragment.ProgressDialogFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.mattford.net.MyApplication.logD;
import static com.mattford.net.tools.FontAvenirConfigure.setFontViewGroup;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapOfferDialogFragment.OnMapOfferDialogFragmentSelectedListener, View.OnClickListener {

    public final static String BROADCAST_FOUND_OFFER = "found_offer_from_device";
    private GoogleMap mMap;
    private GoogleApiClient.ConnectionCallbacks listener;
    private GoogleApiClient.OnConnectionFailedListener failListener;
    private GoogleApiClient mClient;
    private boolean isMapReady;
    private GoogleMap.CancelableCallback callback;
    private ImageButton offersBtn, profileBtn;
    private boolean campaignsFilterState = true;
    private boolean offerFilterState = true;
    private ImageView campaignsFilterImg;
    private ImageView offerFilterImg;
    private MapOfferDialogFragment offerFragment;
    private boolean offerFragmentState;
    private String id;
    private List<MarkerCard> markerCardsList;
    private List<DeviceByOffers> deviceByOffersList;

    private Context context;
    private SharedPrefManager sharedPrefManager;
    private String idCurrentMarker;
    private IntentFilter intentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private Location location;
    private List<Region> deviceRegionList;
    private long initLaunchTime;
    private boolean moveCameraNyState = true;
    private Set<String> newHashSetMarkers = new HashSet();
    private Set<String> hashSetMarkers = new HashSet();
    private boolean testState;
    private DrawerLayout mDrawerLayout;
    private ImageButton fab;
    private Dialog profileDialog;
    private Dialog walletDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.map_navigation_layout);
        FontAvenirConfigure.getInstance(getApplicationContext());  // initial Fonts

        sharedPrefManager = new SharedPrefManager();
        sharedPrefManager.init(this);
        id = sharedPrefManager.getIdString();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        CPGlobalData.getInstance().dm = displayMetrics;
        CPGlobalData.getInstance().widthDisplayPix = displayMetrics.widthPixels;
        CPGlobalData.getInstance().heightDisplayPix = displayMetrics.heightPixels;
        CPConstan.threshold = displayMetrics.widthPixels / 4;

        CPGlobalData.getInstance().markerCardsList = markerCardsList = new ArrayList<MarkerCard>();

        startBtService();
        initReceiver();
        navigationView();
        initView();

        setRegisterDevice();

        initLocation();
        connectToLocation();

        setFontViewGroup((ViewGroup) findViewById(R.id.map_layout), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);

    }

    private void navigationView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState(); // делает кнопку динамической (поворот в стрелку при взаимодействии)
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // слушатель по кликам в меню Navigation
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();
                String url = "";
                if (id == R.id.nav_view_inventory) {                                            //TODO INTENT
                    url = "http://www.mathewsnewark.com/inventory.cfm?reset=true&viewnewused=new";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.view_inventory);
                } else if (id == R.id.nav_service_scheduling) {
                    url = "http://mathewsfordnewark.com/Service-Appointment/";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.service_scheduling);
                } else if (id == R.id.nav_finance_products) {
                    url = "http://www.mathewsnewark.com/content.cfm?contentId=1561";// url = "http://mathewsfordnewark.com/Finance.aspx";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.finance_products);
                } else if (id == R.id.nav_lease_end_process) {
                    url = "https://www.ford.com/finance/lease-end";// url = "http://www.fordcredit.com/finance/lease-end";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.lease_end_process);
                } else if (id == R.id.nav_contact_info) {
                    url = "http://www.mathewsnewark.com/contact.cfm";//  url = "http://mathewsfordnewark.com/ContactUs.aspx";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.contact_info);
                } else if (id == R.id.nav_staff_bios) {
                    // url = "http://mathewsfordnewark.com/Staff.aspx";//  url = "http://www.mathewsnewark.com/staff.cfm?dkid=706sales";
                    url = "http://www.mathewsnewark.com/staff.cfm?dkid=706sales";
                    startActivityDelayed(MapsActivity.this, WebViewActivity.class, url, R.string.staff_bios);
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    public void startActivityDelayed(final Context activityContext, final Class<? extends Activity> activityClass, final String url, final int titleId) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activityContext, activityClass);
                intent.putExtra("url", url);
                intent.putExtra("title", getResources().getString(titleId));
                startActivity(intent);
            }
        }, 180);
    }

    private void initView() {
        initLaunchTime = System.currentTimeMillis();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        CPGlobalData.getInstance().mScreenHeight = displaymetrics.heightPixels;

        offersBtn = (ImageButton) findViewById(R.id.navigation_btn);
        profileBtn = (ImageButton) findViewById(R.id.profile_btn);

        offerFragment = MapOfferDialogFragment.newInstance();

        offersBtn.setOnClickListener(this);
        profileBtn.setOnClickListener(this);

        fab = (ImageButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    private void checkDialogState() {
        if (sharedPrefManager != null)
            if (!sharedPrefManager.getProfileState()) {
                initProfileDialog();
            } else if (!sharedPrefManager.getWalletState()) {
                haveAppWalletPasses();
            }
    }


    private void initProfileDialog() {
        profileDialog = onCreateDialog(this);                   // Create Dialog
        profileDialog.show();
        sharedPrefManager.setProfileState(true);
    }

    private void initWalletDialog() {
        walletDialog = onCreateDialogWallet(this);                   // Create Dialog
        walletDialog.show();
        sharedPrefManager.setWalletState(true);
    }

    private void haveAppWalletPasses() {
        String packageNameVerifiable = "io.walletpasses.android";
        boolean resolved = checkUserInstalledApps(packageNameVerifiable);
        if (!resolved) {
            initWalletDialog();
        }
//            else {
//                Toast.makeText(this, "WalletPasses found", Toast.LENGTH_LONG).show();              // TODO DEBUG
//            }
    }

    private boolean checkUserInstalledApps(String packageNameVerifiable) {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> apps = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo app : apps) {
            if (pm.getLaunchIntentForPackage(app.packageName) != null) {
                // All apps with launcher intent
                if (app.packageName.startsWith(packageNameVerifiable)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Dialog onCreateDialog(Context viewContext) { // TODO need redesign
        DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_POSITIVE:// положительная кнопка
                        startActivity(new Intent(MapsActivity.this, ProfileActivity.class));
                        break;
                    case Dialog.BUTTON_NEGATIVE:// негативная кнопка
                        haveAppWalletPasses();
                        break;
                    case Dialog.BUTTON_NEUTRAL:// нейтральная кнопка
                        break;
                }
            }
        };
        AlertDialog.Builder adb = new AlertDialog.Builder(viewContext);
        adb.setTitle("Profile");                                  // заголовок
        adb.setMessage("Please take a minute to complete your Profile");// сообщение
        adb.setIcon(R.drawable.ic_error_black_48px_v);           // иконка
        adb.setPositiveButton("GO", myClickListener);             // кнопка положительного ответа
        adb.setNegativeButton("CANCEL", myClickListener);         // кнопка отрицательного ответа
        //adb.setNeutralButton(R.string.cancel, myClickListener); // кнопка нейтрального ответа
        return adb.create();                                      // создаем диалог
    }

    protected Dialog onCreateDialogWallet(Context viewContext) { // TODO need redesign
        DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_POSITIVE:// положительная кнопка
                        Intent marketIntent = new Intent();
                        marketIntent.setData(Uri.parse("market://details?id=io.walletpasses.android"));
                        startActivity(marketIntent);
                        break;
                    case Dialog.BUTTON_NEGATIVE:// негативная кнопка
                        break;
                    case Dialog.BUTTON_NEUTRAL:// нейтральная кнопка
                        break;
                }
            }
        };
        AlertDialog.Builder adb = new AlertDialog.Builder(viewContext);
        adb.setTitle("Wallet");                                  // заголовок
        adb.setMessage("Any Offer you encounter here can also be downloaded and installed in your Mobile Wallet. Download WalletPasses");// сообщение
        adb.setIcon(R.drawable.ic_error_black_48px_v);           // иконка
        adb.setPositiveButton("OK", myClickListener);             // кнопка положительного ответа
        adb.setNegativeButton("CANCEL", myClickListener);         // кнопка отрицательного ответа
        //adb.setNeutralButton(R.string.cancel, myClickListener); // кнопка нейтрального ответа
        return adb.create();                                      // создаем диалог
    }

    private void setRegisterDevice() {
        if (id != null && id.equals("-1")) {
            NetworkController.getInstance().sendRegisterDeviceRequest(this, new NetworkCallBack() {
                @Override
                public void onCompleteString(String jsonString) {
                    DeviceRegModel deviceModel = new Gson().fromJson(jsonString, DeviceRegModel.class);
                    if (deviceModel != null && deviceModel.success) {
                        id = deviceModel.data.id;
                        sharedPrefManager.setIdString(id);
                        getOffers();
                    }
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    //    Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_LONG).show();                   // TODO DEBUG
                }
            });
        }
    }

    private void sendCampaignActionRequest(final String action, final String idCampaign) {
        NetworkController.getInstance().sendCampaignActionRequest(this, action, idCampaign, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
            }
        });
    }

    private void sendOffersActionRequest(final String action, final String idOffer) {
        NetworkController.getInstance().sendOffersActionRequest(this, action, idOffer, new NetworkCallBack() {
            @Override
            public void onCompleteString(String jsonString) {
            }


            @Override
            public void onError(String errorCode, String errorMessage) {
            }

        });
    }


    private void getOffers() {
        if (location != null) {
            String lat_lon_rad = location.getLongitude() + "/" + location.getLatitude() + "/" + CPConstan.RADIUS;
            NetworkController.getInstance().sendOffersRequest(this, lat_lon_rad, new NetworkCallBack() {
                @Override
                public void onCompleteString(String jsonString) {
//                    if (testState) {
//                    jsonString = JSON_GET_OFFER_TEST;                                                                                // TODO TEST_JSON
//                        testState = false;
//                    }
                    onCompleteResponse(jsonString);
                    logD("getOffers Complete: location != null");
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    //     Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show();                // TODO DEBUG
                }
            });
        } else {
            if ((initLaunchTime + CPConstan.GET_DEFAULT_OFFER_DURATION) > System.currentTimeMillis()) {
                Log.d(CPConstan.TAG, "white location");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mClient != null) {
                            mClient.disconnect();
                            mClient.connect();
                        }
                        getOffers();
                    }
                }, CPConstan.GET_OFFER_DURATION);
            } else {
                Log.d(CPConstan.TAG, "send NY");
                //  40.709993/-74.007109/1000.00  NY
                String lat_lon_rad = "40.709993" + "/" + "-74.007109" + "/" + CPConstan.RADIUS;
                NetworkController.getInstance().sendOffersRequest(this, lat_lon_rad, new NetworkCallBack() {
                    @Override
                    public void onCompleteString(String jsonString) {
//                        if (testState) {
//                        jsonString = JSON_GET_OFFER_TEST;                                                                                // TODO TEST_JSON
//                            testState = false;
//                        }
                        onCompleteResponse(jsonString);
                        moveCameraNY();
                        logD("getOffers Complete: location == null");
                    }

                    @Override
                    public void onError(String errorCode, String errorMessage) {
                        //     Toast.makeText(getApplicationContext(), errorCode + errorMessage, Toast.LENGTH_SHORT).show();    // TODO DEBUG
                    }
                });
            }
        }
    }

    private void moveCameraNY() {
        if (moveCameraNyState) {
            moveCameraNyState = false;
            //Move the camera to the user's location and zoom in!
            LatLng mPosition = new LatLng(40.709993, -74.007109);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mPosition));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mPosition, 10.0f), 10, callback);      // 10.0f - zoom on city
        }
    }

    private void onCompleteResponse(String jsonString) {
        OffersResponseModel offersResponseModelModel = null;
        try {
            offersResponseModelModel = new Gson().fromJson(jsonString, OffersResponseModel.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        if (offersResponseModelModel != null && offersResponseModelModel.data != null) {
            addAllMarker(offersResponseModelModel);
            if (CPGlobalData.getInstance().beaconManager != null && CPGlobalData.getInstance().beaconManagerIsReady)
                startMonitorAllBeacons();                                                                                       // MONITORING BEACONS
        } else if (offersResponseModelModel.data == null && offersResponseModelModel.error.equals("No more new offers!")) {
            //      Toast.makeText(getApplicationContext(), R.string.no_more_offer, Toast.LENGTH_SHORT).show();
        }
    }

    private void startMonitorAllBeacons() {
        if (deviceByOffersList != null && !deviceByOffersList.isEmpty() && CPGlobalData.getInstance().beaconManagerIsReady) {
            deviceRegionList = new ArrayList<>();
            BeaconManager bt = CPGlobalData.getInstance().beaconManager;
            for (int i = 0; i < deviceByOffersList.size(); i++) {
                DeviceByOffers device = deviceByOffersList.get(i);
                if (device.iBeacon) {
                    String identifier = device.offerId;
                    String udid = device.deviceUdid;
                    int major = device.major;
                    int minor = device.minor;
                    if (!udid.isEmpty() && major > 0 || minor > 0) {
                        try {
                            Region region = new Region(identifier, UUID.fromString(udid), major, minor);
                            bt.startMonitoring(region);
                            bt.startRanging(region);
                            deviceRegionList.add(region);
                        } catch (Exception e) {
                            e.printStackTrace();
//                            FirebaseCrash.log("Error device.udid: " + device.device.udid);
                        }
                    } else if (major == 0 && minor == 0) {                                                                   // TODO
//                        FirebaseCrash.log(device.device.udid + " (major && minor) == 0");
                    }
                }
            }
        }
    }

    private void stopMonitorAllRegions() {
        if (deviceRegionList != null && !deviceRegionList.isEmpty() && CPGlobalData.getInstance().beaconManagerIsReady) {
            for (int i = 0; i < deviceRegionList.size(); i++) {
                BeaconManager bt = CPGlobalData.getInstance().beaconManager;
                bt.stopMonitoring(deviceRegionList.get(i));
                bt.stopRanging(deviceRegionList.get(i));
            }
            deviceRegionList = null;
        }
    }

    private void onPreAddAllMarker() {
        if (markerCardsList == null)
            markerCardsList = new ArrayList<>();
        CPGlobalData.getInstance().markerCardsList = markerCardsList;
        deviceByOffersList = new ArrayList<>();
    }

    private void addAllMarker(OffersResponseModel model) {
        onPreAddAllMarker();
        Campaign campaign;
        Offer offer;
        if (model.data != null && !model.data.isEmpty()) {
            for (int i = 0; i < model.data.size(); i++) {
                campaign = model.data.get(i);
                if (campaign.like == CPConstan.LIKE) {                                                             // SETTINGS - visible campaign
                    addMarker(campaign);
                    if (campaign.offers != null && !campaign.offers.isEmpty())
                        for (int j = 0; j < campaign.offers.size(); j++) {
                            offer = campaign.offers.get(j);
/*                            if (offer.optIn == CPConstan.OPT_IN) {                                                // SETTINGS - visible offers
                                addOfferMarker(offer);
                            }*/
                            addDevicesByOffer(offer);                                                               // MONITOR offers
                        }
                } else if (campaign.offers != null && !campaign.offers.isEmpty())
                    for (int j = 0; j < campaign.offers.size(); j++) {
                        offer = campaign.offers.get(j);
                        if (offer.optIn == CPConstan.OPT_IN) {                                                      // SETTINGS - visible offers
                            addMarker(offer);
                        }
                        addDevicesByOffer(offer);                                                                   // MONITOR offers
                    }
            }
            onPostAddAllMarker();
        }
    }

    private void onPostAddAllMarker() {
        mMap.setInfoWindowAdapter(new InfoAdapter(getLayoutInflater(), markerCardsList));
        CPGlobalData.getInstance().deviceByOffersList = deviceByOffersList;                                    // deviceByOffersList instance
        startWiFiService();
        cleanMarkersOnTheMap();
        filteredMarkersOnTheMap();
    }

    private void cleanMarkersOnTheMap() {
        for (Iterator<String> i = hashSetMarkers.iterator(); i.hasNext(); ) {
            String element = i.next();
            if (newHashSetMarkers.isEmpty() || !newHashSetMarkers.remove(element)) {
                if (!markerCardsList.isEmpty())
                    first:for (Iterator<MarkerCard> j = markerCardsList.iterator(); j.hasNext(); ) {
                        MarkerCard markerCard = j.next();
                        if (markerCard.id.equals(element)) {
                            markerCard.marker.setVisible(false);
                            j.remove();
                            i.remove();
                            break first;
                        }
                    }
            }
        }
    }

    private void removeAllMarker() {
        if (markerCardsList != null && markerCardsList.size() > 0)
            for (int i = 0; i < markerCardsList.size(); i++) {
                markerCardsList.get(i).marker.setVisible(false);
            }
    }

    private void addMarker(Campaign campaign) {
        if (isMapReady && campaign != null && campaign.markers != null && !campaign.markers.isEmpty()) {
            for (int i = 0; i < campaign.markers.size(); i++) {
                if (campaign.like == CPConstan.LIKE) {                                                                // if Campaign not Like - show and add to list
                    MarkerOptions markerYellow = new MarkerOptions()
                            .position(new LatLng((campaign.markers.get(i).lat), (campaign.markers.get(i).lng)))
                            .title(campaign.name)
                            .snippet(campaign.description)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_pin));
                    newHashSetMarkers.add(campaign.campaignId);
                    if (hashSetMarkers.add(campaign.campaignId)) {                                                  // add offer/marker to HashSet list
                        Marker marker = mMap.addMarker(markerYellow);
                        MarkerCard markerCard = new MarkerCard(marker, campaign, campaign.markers.get(i), i);
                        markerCardsList.add(markerCard);
                    }
                }
            }
        }
    }

    private void addMarker(Offer offer) {
        if (isMapReady && offer != null && offer.markers != null && !offer.markers.isEmpty()) {
            for (int i = 0; i < offer.markers.size(); i++) {
                if (offer.optIn == CPConstan.OPT_IN) {                                                                // if Offer not optIn - show and add to list
                    MarkerOptions markerBlue = new MarkerOptions()
                            .position(new LatLng((offer.markers.get(i).lat), (offer.markers.get(i).lng)))
                            .title(offer.name)
                            .snippet(offer.description)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_pin));
                    newHashSetMarkers.add(offer.offerId);
                    if (hashSetMarkers.add(offer.offerId)) {                                                       // add offer/marker to HashSet list
                        Marker marker = mMap.addMarker(markerBlue);
                        MarkerCard markerCard = new MarkerCard(marker, offer, offer.markers.get(i), i);
                        markerCardsList.add(markerCard);
                    }
                }
            }
        }
    }

    private void addDevicesByOffer(Offer offer) {
        if (offer.devices != null && !offer.devices.isEmpty()) {
            for (int i = 0; i < offer.devices.size(); i++) {
                DeviceOffers device = offer.devices.get(i);
                if (device.type != null && !device.type.isEmpty()) {
                    if (device.type.equalsIgnoreCase("iBeacon") || device.type.equalsIgnoreCase("macaddress")) {
                        DeviceByOffers deviceByOffers = new DeviceByOffers(offer, device);
                        deviceByOffersList.add(deviceByOffers);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (offerFragmentState) {
            endFragment(offerFragment);
        } else if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void initLocation() {
        listener = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                onConnectedLocation();
            }

            @Override
            public void onConnectionSuspended(int i) {
            }
        };

        failListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
            }
        };

        mClient = new GoogleApiClient.Builder(MapsActivity.this)
                .addConnectionCallbacks(listener)
                .addOnConnectionFailedListener(failListener)
                .addApi(LocationServices.API)
                .build();

        callback = new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onCancel() {
            }
        };
    }

    private void onConnectedLocation() {
        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this, // TODO: Consider calling permissions
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, 0);
            return;
        } else {
            startGpsService();
            location = LocationServices.FusedLocationApi.getLastLocation(mClient);
            //  if (!BuildConfig.DEBUG)                                                                                // TODO NO AUTO MOVE CAMERA in DEBUG
            if (location != null && isMapReady && location.getLatitude() > 0 && location.getLongitude() > 0) {
                //Move the camera to the user's location and zoom in!
                LatLng mPosition = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(mPosition));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mPosition, 10.0f), 10, callback);      // 10.0f - zoom on city
                mMap.setMyLocationEnabled(true);                                                            // Camera on my location
                moveCameraNyState = false;
            }
            if (location != null && id != null) {
                if (id != null) {
                    getOffers();
                }
            }
        }
    }

    private void connectToLocation() {
        mClient.connect();
    }

    @Override
    protected void onDestroy() {
        stopServices();
        stopMonitorAllRegions();
        mClient.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        checkDialogState();
        registerReceiver(mBroadcastReceiver, intentFilter);
        checkOfferFromDevice();
        getOffers();
    }

    @Override
    protected void onPause() {
        MyApplication.activityPaused();
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        isMapReady = true;
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);//Disable Map Toolbar:
        mMap.getUiSettings().setZoomControlsEnabled(false);//Disable Zoom Buttons:
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                idCurrentMarker = marker.getId();
                getCurrentMarkerAndModel(idCurrentMarker);
            }
        });

    }

    private void getCurrentMarkerAndModel(String idCurrentMarker) {
        for (int i = 0; i < markerCardsList.size(); i++) {
            if (markerCardsList.get(i).markerId.equals(idCurrentMarker)) {
                CPGlobalData.getInstance().indexCurrentMarker = i;
                CPGlobalData.getInstance().isCurrentMarker = true;
                startFragment(offerFragment);
                break;
            }
        }
    }

    public void initReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                checkOfferFromDevice();
            }
        };
        intentFilter = new IntentFilter(BROADCAST_FOUND_OFFER);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private List<OfferCard> cleanOfferFromDevice() {
        long validTime = System.currentTimeMillis() - CPConstan.VALID_TIME_OFFERS;
        List<OfferCard> offersDeviceCardList = new ArrayList<>();

        Iterator<DeviceByOffers> iter = CPGlobalData.getInstance().offersFromDevice.iterator();
        while (iter.hasNext()) {
            DeviceByOffers offerByDevice = iter.next();
            if (offerByDevice.boundsInState && validTime < offerByDevice.boundsInMillisecond) {     // check offer validTime
                if (!offerByDevice.visibleState) {                                                  // check offer visibleState
                    offerByDevice.visibleState = true;
                    offersDeviceCardList.add(new OfferCard(offerByDevice.offer));
                }
            } else {
                iter.remove();
            }
        }
        return offersDeviceCardList;
    }

    private void checkOfferFromDevice() {
        CPGlobalData globalData = CPGlobalData.getInstance();
        if (globalData.offersFromDevice != null) {                               //TODO FIX WIFI
            if (!globalData.offersFromDevice.isEmpty()) {
                logD("checkOfferFromDevice " + globalData.offersFromDevice.size());
                List<OfferCard> offersDeviceCardList = cleanOfferFromDevice();
/*                if (CPGlobalData.getInstance().offersFromDevice != null && !CPGlobalData.getInstance().offersFromDevice.isEmpty())
                    for (int i = 0; i < CPGlobalData.getInstance().offersFromDevice.size(); i++) {
                        Offer offer = CPGlobalData.getInstance().offersFromDevice.get(i).offer;
                        offerCards.add(new OfferCard(offer));
                    }*/
                if (!offersDeviceCardList.isEmpty()) {
                    globalData.isOfferFromDevice = true;
                    if (globalData.offersCardList != null) {
                        for (int i = 0; i < offersDeviceCardList.size(); i++) {
                            globalData.offersCardList.add(offersDeviceCardList.get(i));
                            logD("globalData.offersCardList.add(offers) (for)" + (i));
                        }
                    } else {
                        globalData.offersCardList = offersDeviceCardList;
                        logD("globalData.offersCardList = (offers) " + offersDeviceCardList.size());
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startFragment(offerFragment);                                                           //Offers By Device
                        }
                    }, 100);
                }
            }
        }
    }


    private void startFragment(Fragment fragment) {
        offerFragmentState = true;
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.show_transition, R.anim.hide_transition)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private void endFragment(Fragment fragment) {
        if (fragment != null) {
            offerFragmentState = false;
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.show_transition, R.anim.hide_transition)
                    .remove(fragment)
                    .commit();
        }
    }

    @Override
    public void onDislikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("dislike", offerCard.campaignId);
            for (int i = 0; i < markerCardsList.size(); i++) {
                MarkerCard markerCard = markerCardsList.get(i);
                if (markerCard.offerCard.equals(offerCard)) {
                    markerCard.isLike = false;
                }
            }
        } else {
            sendOffersActionRequest("dislike", offerCard.offerId);
            for (int i = 0; i < markerCardsList.size(); i++) {
                MarkerCard markerCard = markerCardsList.get(i);
                if (markerCard.offerCard.equals(offerCard)) {
                    markerCard.optIn = false;
                }
            }
        }
        filteredMarkersOnTheMap();
    }

    @Override
    public void onLikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("like", offerCard.campaignId);
            for (int i = 0; i < markerCardsList.size(); i++) {
                MarkerCard markerCard = markerCardsList.get(i);
                if (markerCard.offerCard.mCampaign != null
                        && offerCard.mCampaign != null
                        && markerCard.offerCard.mCampaign.equals(offerCard.mCampaign)) {
                    markerCard.isLike = true;
                }
            }
        } else {
            CPGlobalData.getInstance().myOffersListDeprecated = true;                           // My Offers List is Deprecated
            sendOffersActionRequest("like", offerCard.offerId);
            for (int i = 0; i < markerCardsList.size(); i++) {
                MarkerCard markerCard = markerCardsList.get(i);
                if (markerCard.offerCard.mOffer != null
                        && offerCard.mOffer != null
                        && markerCard.offerCard.mOffer.equals(offerCard.mOffer)) {
                    markerCard.optIn = true;
                }
            }
        }
        filteredMarkersOnTheMap();
    }

    @Override
    public void onMaybeButtonSelected() {
        endFragment(offerFragment);
        testState = true;
        getOffers();
    }

    @Override
    public void onShareTwitterButtonSelected(final OfferCard offerCard) {
        if (offerCard.mOffer.media != null && offerCard.mOffer.media.size() > 0) {
            String imgUrl = offerCard.mOffer.media.get(0).replace(" ", "+");
            showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                    hideLoader();
                    shareTwitter(offerCard, response, true);
                }
            });
        } else {
            shareTwitter(offerCard, null, false);
        }
    }

    private void shareTwitter(OfferCard offerCard, Bitmap bitmap, boolean haveImage) {
        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        if (haveImage) {
            tweetIntent.setType("image/*");
            tweetIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
        }
        tweetIntent.setType("text/plain");
        tweetIntent.putExtra(Intent.EXTRA_TEXT, offerCard.name + "\n" + offerCard.title /*+ "\n" + offerCard.subtitle*/);
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);
        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }
        if (resolved) {
            startActivity(tweetIntent);
        } else {
            Toast.makeText(this, R.string.twitter_not_found, Toast.LENGTH_LONG).show();              // TODO Alert
            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
            marketIntent.setData(Uri.parse(getString(R.string.twitter_url_market)));
            startActivity(marketIntent);
        }

    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void showLoader() {
        new ProgressDialogFragment().show(getSupportFragmentManager(), ProgressDialogFragment.class.getName());
    }

    public void hideLoader() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ProgressDialogFragment.DISMISS_ACTION));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.navigation_btn:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    //      offersBtn.setImageResource(R.drawable.navigation_icon);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    //      offersBtn.setImageResource(R.drawable.back_arrow);
                }
                break;
            case R.id.fab:
                startActivity(new Intent(this, MyOffersListActivity.class));
                break;
            case R.id.profile_btn:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
        }
    }

    private void clickCampaignFilter() {
        if (campaignsFilterState) {
            campaignsFilterState = false;
            filteredMarkersOnTheMap();
            campaignsFilterImg.setVisibility(View.INVISIBLE);
        } else {
            campaignsFilterState = true;
            filteredMarkersOnTheMap();
            campaignsFilterImg.setVisibility(View.VISIBLE);
        }
    }

    private void clickOfferFilter() {
        if (offerFilterState) {
            offerFilterState = false;
            filteredMarkersOnTheMap();
            offerFilterImg.setVisibility(View.INVISIBLE);
        } else {
            offerFilterState = true;
            filteredMarkersOnTheMap();
            offerFilterImg.setVisibility(View.VISIBLE);
        }
    }

    private void filteredMarkersOnTheMap() {
        if (markerCardsList != null && markerCardsList.size() > 0) {
            for (int i = 0; i < markerCardsList.size(); i++) {
                MarkerCard markerCard = markerCardsList.get(i);
                if (markerCard.isCampaignType) {
                    if (campaignsFilterState && markerCard.isLike == CPConstan.LIKE) {
                        markerCard.marker.setVisible(true);                                        //Visible Campaign in map if not Like(false)
                    } else {
                        markerCard.marker.setVisible(false);
                    }
                } else {
                    if (offerFilterState && markerCard.optIn == CPConstan.OPT_IN) {                         //Visible Offer in map if optIn false
                        markerCard.marker.setVisible(true);
                    } else {
                        markerCard.marker.setVisible(false);
                    }
                }
            }
        }
    }


    private void startBtService() {
        if (!isMyServiceRunning(BtEstimoteService.class))
            sendBroadcast(new Intent("ru.racoondeveloper.beaconnotifyer.WAKE_RECEIVER"));
    }

    private void startWiFiService() {
        sendBroadcast(new Intent("ru.racoondeveloper.beaconnotifyer.WAKE_WIFI_RECEIVER"));
    }

    private void startGpsService() {
        if (!isMyServiceRunning(GPSService.class))
            sendBroadcast(new Intent("ru.racoondeveloper.beaconnotifyer.WAKE_GPS_RECEIVER"));
    }

    private void stopServices() {
        stopService(new Intent(context, GPSService.class));
        stopService(new Intent(context, BtEstimoteService.class));

        CPGlobalData.getInstance().deviceByOffersList = null;
        stopService(new Intent(context, WifiService.class));
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) { // check working service
        List<String> list = new ArrayList<>();
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            list.add(service.service.getClassName());
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
