package com.mattford.net.network.model.myoffers;

import com.google.gson.annotations.SerializedName;

public class Markers {
    @SerializedName("lat")
    public Float lat;
    @SerializedName("lng")
    public Float lng;
    @SerializedName("radius")
    public Integer radius;
}
