package com.mattford.net;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.mattford.net.tools.CPConstan;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.ui.MapsActivity;

/**
 * add to AndroidManifest.xml
 * <application android:name=".MyApplication"
 * use - MyApplication.getAppContext();
 */

public class MyApplication extends Application {

    protected static Context context;
    private static Resources resources;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
        MyApplication.resources = getResources();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    public static Resources getAppResources() {
        return MyApplication.resources;
    }

    public static boolean isActivityVisible() {
        return CPGlobalData.getInstance().activityVisible;
    }

    public static void activityResumed() {
        CPGlobalData.getInstance().activityVisible = true;
    }

    public static void logD(String massage) {
        if (!BuildConfig.DEBUG)
            Log.d(CPConstan.TAG, massage);
    }

    public static void logD(String TAG, String massage) {
            Log.d(TAG, massage);
    }

    public static void activityPaused() {
        CPGlobalData.getInstance().activityVisible = false;
    }

    public static void showNotificationBeacon(String title, String message) {
        Intent notifyIntent = new Intent(context, MapsActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivities(context, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(context)
                .setSmallIcon(R.drawable.blue_pin)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
