
package com.mattford.net.network.model.myoffers;

import com.google.gson.annotations.SerializedName;

public class Account {

    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("media")
    public String media;

}
