package com.mattford.net.tools;


public class CPConstan {
    public static final String TAG = "Airport";
    public static final long CONVERTER_TO_MINUTES = 1000 * 60 * 1;
    public static final long CONVERTER_TO_SECONDS = 1000;

    public final static boolean LIKE = false;
    public final static boolean OPT_IN = false;
    public final static double RADIUS = 1000.00; // meters

    public final static long GET_OFFER_DURATION = CONVERTER_TO_SECONDS * 1; //sec
    public final static long GET_DEFAULT_OFFER_DURATION = CONVERTER_TO_SECONDS * 3; //sec

    public final static long DELAY_WIFI_GET_MAC = CONVERTER_TO_SECONDS * 5; //sec
    public final static long DELAY_WIFI_OUT = CONVERTER_TO_MINUTES * 10; //min
    public final static long DELAY_BT_OUT = CONVERTER_TO_MINUTES * 10; //min

    public final static long BUTTON_GROUP_DURATION = 150; //millis
    public final static long UPDATE_BUTTON_DELAY = 100; //millis

    public static float threshold = 300; //pix  (CPConstan.threshold = displayMetrics.widthPixels / 4)
    public final static long END_FRAGMENT_DURATION = 300; //millis
    public static final long FILTER_ANIMATION_DURATION = 150;



    // The minimum distance to change Updates in meters
    public static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // meters
    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = CONVERTER_TO_MINUTES * 1; // min
    // The minimum distance to show nearest offer from MyOfferList
    public static final double VALID_OFFERS_DISTANCE = 500; // meters
    // The minimum time delay rerun Offer
    public final static long VALID_TIME_OFFERS = CONVERTER_TO_MINUTES * 10; //min


    public final static String JSON_GET_OFFER_TEST = "{\n" +
            "  \"success\": true,\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"id\": \"666db5b2-b7ad-11e6-aee2-23d95b88d4d9\",\n" +
            "      \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "      \"account\": {\n" +
            "        \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "        \"name\": \"CherryPie Studio\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "      },\n" +
            "      \"name\": \"Donatos Campaign 3\",\n" +
            "      \"effectiveDate\": \"2017-01-01\",\n" +
            "      \"expirationDate\": \"2017-01-10\",\n" +
            "      \"description\": \"Bonuses for Online Orders\",\n" +
            "      \"categories\": \"Entertainment\",\n" +
            "      \"ages\": \"30-34\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-campaign.s3.amazonaws.com/91746777-6588-ee7c-2cdf-8c4e09a55522_Don_Campaign_03.jpeg\"\n" +
            "      ],\n" +
            "      \"dislike\": 4,\n" +
            "      \"like\": false,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 40.748669,\n" +
            "          \"lng\": -73.912967,\n" +
            "          \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"64845c40-b7ad-11e6-ac20-1b6ff4cbd191\",\n" +
            "      \"offers\": [\n" +
            "        {\n" +
            "          \"id\": \"66a6ee29-b7ad-11e6-aee2-23d95b88d4d9\",\n" +
            "          \"media\": [\n" +
            "            \"https://onseen-offer.s3.amazonaws.com/02a95ff9-c631-5f6f-f743-7d5594b45fae_Don_Campaign_03_Offer_01.jpeg\"\n" +
            "          ],\n" +
            "          \"account\": {\n" +
            "            \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "            \"name\": \"CherryPie Studio\",\n" +
            "            \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "          },\n" +
            "          \"name\": \"CherryPie Studio iBeacon (11270) Offer\",\n" +
            "          \"effectiveDate\": \"2017-01-01\",\n" +
            "          \"expirationDate\": \"2017-01-05\",\n" +
            "          \"description\": \"3561:11270\",\n" +
            "          \"apikey\": \"Key\",\n" +
            "          \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "          \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "          \"campaignName\": \"3561:11270\",\n" +
            "          \"like\": 6,\n" +
            "          \"dislike\": 0,\n" +
            "          \"markers\": [\n" +
            "            {\n" +
            "              \"lat\": 40.748669,\n" +
            "              \"lng\": -73.912967,\n" +
            "              \"radius\": 30\n" +
            "            }\n" +
            "          ],\n" +
            "          \"devices\": [\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"ccbeafa2fde3d141e4d65abc480e601e\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:4713:11708\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"774c9bea28d2a635e38e5b7398d2e806\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:54856:27275\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"2acd06c611262655e58e5e8933644913\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:13614:1862\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"2acd06c611262655e58e5e8933644913\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:44521:13476\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"CherryPie\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:3561:11270\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:3561:11270\"\n" +
            "            }\n" +
            "          ],\n" +
            "          \"offerId\": \"02a95ff9-c631-5f6f-f743-7d5594b45fae1\",\n" +
            "          \"optIn\": false,\n" +
            "          \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "          \"deviceId\": \"64845c40-b7ad-11e6-ac20-1b6ff4cbd191\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"id\": \"66a6ee29-b7ad-11e6-aee2-23d95b88d4d9\",\n" +
            "          \"media\": [\n" +
            "            \"https://onseen-offer.s3.amazonaws.com/02a95ff9-c631-5f6f-f743-7d5594b45fae_Don_Campaign_03_Offer_01.jpeg\"\n" +
            "          ],\n" +
            "          \"account\": {\n" +
            "            \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "            \"name\": \"CherryPie Studio\",\n" +
            "            \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "          },\n" +
            "          \"name\": \"CherryPie Studio iBeacon (59925) Offer CherryPie Studio iBeacon (59925) Offer CherryPie Studio iBeacon (59925) Offer CherryPie Studio iBeacon (59925) Offer CherryPie Studio iBeacon (59925) Offer CherryPie Studio iBeacon (59925) Offer\",\n" +
            "          \"effectiveDate\": \"2017-01-01\",\n" +
            "          \"expirationDate\": \"2017-01-05\",\n" +
            "          \"description\": \"9872:59925\",\n" +
            "          \"apikey\": \"Key\",\n" +
            "          \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "          \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "          \"campaignName\": \"9872:59925\",\n" +
            "          \"like\": 6,\n" +
            "          \"dislike\": 0,\n" +
            "          \"markers\": [\n" +
            "            {\n" +
            "              \"lat\": 40.748669,\n" +
            "              \"lng\": -73.912967,\n" +
            "              \"radius\": 30\n" +
            "            }\n" +
            "          ],\n" +
            "          \"devices\": [\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:4713:11708\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:4713:11708\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:54856:27275\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:54856:27275\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:13614:1862\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:13614:1862\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"Onseen\",\n" +
            "              \"identifier\": \"b9407f30-f5f8-466e-aff9-25556b57fe6d:44521:13476\",\n" +
            "              \"udid\": \"b9407f30-f5f8-466e-aff9-25556b57fe6d:44521:13476\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"CherryPie\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:9872:59925\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:9872:59925\"\n" +
            "            }\n" +
            "          ],\n" +
            "          \"offerId\": \"02a95ff9-c631-5f6f-f743-7d5594b45fae2\",\n" +
            "          \"optIn\": false,\n" +
            "          \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "          \"deviceId\": \"64845c40-b7ad-11e6-ac20-1b6ff4cbd191\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"id\": \"66a6ee29-b7ad-11e6-aee2-23d95b88d4d9\",\n" +
            "          \"media\": [\n" +
            "            \"https://onseen-offer.s3.amazonaws.com/02a95ff9-c631-5f6f-f743-7d5594b45fae_Don_Campaign_03_Offer_01.jpeg\"\n" +
            "          ],\n" +
            "          \"account\": {\n" +
            "            \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "            \"name\": \"CherryPie Studio\",\n" +
            "            \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "          },\n" +
            "          \"name\": \"CherryPie Studio WiFi or iBeacon(59925) Offer CherryPie Studio WiFi or iBeacon(59925) Offer\",\n" +
            "          \"effectiveDate\": \"2017-01-01\",\n" +
            "          \"expirationDate\": \"2017-01-05\",\n" +
            "          \"description\": \"EC:08:6B:46:CA:7B\",\n" +
            "          \"apikey\": \"Key\",\n" +
            "          \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "          \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "          \"campaignName\": \"EC:08:6B:46:CA:7B\",\n" +
            "          \"like\": 6,\n" +
            "          \"dislike\": 0,\n" +
            "          \"markers\": [\n" +
            "            {\n" +
            "              \"lat\": 40.748669,\n" +
            "              \"lng\": -73.912967,\n" +
            "              \"radius\": 30\n" +
            "            }\n" +
            "          ],\n" +
            "          \"devices\": [\n" +
            "            {\n" +
            "              \"type\": \"macaddress\",\n" +
            "              \"deviceName\": \"CherryPie\",\n" +
            "              \"identifier\": \"ec:08:6b:46:ca:7c\",\n" +
            "              \"udid\": \"ec:08:6b:46:ca:7c\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"macaddress\",\n" +
            "              \"deviceName\": \"CherryPie\",\n" +
            "              \"identifier\": \"ec:08:6b:46:ca:7c\",\n" +
            "              \"udid\": \"ec:08:6b:46:ca:7c\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"type\": \"ibeacon\",\n" +
            "              \"deviceName\": \"CherryPie\",\n" +
            "              \"identifier\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:9872:59925\",\n" +
            "              \"udid\": \"B9407F30-F5F8-466E-AFF9-25556B57FE6D:9872:59925\"\n" +
            "            }\n" +
            "          ],\n" +
            "          \"offerId\": \"02a95ff9-c6131-5f6f-f743-7d5594b45fae3\",\n" +
            "          \"optIn\": false,\n" +
            "          \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "          \"deviceId\": \"64845c40-b7ad-11e6-ac20-1b6ff4cbd191\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"error\": null\n" +
            "}";


    public final static String JSON_MY_OFFER_TEST = "{\n" +
            "  \"success\": true,\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"id\": \"b83bcc47-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/b4e73e71-ddcc-c55a-fa81-b2c6f1cfc257_Don_Campaign_01_Offer_01.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "        \"name\": \"Donatos\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "      },\n" +
            "      \"name\": \"Stromboli Offer\",\n" +
            "      \"effectiveDate\": \"2016-12-01\",\n" +
            "      \"expirationDate\": \"2016-12-15\",\n" +
            "      \"description\": \"Stromboli with pizza\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"71a980f8-dd27-ccc9-f6f3-17d6cece2085\",\n" +
            "      \"campaignName\": \"Donatos Campaign 1\",\n" +
            "      \"like\": 16,\n" +
            "      \"dislike\": 0,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 40.748669,\n" +
            "          \"lng\": -73.912967,\n" +
            "          \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"b4e73e71-ddcc-c55a-fa81-b2c6f1cfc257\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102860-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "          \"name\": \"Donatos\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "        },\n" +
            "        \"name\": \"Donatos Campaign 1\",\n" +
            "        \"effectiveDate\": \"2016-12-01\",\n" +
            "        \"expirationDate\": \"2016-12-31\",\n" +
            "        \"description\": \"Much more than just pizza\",\n" +
            "        \"categories\": \"Entertainment\",\n" +
            "        \"ages\": \"26-30\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/71a980f8-dd27-ccc9-f6f3-17d6cece2085_Don_Campaign_01.jpeg\"\n" +
            "        ],\n" +
            "        \"dislike\": 5,\n" +
            "        \"like\": true,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 40.748669,\n" +
            "            \"lng\": -73.912967,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"71a980f8-dd27-ccc9-f6f3-17d6cece2085\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc4a-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/2d098404-7df7-1b17-a3a1-af0adc618a3a_Don_Campaign_03_Offer_02.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "        \"name\": \"Donatos\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "      },\n" +
            "      \"name\": \"$5 Off on $20\",\n" +
            "      \"effectiveDate\": \"2017-01-01\",\n" +
            "      \"expirationDate\": \"2017-01-10\",\n" +
            "      \"description\": \"$5 OFF ONLINE ORDER OF $20\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "      \"campaignName\": \"Donatos Campaign 3\",\n" +
            "      \"dislike\": 0,\n" +
            "      \"like\": 8,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 40.748669,\n" +
            "          \"lng\": -73.912967,\n" +
            "          \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"2d098404-7df7-1b17-a3a1-af0adc618a3a\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102862-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"a8a23130-b641-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"a9805c30-b641-11e6-9216-0bb84b03270b\",\n" +
            "          \"name\": \"Donatos\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/a9805c30-b641-11e6-9216-0bb84b03270b.jpeg?1480430638652\"\n" +
            "        },\n" +
            "        \"name\": \"Donatos Campaign 3\",\n" +
            "        \"effectiveDate\": \"2017-01-01\",\n" +
            "        \"expirationDate\": \"2017-01-10\",\n" +
            "        \"description\": \"Bonuses for Online Orders\",\n" +
            "        \"categories\": \"Entertainment\",\n" +
            "        \"ages\": \"30-34\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/91746777-6588-ee7c-2cdf-8c4e09a55522_Don_Campaign_03.jpeg\"\n" +
            "        ],\n" +
            "        \"dislike\": 4,\n" +
            "        \"like\": true,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 40.748669,\n" +
            "            \"lng\": -73.912967,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"91746777-6588-ee7c-2cdf-8c4e09a55522\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc40-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/f5f7ed2a-0b55-350d-0906-6259754f8b01_new_KFC_offer1.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"6a4b81e0-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "        \"name\": \"KFC\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/.jpeg?1480433710454\"\n" +
            "      },\n" +
            "      \"name\": \"Bucket Sale\",\n" +
            "      \"effectiveDate\": \"2017-01-01\",\n" +
            "      \"expirationDate\": \"2017-01-10\",\n" +
            "      \"description\": \"Bucket sale on $17.99\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"3f3ecfe9-503c-4053-2a27-117aaab6fbf5\",\n" +
            "      \"campaignName\": \"KFC Campaign 1\",\n" +
            "      \"like\": 19,\n" +
            "      \"dislike\": 0,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 38.2572547,\n" +
            "          \"lng\": -85.7540817,\n" +
            "          \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"f5f7ed2a-0b55-350d-0906-6259754f8b01\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102863-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"6a4b81e0-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "          \"name\": \"KFC\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/.jpeg?1480433710454\"\n" +
            "        },\n" +
            "        \"name\": \"KFC Campaign 1\",\n" +
            "        \"effectiveDate\": \"2017-01-01\",\n" +
            "        \"expirationDate\": \"2017-01-15\",\n" +
            "        \"description\": \"KFC New Campaign\",\n" +
            "        \"categories\": \"Entertainment\",\n" +
            "        \"ages\": \"14-18\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/3f3ecfe9-503c-4053-2a27-117aaab6fbf5_KFC_Campaign.jpeg\"\n" +
            "        ],\n" +
            "        \"like\": true,\n" +
            "        \"dislike\": 5,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 38.2572547,\n" +
            "            \"lng\": -85.7540817,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"3f3ecfe9-503c-4053-2a27-117aaab6fbf5\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc41-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/44db9932-41ec-c28d-453a-6a61152558c6_new_KFC_offer2.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"6a4b81e0-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "        \"name\": \"KFC\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/.jpeg?1480433710454\"\n" +
            "      },\n" +
            "      \"name\": \"Chiken & Biscutes\",\n" +
            "      \"effectiveDate\": \"2017-01-11\",\n" +
            "      \"expirationDate\": \"2017-01-15\",\n" +
            "      \"description\": \"Chiken & Biscutes at $999\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"3f3ecfe9-503c-4053-2a27-117aaab6fbf5\",\n" +
            "      \"campaignName\": \"KFC Campaign 1\",\n" +
            "      \"like\": 16,\n" +
            "      \"dislike\": 0,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 38.2572547,\n" +
            "          \"lng\": -85.7540817,\n" +
            "          \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"44db9932-41ec-c28d-453a-6a61152558c6\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102863-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"6a4b81e0-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "          \"name\": \"KFC\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/.jpeg?1480433710454\"\n" +
            "        },\n" +
            "        \"name\": \"KFC Campaign 1\",\n" +
            "        \"effectiveDate\": \"2017-01-01\",\n" +
            "        \"expirationDate\": \"2017-01-15\",\n" +
            "        \"description\": \"KFC New Campaign\",\n" +
            "        \"categories\": \"Entertainment\",\n" +
            "        \"ages\": \"14-18\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/3f3ecfe9-503c-4053-2a27-117aaab6fbf5_KFC_Campaign.jpeg\"\n" +
            "        ],\n" +
            "        \"like\": true,\n" +
            "        \"dislike\": 5,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 38.2572547,\n" +
            "            \"lng\": -85.7540817,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"3f3ecfe9-503c-4053-2a27-117aaab6fbf5\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc44-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/303022c6-9407-c4db-0f3b-44f1752cb828_nike_offer1.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "        \"name\": \"Nike\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "      },\n" +
            "      \"name\": \"Sale Offer\",\n" +
            "      \"effectiveDate\": \"2017-01-15\",\n" +
            "      \"expirationDate\": \"2017-01-30\",\n" +
            "      \"description\": \"50% Sale Offer\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "      \"campaignName\": \"Nike Campaign 1\",\n" +
            "      \"like\": 20,\n" +
            "      \"dislike\": 0,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "        \"lat\": 40.76274,\n" +
            "        \"lng\": -73.9734655,\n" +
            "        \"radius\": 30\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"303022c6-9407-c4db-0f3b-44f1752cb828\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102864-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "          \"name\": \"Nike\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "        },\n" +
            "        \"name\": \"Nike Campaign 1\",\n" +
            "        \"effectiveDate\": \"2017-01-15\",\n" +
            "        \"expirationDate\": \"2017-01-30\",\n" +
            "        \"description\": \"Nike New Campaign\",\n" +
            "        \"categories\": \"Health & Fitness\",\n" +
            "        \"ages\": \"30-34\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/5afc1583-7bbc-1172-4d75-980c609219ab_nike_Campaign.jpeg\"\n" +
            "        ],\n" +
            "        \"like\": true,\n" +
            "        \"dislike\": 23,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 40.7622974,\n" +
            "            \"lng\": -73.9734655,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc45-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/f9ffb067-d85c-0ebc-9d96-64b6c6dbedc8_nike_offer2.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "        \"name\": \"Nike\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "      },\n" +
            "      \"name\": \"New Product Offer\",\n" +
            "      \"effectiveDate\": \"2017-01-15\",\n" +
            "      \"expirationDate\": \"2017-01-25\",\n" +
            "      \"description\": \"Brand New shoes Offer\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "      \"campaignName\": \"Nike Campaign 1\",\n" +
            "      \"like\": 15,\n" +
            "      \"dislike\": 0,\n" +
            "      \"markers\": [\n" +
            "        {\n" +
            "          \"lat\": 0,\n" +
            "          \"lng\": 0,\n" +
            "          \"radius\": 0\n" +
            "        }\n" +
            "      ],\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"f9ffb067-d85c-0ebc-9d96-64b6c6dbedc8\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102864-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "          \"name\": \"Nike\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "        },\n" +
            "        \"name\": \"Nike Campaign 1\",\n" +
            "        \"effectiveDate\": \"2017-01-15\",\n" +
            "        \"expirationDate\": \"2017-01-30\",\n" +
            "        \"description\": \"Nike New Campaign\",\n" +
            "        \"categories\": \"Health & Fitness\",\n" +
            "        \"ages\": \"30-34\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/5afc1583-7bbc-1172-4d75-980c609219ab_nike_Campaign.jpeg\"\n" +
            "        ],\n" +
            "        \"like\": true,\n" +
            "        \"dislike\": 23,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 40.7622974,\n" +
            "            \"lng\": -73.9734655,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b83bcc46-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "      \"media\": [\n" +
            "        \"https://onseen-offer.s3.amazonaws.com/1a769fdc-7403-01af-2f5a-6610ce03e90e_nike_offer3.jpeg\"\n" +
            "      ],\n" +
            "      \"account\": {\n" +
            "        \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "        \"name\": \"Nike\",\n" +
            "        \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "      },\n" +
            "      \"name\": \"New Shoe Offer\",\n" +
            "      \"effectiveDate\": \"2017-01-21\",\n" +
            "      \"expirationDate\": \"2017-01-30\",\n" +
            "      \"description\": \"Upcoming Shoe Offer\",\n" +
            "      \"apikey\": \"Key\",\n" +
            "      \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "      \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "      \"campaignName\": \"Nike Campaign 1\",\n" +
            "      \"dislike\": 0,\n" +
            "      \"like\": 10,\n" +
            "      \"devices\": [],\n" +
            "      \"offerId\": \"1a769fdc-7403-01af-2f5a-6610ce03e90e\",\n" +
            "      \"optIn\": true,\n" +
            "      \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "      \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\",\n" +
            "      \"campaign\": {\n" +
            "        \"id\": \"b8102864-c042-11e6-a89b-ebe3fcdc271d\",\n" +
            "        \"userId\": \"3cb7dfd0-b649-11e6-bd3e-11d979432393\",\n" +
            "        \"account\": {\n" +
            "          \"id\": \"ad5f2360-b649-11e6-939b-33eea3d1ecdf\",\n" +
            "          \"name\": \"Nike\",\n" +
            "          \"media\": \"https://onseen-company.s3.amazonaws.com/.png?1480433822925\"\n" +
            "        },\n" +
            "        \"name\": \"Nike Campaign 1\",\n" +
            "        \"effectiveDate\": \"2017-01-15\",\n" +
            "        \"expirationDate\": \"2017-01-30\",\n" +
            "        \"description\": \"Nike New Campaign\",\n" +
            "        \"categories\": \"Health & Fitness\",\n" +
            "        \"ages\": \"30-34\",\n" +
            "        \"media\": [\n" +
            "          \"https://onseen-campaign.s3.amazonaws.com/5afc1583-7bbc-1172-4d75-980c609219ab_nike_Campaign.jpeg\"\n" +
            "        ],\n" +
            "        \"like\": true,\n" +
            "        \"dislike\": 23,\n" +
            "        \"markers\": [\n" +
            "          {\n" +
            "            \"lat\": 40.7622974,\n" +
            "            \"lng\": -73.9734655,\n" +
            "            \"radius\": 30\n" +
            "          }\n" +
            "        ],\n" +
            "        \"campaignId\": \"5afc1583-7bbc-1172-4d75-980c609219ab\",\n" +
            "        \"applicationId\": \"5231840d-8a86-4d42-9c7f-e40fb74dadc5\",\n" +
            "        \"deviceId\": \"b6d78150-c042-11e6-b5f4-7fd15d0ec0f2\"\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"error\": null\n" +
            "}";
}

