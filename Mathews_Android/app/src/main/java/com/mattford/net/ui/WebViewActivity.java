package com.mattford.net.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mattford.net.MyApplication;
import com.mattford.net.R;
import com.mattford.net.network.NetworkCallBackImage;
import com.mattford.net.network.NetworkController;
import com.mattford.net.network.model.OfferCard;
import com.mattford.net.tools.CPGlobalData;
import com.mattford.net.ui.fragment.MapOfferDialogFragment;

import static com.mattford.net.ui.MapsActivity.BROADCAST_FOUND_OFFER;

public class WebViewActivity extends BaseActivity {

    private MapOfferDialogFragment mapOfferFragment;
    private IntentFilter intentFilter;
    private BroadcastReceiver mBroadcastReceiver;
    private boolean mapOfferFragmentState;
    private WebView mWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(getLayoutResourceId());

        mapOfferFragment = MapOfferDialogFragment.newInstance();

        initReceiver();

        initView();

        String url = getIntent().getStringExtra("url");
        if (!url.isEmpty())
            mWebViewPostRequest(url);

        ImageButton backBtn = (ImageButton) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //  setFontViewGroup((ViewGroup) findViewById(R.id.profile_layout), FontAvenirConfigure.FONT_AVENIR_NEXT_REGULAR);
    }

    private void initView() {
        String title = getIntent().getStringExtra("title");
        TextView titleTextView = (TextView) findViewById(R.id.title_toolbar);
        titleTextView.setText(title);
    }


    private void mWebViewPostRequest(String url) {
        mWebView = (WebView) findViewById(R.id.ticketWebView);
        mWebView.getSettings().setJavaScriptEnabled(true); // включаем поддержку JavaScript
        mWebView.setWebViewClient(new MyWebViewClient());  // указываем оброботчика для отображения - наше приложение
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(false);
/*        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(true);*/

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(mWebView, url);
                // hideLoader();
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                hideLoader();
            }
        });

        try {
            // load the url
            showLoader();
            mWebView.loadUrl(url);         // отправляем get запрос
        } catch (Exception e) {
            e.printStackTrace();
            // hideLoader();
        }

    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                view.loadUrl(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.web_view;   // set layout instead of setContentView
    }

    public void initReceiver() {
        mapOfferFragment = MapOfferDialogFragment.newInstance();
        mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                checkOfferFromDevice(mapOfferFragment);                                             //TODO Offers From Device
            }
        };
        intentFilter = new IntentFilter(BROADCAST_FOUND_OFFER);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }


    @Override
    public void onDislikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("dislike", offerCard.campaignId);
        } else {
            sendOffersActionRequest("dislike", offerCard.offerId);
        }
    }

    @Override
    public void onLikeButtonSelected(OfferCard offerCard) {
        if (offerCard.isCampaignType) {
            sendCampaignActionRequest("like", offerCard.campaignId);
        } else {
            sendOffersActionRequest("like", offerCard.offerId);
            CPGlobalData.getInstance().myOffersListDeprecated = true;                           // My Offers List is Deprecated
        }
    }

    @Override
    public void onMaybeButtonSelected() {
        mapOfferFragmentState = false;
        endFragment(mapOfferFragment);
    }

    @Override
    public void onShareTwitterButtonSelected(final OfferCard offerCard) {

        if (offerCard.mOffer.media != null && offerCard.mOffer.media.size() > 0) {
            String imgUrl = offerCard.mOffer.media.get(0).replace(" ", "+");
             showLoader();
            NetworkController.getInstance().getImageFromUrl(this, imgUrl, new NetworkCallBackImage() {
                @Override
                public void onResponse(Bitmap response) {
                         hideLoader();
                    shareTwitter(offerCard, response, true);
                }
            });
        } else {
            shareTwitter(offerCard, null, false);
        }
    }

    @Override
    public void onBackPressed() {
        if (mapOfferFragmentState) {
            mapOfferFragmentState = false;
            endFragment(mapOfferFragment);
        }
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        MyApplication.activityPaused();
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }
}
